import time
import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, \
quantos_handling, centrifuge,stir_handling, hplc_handling

from scipy import stats
import json
from datetime import datetime

if __name__ == '__main__':

    wash_config = pd.read_csv("config/wash_config.csv")
    number_of_samples = len(wash_config.index)
    samplomatic.prime(volume_ml=1)
    centrifuge.home()
    duplicates = 2
    hplc_runtime = 10

    place_sample_in_A3 = True

    if place_sample_in_A3:
        df = wash_config.iloc[0]
        # solid_name = df["solid_name"]
        # solid_mass = df["solid_mass_g"]
        # predosed_tube_index = df["predosed_filter_index"]
        # duration = int(df["duration_min"])
        solvent_A = df["solvent_A"]
        solvent_B = df["solvent_B"]
        number_of_washes = df["number_of_washes"]
        solvent_ratio = float(df["solvent_ratio"])
        slurry_volume = float(df["slurry_volume_ml"])
        sample_volume = float(df["sample_volume_ml"])
        dilution_factor = int(df["dilution_factor"])

        # samplomatic.prime(volume_ml=0.4)

        vial_handling.home()
        somatic.grab_samplomatic()
        for i in range(duplicates):
            somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3')
            vial_handling.home()
            vial_index = deck_consumables.get_clean_vial(insert = True)[0]
            somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor, vial_index=vial_index)
        somatic.return_samplomatic()

        vial_handling.home()
        # hplc_handling.add_to_queue_and_run_if_ready(vial_index=vial_index, hplc_runtime=hplc_runtime)

    else:

        for i in range(number_of_samples):
            df = wash_config.iloc[i]

            solid_name = df["solid_name"]
            solid_mass = df["solid_mass_g"]
            predosed_tube_index = df["predosed_filter_index"]
            duration = int(df["duration_min"])
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            number_of_washes = df["number_of_washes"]
            solvent_ratio = float(df["solvent_ratio"])
            slurry_volume = float(df["slurry_volume_ml"])
            sample_volume = float(df["sample_volume_ml"])
            dilution_factor = int(df["dilution_factor"])

            sample_locs = ['A1','B1','C1']

            for n in range(number_of_washes):

                centrifuge.tube_to_active_area(tube_index=sample_locs[n], front=True)
                centrifuge.open_tube(index='front')
                centrifuge.push_tube_down(open=True)
                centrifuge.filter_from_tube()
                centrifuge.tube_to_tray(tube_index='B3',filter=True)

                samplomatic.prime(volume_ml=0.1)
                r_squared, slope, intercept = deck_consumables.get_calibration_info('THF')
                sample_volume_calibrated = (sample_volume-intercept)/slope
                vial_handling.home()
                somatic.grab_samplomatic()
                somatic.draw_sample_from_tube(volume_ml=sample_volume_calibrated, sample_index='A3')
                vial_handling.home()
                vial_index = deck_consumables.get_clean_vial()[0]
                somatic.dispense_hplc_grid(volume_ml=sample_volume_calibrated*dilution_factor, vial_index=vial_index)
                somatic.return_samplomatic()

                centrifuge.tube_from_tray(tube_index='B3', filter=True)
                centrifuge.filter_to_tube(index='front')
                centrifuge.close_tube(index='front')
                centrifuge.tube_from_active_area(tube_index=sample_locs[n], front=True)
                vial_handling.home()


















        '''
        TODO:
        -How do I shake the HPLC vials? Put in stirrer
        -Make a "calibrate volume" consumables function
        -COVER CHANGE IS BAD IDEA, REVERTonly have remove_cover at beginign 
        -'''









