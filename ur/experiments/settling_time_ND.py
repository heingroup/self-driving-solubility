import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, \
    quantos_handling, hplc_handling, centrifuge, shaker_handling, hplc_comm
from scipy import stats
import time
from datetime import datetime

sample_volume = 0.06
dilution_factor = 10
diluent_volume = sample_volume*dilution_factor
slurry_volume = 1
solvent = 'heptane'
interval_time = 1 # In minutes
rest_time = 0
number_of_samples = 7
vial_index = 'A1'
shaker_index_list = ['A1']
hplc_runtime = 5.5
use_hplc = False

# ts.set_temperature = 45
# ts.start_tempering()
# ts.start_shaking()
# time.sleep(10*60)
# ts.set_temperature = 15
# time.sleep(40*60)
# ts.stop_shaking()
# time.sleep(10)

# exit()
for shaker_index in shaker_index_list:
    diluent_vial_index_list = []

    if use_hplc:
        hplc_comm.send_tcp_message(f'solubility_{number_of_samples}')

    samplomatic.prime(2)

    if use_hplc:
        hplc_handling.run_blank()

    for i in range(number_of_samples):
        ts.start_shaking()
        time.sleep(15)
        start_time = datetime.now()
        ts.stop_shaking()

        time.sleep(rest_time*60)

        vial_handling.push_shaker()

        somatic.grab_samplomatic()

        somatic.draw_sample_from_shaker(sample_volume,shaker_index=shaker_index,type='hplc')

        end_time = datetime.now()
        calculated_rest_time = end_time - start_time
        calculated_rest_time_sec = calculated_rest_time.total_seconds()
        print(f'when rest time is {rest_time}, caluclated rest time is {calculated_rest_time_sec} sceonds')

        if diluent_volume <= 0.3:
            diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
        else:
            diluent_vial_index = deck_consumables.get_clean_vial()[0]
        diluent_vial_index_list.append(diluent_vial_index)
        somatic.dispense_hplc_grid(vial_index=diluent_vial_index, volume_ml=diluent_volume)

        somatic.return_samplomatic()

        rest_time += interval_time

    if use_hplc:
        for vial in diluent_vial_index_list:
            hplc_handling.run_current_vial(vial_index=vial, hplc_runtime=hplc_runtime + 0.75)

        hplc_handling.return_last_vial()
        hplc_handling.process_data_and_send_csv()

