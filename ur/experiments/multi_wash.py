import time
import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, \
quantos_handling, centrifuge,stir_handling

from scipy import stats
import json
from datetime import datetime

if __name__ == '__main__':

    wash_config = pd.read_csv("wash_config.csv")
    number_of_samples = len(wash_config.index)
    # samplomatic.prime(volume_ml=1)
    centrifuge.home()

    for i in range(number_of_samples):
        df = wash_config.iloc[i]

        solid_name = df["solid_name"]
        solid_mass = df["solid_mass_g"]
        predosed_tube_index = df["predosed_filter_index"]
        duration = int(df["duration_min"])
        solvent_A = df["solvent_A"]
        solvent_B = df["solvent_B"]
        number_of_washes = df["number_of_washes"]
        solvent_ratio = float(df["solvent_ratio"])
        slurry_volume = float(df["slurry_volume_ml"])
        sample_volume = float(df["sample_volume_ml"])
        dilution_factor = int(df["dilution_factor"])

        if predosed_tube_index == 'None':
            tube_index = deck_consumables.get_clean_tube(stirbar=True)

            centrifuge.tube_to_active_area(tube_index = tube_index)
            centrifuge.open_tube()

            centrifuge.tube_from_tray(tube_index = 'A3')
            quantos_handling.vial_to_holder(vial_type='centrifuge')

            dosed_mass = quantos_handling.dose_with_quantos(solid_weight=float(solid_mass * 1000),
                                                            vial_type='centrifuge')
            solid_mass = dosed_mass / 1000

            quantos_handling.vial_from_holder(vial_type='centrifuge')
            centrifuge.tube_to_tray(tube_index = 'A3')

        else:
            tube_index = predosed_tube_index

            centrifuge.tube_to_active_area(tube_index=tube_index)
            centrifuge.open_tube()

        centrifuge.remove_cover()

        #add save functions
        #change DataHandling to if can do all, change "add to experiment" to reflect the workflow it is using
        for n in range(number_of_washes):
            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            r_squared, slope, intercept = deck_consumables.get_calibration_info(solvent_A)
            slurry_volume_calibrated_A = (slurry_volume-intercept)/slope
            slurry_volume_calibrated_B = 0

            vial_handling.home()
            samplomatic.prime(volume_ml=0.1)
            somatic.grab_samplomatic()
            somatic.aspirate_stock_solvent(volume_ml=slurry_volume_calibrated_A * solvent_ratio, stock_index=stock_index_A)
            somatic.dispense_solvent_in_tube(volume_ml=slurry_volume_calibrated_A,
                                             sample_index='A3')

            if stock_index_B != None:
                r_squared, slope, intercept = deck_consumables.get_calibration_info(solvent_B)
                slurry_volume_calibrated_B = (slurry_volume-intercept)/slope
                somatic.aspirate_stock_solvent(volume_ml=slurry_volume_calibrated_B * (1 - solvent_ratio),
                                               stock_index=stock_index_B)
                somatic.dispense_solvent_in_tube(volume_ml=slurry_volume_calibrated_B,
                                                 sample_index='A3')

            somatic.return_samplomatic()

            centrifuge.push_tube_down(open=True)
            centrifuge.close_tube(index='front')

            centrifuge.tube_to_stirrer()
            stir_handling.stir_for_duration(stir_duration=duration * 60)
            centrifuge.tube_from_stirrer()

            centrifuge.run_centrifuge(tube_index='front', centrifuge_duration=15)

            clean_tube_index = deck_consumables.get_clean_tube()
            centrifuge.tube_to_active_area(tube_index=clean_tube_index, front=False)
            centrifuge.open_tube(index='back')
            centrifuge.open_tube(index='front')
            centrifuge.push_tube_down(open=True)
            centrifuge.switch_filter(leave_front_empty=True)

            samplomatic.prime(volume_ml=0.1)
            r_squared, slope, intercept = deck_consumables.get_calibration_info('THF')
            sample_volume_calibrated = (sample_volume-intercept)/slope
            vial_handling.home()
            somatic.grab_samplomatic()
            somatic.draw_sample_from_tube(volume_ml=sample_volume_calibrated, sample_index='A3')
            vial_handling.home()
            vial_index = deck_consumables.get_clean_vial()[0]
            somatic.dispense_hplc_grid(volume_ml=sample_volume_calibrated*dilution_factor, vial_index=vial_index)
            somatic.return_samplomatic()

            centrifuge.tube_from_tray(tube_index='B3', filter=True)
            centrifuge.filter_to_tube(index='front')
            centrifuge.push_tube_down(open=True)
            centrifuge.close_tube(index='front')
            centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
            centrifuge.tube_to_active_area(tube_index='C3', front=True)
            centrifuge.push_tube_down(open=True)
            centrifuge.home()

            tube_index = clean_tube_index

        centrifuge.close_tube(index='front')
        centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
        centrifuge.place_cover()

















    '''
    TODO:
    -How do I shake the HPLC vials? Put in stirrer
    -Make a "calibrate volume" consumables function
    -COVER CHANGE IS BAD IDEA, REVERTonly have remove_cover at beginign 
    -'''









