from typing import Dict
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import vial_handling, somatic, samplomatic, ts
from hein_utilities.files import Watcher
import os
from pathlib import Path
import logging
import json
import time
path = r'D:\Dropbox\Voice (1)\Kinova Manager\current'
control_folder = os.path.join(path, 'control')

watch = Watcher(
    path=control_folder,
    watchfor='.txt',
)
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

current_shaker = None
vial_index_list = []
shaker_index_list = []

def prepare_sample():
    # indices = ['A', 'B', 'C', 'D', 'E']
    # fir_pos = ((solvent_index % 5))
    # sec_pos = str((solvent_index % 2) + 1)
    #
    # solvent = indices[5-fir_pos] + sec_pos
    global vial_index_list
    global shaker_index_list
    # samplomatic.prime(1)
    somatic.grab_samplomatic()
    vial_index = deck_consumables.get_clean_vial()[0]
    vial_index_list.append(vial_index)
    shaker_index = deck_consumables.get_empty_shaker_location()
    shaker_index_list.append(shaker_index)
    somatic.aspirate_stock_solvent(volume_ml=0.5, stock_index="A1")
    somatic.dispense_hplc_grid(volume_ml=0.5, vial_index=vial_index, backend=False)
    somatic.return_samplomatic()
    vial_handling.vial_from_tray(vial_index=vial_index)
    vial_handling.cup_to_shaker(shaker_index=shaker_index)
    ts.start_shaking()

def return_vial():
    global vial_index_list
    global shaker_index_list
    ts.stop_shaking()
    vial_handling.push_shaker()
    for i in range(len(vial_index_list)):
        ts.stop_shaking()
        vial_handling.cup_from_shaker(shaker_index_list[i])
        vial_handling.vial_to_tray(vial_index_list[i])

    vial_index_list = []
    shaker_index_list = []


def main():
    while True:
        control_folder_contents = watch.contents
        if len(control_folder_contents) > 0:
            action_file_path = watch.oldest_instance()
            action_info = Path(action_file_path).read_text()
            json_acceptable_string = action_info.replace("'", "\"")
            json_acceptable_string = json_acceptable_string.replace("\n", "")
            json_acceptable_string = json_acceptable_string.replace("[", "{")
            json_acceptable_string = json_acceptable_string.replace("]", "}")
            action_info = json.loads(json_acceptable_string)
            action_uuid: str = list(action_info.keys())[0]
            executable: str = action_info[action_uuid]['executable']
            key_word_arguments: Dict = action_info[action_uuid]['key_word_arguments']
            try:
                print(f'execute: {executable} with key word arguments: {key_word_arguments}')
                exec(f'{executable}(**{key_word_arguments})')
                os.remove(action_file_path)
            except Exception as e:
                error_message = f'could not execute: {executable} with key word arguments: ' \
                                f'{key_word_arguments})\nError message:' + str(e)
                logger.warning(error_message)
        else:
            print('waiting for commands')
            time.sleep(5)

if __name__ == '__main__':
    main()
