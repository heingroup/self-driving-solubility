import pandas as pd
from ur.configuration import deck_consumables
from scipy import stats
import json
from datetime import datetime

def new_calibration():
    with open('pump_calibration.json') as fp:
        listObj = json.load(fp)
        listObj.append({'date': str(datetime.now()),
                        'solvent':'None',
                        'slope':'None',
                        'intercept':'None',
                        'r_squared':'None'})
        with open('pump_calibration.json', 'w') as json_file:
            json.dump(listObj, json_file)

def save_value(data_name, data_value):
    with open('pump_calibration.json') as fp:
        listObj = json.load(fp)
    listObj[-1][data_name] = data_value
    with open('pump_calibration.json', 'w') as json_file:
        json.dump(listObj, json_file)

if __name__ == '__main__':
    quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)

    # solvent = 'THF'
    # input_volumes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    # actual_volumes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    # solvent_density = deck_consumables.get_stock_info(solvent)[0]
    #
    # df = pd.DataFrame({'input_volume': input_volumes, 'actual_volume': actual_volumes})
    # df.to_csv('pump_calibration.csv', index=False)
    #
    # slope, intercept, r_value, p_value, std_err = stats.linregress(actual_volumes, input_volumes)
    #
    # print(slope)
    # print(intercept)
    # print(r_value)
    #
    # r_squared = r_value ** 2
    #
    # new_calibration()
    # save_value('solvent', solvent)
    # save_value('slope', str(slope))
    # save_value('intercept', str(intercept))
    # save_value('r_squared', str(r_squared))
