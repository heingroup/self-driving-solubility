import time
import ur.configuration.ur3_station_engagements as ur3
# import n9.experiments.station_robot_engagement as n9
# from ur.configuration.ur_deck import ur3_centrifuge

ur3.open_tube('front')
ur3.close_tube('front')
ur3.open_tube('back')
ur3.close_tube('back')
# ur3.liquid_handlling(2)
# ur3.liquid_handlling(2)


ur3.tube_from_tray(tube_index='A1')
ur3.tube_to_capstation(index='front')

ur3.open_tube('front')
ur3.filter_from_capstation('front')
ur3.filter_to_tube_holder()
ur3.holder_to_quantos()
# dose
ur3.holder_from_quantos()
ur3.filter_from_tube_holder()
ur3.filter_to_capstation('front')
# dispense
ur3.close_tube('front')
ur3.tube_from_capstation('front')
ur3.tube_to_minitray()
ur3.home()

# n9.open_vision_station()
# n9.tube_from_miniTray('B2')
# n9.tube_to_vision()
# time.sleep(3)
# n9.tube_from_vision()
# n9.tube_to_miniTray('B2')
# n9.move_away_for_ur()

ur3.tube_from_minitray()
ur3.tube_to_capstation('front')
ur3.run_centrifuge()

ur3.tube_from_tray('A2')
ur3.tube_to_capstation('back')

ur3.open_tube('back')
ur3.open_tube('front')
ur3.filter_from_capstation('back')
ur3.filter_to_tube_holder()
ur3.filter_from_capstation('front')
ur3.filter_to_capstation('back')
ur3.close_tube('back')

ur3.tube_from_capstation('back')
ur3.tube_to_minitray()
ur3.home()
#
# n9.open_vision_station()
# n9.tube_from_miniTray('B2')
# n9.tube_to_vision()

ur3.filter_from_tube_holder()
ur3.filter_to_capstation('front')
ur3.close_tube('front')
ur3.tube_from_capstation('front')
ur3.tube_to_tray('A1')
ur3.home()

# n9.tube_from_vision()
# n9.tube_to_miniTray('B2')
# n9.move_away_for_ur()

# ur3_centrifuge.run_centrifuge(5)


