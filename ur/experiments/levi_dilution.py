import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling
import time
import logging
from datetime import datetime

number_of_samples = 7
#change to count
levi_config = pd.read_csv("config/levi_config_v2.csv")

for n in range(number_of_samples):
    df = levi_config.iloc[n]
    shaker_index = ['A1', 'B1', 'C1', 'D1', 'A2', 'B2', 'C2', 'D2']
    cup_index = ['A1', 'A2', 'A3', 'A4', 'B1', 'B2', 'B3', 'B4']
    sample_volume = float(df["sample_volume"])
    # r_squared, slope, intercept = deck_consumables.get_calibration_info('THF')
    # sample_volume_calibrated = (sample_volume - intercept) / slope
    dilution_factor = float(df["dilution_factor"])
    diluent_volume = sample_volume*dilution_factor
    if diluent_volume <= 0.3:
        diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
    else:
        diluent_vial_index = deck_consumables.get_clean_vial()[0]
    if n == 0:
        samplomatic.prime(volume_ml=2)

    vial_handling.push_filter_in_shaker(shaker_index=shaker_index[n])
    vial_handling.cup_from_shaker(shaker_index=shaker_index[n])
    vial_handling.cup_to_tray(cup_index=cup_index[n])

    samplomatic.prime(volume_ml=0.2)
    somatic.grab_samplomatic()
    somatic.draw_sample_from_cup(volume_ml=diluent_volume, sample_index=cup_index[n])
    somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor, vial_index=diluent_vial_index)
    somatic.return_samplomatic()