import math
import time

from hein_robots.robotics import Location, Cartesian
from ur.configuration import ur_deck

JOINT_VELOCITY = 60
LINEAR_VELOCITY = 80
ROTATE_VELOCITY: float = 10

def home():
    ur_deck.arm.move_joints(ur_deck.QUANTOS_SEQUENCE.joints['home'], velocity=JOINT_VELOCITY)


def home_shaker():
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['home'], velocity=JOINT_VELOCITY)

def liquid_handlling(volume: float):
    ur_deck.sampleomatic.move_in_mm(20)
    ur_deck.dosing_pump.dispense_ml(volume, from_port=ur_deck.SOLVENT_PORT, to_port=ur_deck.SAMPLE_PORT, )
    ur_deck.sampleomatic.home()


def close_shaker_gripper():
    ur_deck.arm.open_gripper(0.5)
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_back'] + Location(z=40),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_back'], velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_back'] + Location(x=29),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_back'], velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_back'] + Location(z=40),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)


def open_shaker_gripper():
    ur_deck.arm.open_gripper(0.5)
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_front'] + Location(z=40),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_front'], velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_front'] + Location(x=-29),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_front'], velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.SHAKER_SEQUENCE.locations['approach_front'] + Location(z=40),
                                 velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)


def push_filter_in_shaker(shaker_index: str = 'A1'):
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)
    ur_deck.arm.open_gripper(0.95)
    ur_deck.arm.move_to_location(ur_deck.shaker_grid.locations[shaker_index] + Location(z=40), velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_to_location(ur_deck.shaker_grid.locations[shaker_index], velocity=LINEAR_VELOCITY)
    ur_deck.arm.move_joints(ur_deck.SHAKER_SEQUENCE.joints['safe_above'], velocity=JOINT_VELOCITY)


if __name__ == "__main__":
    print('start')
    # for i in range(50):
    #     holder_to_quantos()
    #     holder_from_quantos()

    # ur_deck.centrifuge.open_tube()
    ur_deck.centrifuge.filter_from_capstation()
    # ur_deck.centrifuge.filter_to_capstation()
    # ur_deck.centrifuge.run_centrifuge(10)


    # open_tube('back')
    # close_tube('back')
    # tube_from_tray('A1')

    # home_shaker()
    # push_filter_in_shaker(shaker_index='A3')
    # push_filter_in_shaker(shaker_index='B4')
    # open_shaker_gripper()
    # close_shaker_gripper()

    # holder_to_quantos()
    # holder_from_quantos()
    # tube_from_tray()
    # tube_to_capstation()
    # open_tube()
    # tube_from_opencap()
    # to_innertube_holder()
    # tube_to_quantos()
    # tube_from_quantos()
    # from_innertube_holder()
    # tube_to_opencap(1)
    # tube_from_tray('B1')
    # tube_to_capstation('back')
    # open_tube('back')
    # filter_from_capstation('back')
    # filter_to_tube_holder()
    # filter_from_capstation()  # filter
    # filter_to_capstation('back')  # Filter
    # close_tube('back')  # add a push
    # filter_from_tube_holder()
    # filter_to_capstation()  # filter
    # close_tube()
    # filter_from_capstation()
    # tube_to_tray()
    # filter_from_capstation('back')
    # tube_to_tray('B1')
