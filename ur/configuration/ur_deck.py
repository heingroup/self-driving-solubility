# import niraapad.backends
# from niraapad.lab_computer.niraapad_client import NiraapadClient
# abstract_config_file = r'C:\\Users\\User\\Desktop\\abstract_config_file_template.json'
# domain_config_file =  r'C:\\Users\\User\\Desktop\\domain_config_file_hein_lab.json'
# NiraapadClient.connect_to_middlebox('hein-iobey.chem.ubc.ca', '1337', abstract_configdir=None, domain_configdir=None)
# from niraapad.shared.utils import MO, BACKENDS
# #NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})
# #NiraapadClient.update_mos(exceptions={BACKENDS.QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})
# # NiraapadClient.update_mos(exceptions={BACKENDS.FTDI_DEVICE: MO.DIRECT_PLUS_MIDDLEBOX})
# NiraapadClient.update_mos()
import sys

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, r'C:\Users\User\PycharmProjects\self_driving_solubility')

import pandas as pd
import time
# import repackage
# .add(r"C:/Users/User/PycharmProjects/self_driving_solubility/ur/ur/configuration/ur_deck.py")

from ur.components.samplomatic.samplomatic import Samplomatic
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.robotics import Location, Cartesian
from hein_robots.grids import Grid, StaticGrid

from ika import Thermoshaker, MagneticStirrer
from ur.components.vision_angle_measurement.measurement import VisionDotAngleMeasurement
from ur.components.ur3_centrifuge.centrifuge import Centrifuge
from ur.components.filter_vial_handling.filter_handling import FilterHandling
from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.samplomatic_stepper.samplomatic_stepper import SamplomaticStepper
from ur.components.shaker_handling.shaker_handling import ShakerHandling
from ur.components.data_handling.data_handling import DataHandling
from ur.components.quantos_handling.quantos_handling import QuantosHandling
from ur.components.stir_vision.stirrer_handling import StirHandling
from ur.components.capper.cap_handling import CapHandling
from ur.components.capper.capper import Capper
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from mtbalance.arduino import ArduinoAugmentedQuantos
from north_devices.pumps.tecan_cavro import TecanCavro
from ftdi_serial import Serial
import serial
from ur.components.relay_board.relay_board import RelayBoard
from ika.magnetic_stirrer import MagneticStirrer
from kinova.configuration.arm_manager.arm_manager import ArmManager
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from ur.components.camera_handling.camera_handling import CameraHandling

from kinova.configuration.sequences.sequences import sequences, sequence_names

print('connecting to ur deck')
# no middle box com9
# middlebox com23
SAMPLOMATIC_STEPPER_PORT = "COM9"
SAMPLE_PORT = 1
SOLVENT_PORT = 2
EMPTY_PORT = 3
DOSING_PUMP_VOL = 2.5  # mL
samplomatic = Samplomatic(SAMPLOMATIC_STEPPER_PORT, tecan_adress=0, port_to_prime_from=SOLVENT_PORT,
                          somatic_port=SAMPLE_PORT, empty_port=EMPTY_PORT)

samplomatic.home_needle()

# middlebox: COM13
# no middlebox: COM37
stirrer = MagneticStirrer("COM37")

# no middlebox: COM28
relay = RelayBoard(Serial(device_serial='FT000001'))

# 0.03 for the long gripper
# tool_offset = Location(z=10).convert_mm_to_m()
# tool_offset = Location(0).convert_mm_to_m()
# relay = RelayBoard(Serial(device_serial='FT000001'))
# hplc_relay = RelayBoard(Serial(device_port='COM36'))
hplc_relay = serial.Serial(port='COM36', baudrate=115200)
# arm
arm = UR3Arm('192.168.254.88')

# Quantos
# middlebox: COM 7
# no middlebox: COM13
# for i in range(0, 5):

#without middlebox: 13
#with middlebox: 7

quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)
quan.front_door_position
quan.set_home_direction(0)
quan.lock_dosing_pin_position()

#     del (quan)
#exit()
# thermoshaker
# no middlebox: COM18
# middlebox: COM34
port = 'COM18'  # todo set to the correct port
dummy = False  # todo set to true if testing without connecting to an actual thermoshaker
kwargs = {
    'port': port,
    'dummy': dummy,
}
ts = Thermoshaker.create(**kwargs)
ts.watchdog_safety_temperature = 15.5
ts.start_watchdog_mode_1(30)
ts.start_watchdog_mode_2(30)
ts.switch_to_normal_operation_mode()
ts.stop_shaking()
camera = CameraHandling()

# ____Sampleomatic___
# stepper samplomatic
# middlebox: COM23
# no middlebox: COM9
# tool_offset = Location(z=0.2-0.035)
# arm.robot.set_tcp([*tool_offset.position, *tool_offset.orientation])
# shorter gripper is z=0.2-0.048
# print("Serial Initialization", flush=True)
# serial_cavro = Serial(device_serial='FT3FJFPV', baudrate=9600)  # Cavro pumps default to 9600
# print("serial after Initialization", flush=True)
# dosing_pump = TecanCavro(serial_cavro, address=0, syringe_volume_ml=DOSING_PUMP_VOL)
# dosing_pump.home()
CENTRIFUGE_SEQUENCE_NAMES = ['center', 'cover_engage', 'center_sh', 'cover', 'cover_sh', 'tube_engage',
                             'tube_approach',
                             'tube_sh']
# COVER_SEQUENCE_NAMES = ['face_cent', 'cent_cover_sh', 'cent_cov_engage', 'cov_putdown_sh', 'cov_putdown_eng']
VIAL_SEQUENCE_NAMES = ['home', 'safe_height1', 'engage1', 'r_safe_height1', 'rotor_engage', 'r_pickup1']
ROD_SEQUENCE_NAMES = ['rotor_center', 'face_tray', 'rod_safeheight', 'rod_engage']
# CAP_SEQUENCE_NAMES = ['safe_hight', 'engage1', 'open_prep', 'open_push', 'open_down', 'close_prep',
#                       'close_up', 'close_push_prep', 'clo_push_engage', 'clos_push_forw', 'clo_push_prep',
#                       'close_push']
CAP_SEQUENCE_NAMES = ['home', 'face_left', 'sh_front', 'open_grip', 'open_up', 'open_right', 'open_push_prep',
                      'open_push_right', 'open_push_down', 'close_lift_prep', 'clo_lift_final', 'close_push_prep',
                      'clo_push_final', 'clo_push_down', 'safe_height']
TRAY_SEQUENCE_NAMES = ['home', 'face_left', 'safe_height', 'approach_A2', 'engage_A2', 'safe_height_A2']
SHAKER_SEQUENCE_NAMES = ['home', 'safe_above', 'approach_front', 'approach_back', 'shaker_a1', 'shaker_a1_above']
SAMPLOMATIC_SEQUENCE_NAMES = ['smp_engage', 'smp_approach', 'above_hplc', 'hplc_a1', 'above_solvents',
                              'solvent_a1']
CENTRIFUGE_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/centrifuge_new.script'
COVER_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/cover.script'
VIAL_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/cent_tube.script'
ROD_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/rod.script'
# CAP_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/tube_cap.script'
QUANTOS_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/quantos.script'
TRAY_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/tube_tray.script'
SHAKER_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/shaker.script'
STIR_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/ika.script'
CAPPER_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/capper.script'
# SAMPLOMATIC_SEQUENCE_FILE = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/samplomatic.script'
CENTRIFUGE_SEQUENCE = URScriptSequence(CENTRIFUGE_SEQUENCE_FILE, location_names=CENTRIFUGE_SEQUENCE_NAMES)
# COVER_SEQUENCE = URScriptSequence(COVER_SEQUENCE_FILE, location_names=COVER_SEQUENCE_NAMES)
VIAL_SEQUENCE = URScriptSequence(VIAL_SEQUENCE_FILE, location_names=VIAL_SEQUENCE_NAMES)
ROD_SEQUENCE = URScriptSequence(ROD_SEQUENCE_FILE, location_names=ROD_SEQUENCE_NAMES)
# CAP_SEQUENCE = URScriptSequence(CAP_SEQUENCE_FILE, location_names=CAP_SEQUENCE_NAMES)
# QUANTOS_SEQUENCE = URScriptSequence(QUANTOS_SEQUENCE_FILE)
TRAY_SEQUENCE = URScriptSequence(TRAY_SEQUENCE_FILE, location_names=TRAY_SEQUENCE_NAMES)
SHAKER_SEQUENCE = URScriptSequence(SHAKER_SEQUENCE_FILE, location_names=SHAKER_SEQUENCE_NAMES)
# STIR_SEQUENCE = URScriptSequence(STIR_SEQUENCE_FILE, location_names=STIR_SEQUENCE_NAMES)
# SAMPLOMATIC_SEQUENCE = URScriptSequence(SAMPLOMATIC_SEQUENCE_FILE, location_names=LIQUID_HANDLING_SEQUENCE_NAMES)
wall_grid = StaticGrid([Location(x=-328.6, y=360.1, z=359.3, rx=65.15, ry=-0.003652, rz=-90.01),
                        Location(x=-326.5, y=410.7, z=357.5, rx=65.15, ry=0.00338, rz=-90.01),
                        Location(x=-327.7, y=360.0, z=451.0, rx=65.16, ry=0.008699, rz=-90.01),
                        Location(x=-325.4, y=410.8, z=448.4, rx=65.16, ry=0.005148, rz=-90.02),
                        Location(x=-327.1, y=360.0, z=541.7, rx=65.16, ry=0.009651, rz=-90.01),
                        Location(x=-323.8, y=410.0, z=540.0, rx=65.15, ry=0.002098, rz=-90.01)],
                       columns=2, rows=3)
wall_sequence_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/indexes.script'
sequence = URScriptSequence(wall_sequence_file)
print(sequence.locations.values())
# quantos_guns = QuantosGuns(arm, quantos=quan, wall_grid=wall_grid,
#                            wall_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/indexes1.script',
#                            quantos_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/rbtprogrm1.script')
# 233.69,386.67,117.72
#
shaker_grid = Grid(Location(x=-99.5, y=355.3, z=169.6, rx=0, ry=0, rz=0), rows=6, columns=4,
                   spacing=Cartesian(18.2, 17.9, 0), offset=Location(rx=-180, ry=0, rz=90))
shaker_grid_dosing = Grid(Location(x=-99.7, y=260.4, z=262.5, rx=0, ry=0, rz=0), rows=4, columns=6,
                          spacing=Cartesian(18.2, 17.9, 0), offset=Location(rx=-180, ry=0, rz=-180))
hplc_vial_grid_front = Grid(Location(x=203.7, y=337.5, z=52.3, rx=0, ry=0, rz=0), rows=7, columns=4,
                      spacing=Cartesian(22, 22, 0),
                      offset=Location(rx=180, ry=0, rz=90))
# hplc_vial_grid_back = Grid(Location(x=-202.2, y=168.1, z=32.78, rx=0, ry=0, rz=0), rows=4, columns=7,
#                             spacing=Cartesian(21.9, 22, 0),
#                             offset=Location(rx=180, ry=0, rz=90))

#The hplc_vial_grid_back and hplc_vial_grid_dosing_back only have 7 columns and 4 rows. However, because we want each
#Grid to start at 'E', we created 4 imaginary rows. In any fucntions that interact with either grid, there is an if/else
#statement that ensures the script only interacts with the back grid when the index is >= 'E' (ie F,G,H etc)

hplc_vial_grid_back = Grid(Location(x=-289.8, y=168.1, z=32.78, rx=0, ry=0, rz=0), rows=4, columns=11,
                            spacing=Cartesian(21.9, 22, 0),
                            offset=Location(rx=180, ry=0, rz=90))
hplc_vial_grid_dosing_front = Grid(Location(x=204.3, y=242.6, z=145.8, rx=0, ry=0, rz=0), rows=7, columns=4,
                             spacing=Cartesian(22.3, 22, 0),
                             offset=Location(rx=-180, ry=0, rz=180))
# hplc_vial_grid_dosing_back = Grid(Location(x=-298.3, y=170.3, z=124.3, rx=0, ry=0, rz=0), rows=4, columns=7,
#                              spacing=Cartesian(21.9, 22, 0),
#                              offset=Location(rx=-180, ry=0, rz=90))
hplc_vial_grid_dosing_back = Grid(Location(x=-385.9, y=168.3, z=124.3, rx=0, ry=0, rz=0), rows=4, columns=11,
                             spacing=Cartesian(21.9, 22, 0),
                             offset=Location(rx=-180, ry=0, rz=90))
stock_grid = Grid(Location(x=156.8, y=64.4, z=208), rows=2, columns=5, spacing=Cartesian(25.5, 52.4),
                  offset=Location(rx=-180, ry=0, rz=90))
cup_grid = Grid(Location(x=-21.8, y=198.5, z=92.5, rx=0, ry=0, rz=0), rows=4, columns=6,
                spacing=Cartesian(20.1, 20.1, 0),
                offset=Location(rx=180, ry=0, rz=90))
cup_grid_dosing = Grid(Location(x=-116.6, y=198.1, z=190.5, rx=0, ry=0, rz=0), rows=4, columns=6,
                       spacing=Cartesian(20.1, 20.1, 0),
                       offset=Location(rx=180, ry=0, rz=90))
filter_grid_large = Grid(Location(x=143.9, y=103.9, z=90, rx=0, ry=0, rz=0), rows=3, columns=4,
                         spacing=Cartesian(20.2, 20.2, 0),
                         offset=Location(rx=-180, ry=0, rz=90))
filter_grid_small = Grid(Location(x=144.5, y=164.1, z=90.6, rx=0, ry=0, rz=0), rows=3, columns=4,
                         spacing=Cartesian(20.2, 20.2, 0),
                         offset=Location(rx=-180, ry=0, rz=90))
tube_grid = Grid(Location(x=122.0, y=-306.5, z=47.99, rx=0, ry=0, rz=0), rows=3, columns=6,
                 spacing=Cartesian(-19.9, 25.2, 0), offset=Location(rx=178.9, ry=0.9612, rz=-175.9))
tube_grid_dosing = Grid(Location(x=121.8, y=-211.0, z=140, rx=0, ry=0, rz=0), rows=3, columns=6,
                        spacing=Cartesian(-19.7, 25.2, 0), offset=Location(rx=180, ry=0, rz=0))
filter_handling = FilterHandling(cam=None, ur=arm, thermoshaker=ts,
                                 cap_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/snap_cap.script',
                                 shaker_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/shakertestnew2.script',
                                 common_zone_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/shakertestnew2.script',
                                 shaker_grid=shaker_grid,
                                 cup_grid=cup_grid, joint_velocity=50, linear_velocity=100, )
vial_handling = VialHandling(ur=arm, thermoshaker=ts, camera=camera,
                             shaker_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/shaker.script',
                             vial_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/vials.script',
                             hplc_vial_grid_front=hplc_vial_grid_front, hplc_vial_grid_back=hplc_vial_grid_back,
                             shaker_grid=shaker_grid, cup_grid=cup_grid, filter_grid_large=filter_grid_large,
                             filter_grid_small=filter_grid_small, joint_velocity=60, linear_velocity=100)
somatic = SamplomaticHandling(ur=arm, samplomatic=samplomatic,
                              samplomatic_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/samplomatic.script',
                              hplc_vial_grid_dosing_front=hplc_vial_grid_dosing_front,
                              hplc_vial_grid_dosing_back=hplc_vial_grid_dosing_back, cup_grid_dosing=cup_grid_dosing,
                              shaker_grid_dosing=shaker_grid_dosing, tube_grid_dosing=tube_grid_dosing,
                              solvent_grid=stock_grid, joint_velocity=60,
                              linear_velocity=100)
kin = KinovaGen3Arm()
kinova = ArmManager(arm=kin, action_sequences=sequences, sequence_names=sequence_names)
kinova.hplc_exchange_home()
hplc_comm = HPLC_Comm(ip='137.82.65.97', port=29620)
hplc_handling = HplcHandling(kinova_arm=kinova, vial_handling=vial_handling, hplc_comm=hplc_comm)
shaker_handling = ShakerHandling(ts=ts, hplc_handling=hplc_handling)
# centrifuge vision
centrifuge_angle_measurement = VisionDotAngleMeasurement(relay,
    'C:/Users/User/PycharmProjects/self_driving_solubility/ur/components/vision_angle_measurement/roi_selection.json',
    'C:/Users/User/PycharmProjects/self_driving_solubility/ur/components/vision_angle_measurement/colour_limits.yaml', camera_index=1)
centrifuge = Centrifuge(arm, centrifuge_angle_measurement, vial_handling=vial_handling,
                        centrifuge_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/centrifuge_new.script',
                        vial_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/cent_tube.script',
                        tube_grid=tube_grid, relay=relay)
data_handling = DataHandling()
quantos_handling = QuantosHandling(ur=arm, quantos=quan, quantos_sequence_file=QUANTOS_SEQUENCE_FILE)
# middlebox: COM28
# no middlebox: COM22
capper = Capper("COM38")
cap_handling = CapHandling(ur=arm, capper=capper, capper_sequence_file=CAPPER_SEQUENCE_FILE)
stir_handling = StirHandling(ur=arm, stirrer=stirrer, stirrer_sequence_file=STIR_SEQUENCE_FILE)

class UserFunction:
    def __init__(self):
        self.vial_handling = vial_handling
        self.quantos_handling = quantos_handling

    def weigh_sample(self, shaker_index):
        self.vial_handling.vial_from_shaker(shaker_index=shaker_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.cup_to_shaker(shaker_index=shaker_index)
        return vial_solvent_mass

    def weigh_diluent_vial(self, diluent_vial_index):
        self.vial_handling.vial_from_tray(vial_index=diluent_vial_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.vial_to_tray(vial_index=diluent_vial_index)
        return vial_solvent_mass

    def test(self):
        pass

user_function = UserFunction()
gui_functions = ["vial_handling","somatic", 'shaker_handling','quantos_handling',
                 # 'cap_handling',
                 "centrifuge",'stir_handling','filter_handling','ts','samplomatic',
                 'kinova', 'hplc_handling',
                 # "weigh_diluent_vial",
                 "user_function"
                 ]

instruments = {}
instruments['kin'] = kin
instruments['ts'] = ts
instruments['arm'] = arm
instruments['stirrer'] = stirrer
# instruments


if __name__ == "__main__":

#     hplc_comm.send_tcp_message('solubility_15')
#     time.sleep(60)
#     hplc_handling.run_blank()
#     time.sleep(120)
#     vial_list = ['B1','B2','B3','B4','B5','B6','B7','C1','C2','C3','C4','C5','C6','D1','D2']
#     for vial in vial_list:
#         hplc_handling.run_current_vial(vial_index=vial, hplc_runtime=6.25)
#     time.sleep(10*60)
#     hplc_handling.process_data_and_send_csv()
#     hplc_handling.return_last_vial()
#
#     exit()
#
#
#
#     for i in range(5):
#         vial_handling.vial_from_tray('A1')
#         vial_handling.cup_to_shaker('D1')
#         vial_handling.vial_from_shaker(shaker_index='D1')
#         vial_handling.home()
#         quantos_handling.vial_to_holder(vial_type='hplc_in')
#         quantos_handling.vial_from_holder(vial_type='hplc_out')
#         vial_handling.home()
#         vial_handling.cup_to_shaker(shaker_index='D1')
#         vial_handling.vial_from_shaker('D1')
#         vial_handling.vial_to_tray('A1')
#     exit()
#
#     import pandas as pd
#     from ur.configuration import deck_consumables
#     from scipy import stats
#     import json
#     from datetime import datetime
#
#     solvent = 'heptane'
#
#     stock_index = deck_consumables.get_stock_info(solvent)[2]
#     print(stock_index)
#
#     samplomatic.prime(0.4)
#     somatic.grab_samplomatic()
#     somatic.aspirate_stock_solvent(volume_ml=0.5, stock_index = stock_index)
#     somatic.return_samplomatic()
#     samplomatic.prime(0.4)
#     exit()

## above commented out Nov. 9

    # for i in range(20):
    #     position_list = ['A1', 'A2', 'A3', 'A4', 'B1', 'B2', 'B3', 'B4']
    #     for p in position_list:
    #         vial_handling.cup_from_tray(cup_index= p)
    #         vial_handling.cup_to_aligner()
    #         vial_handling.cup_from_aligner()
    #         vial_handling.cup_to_tray(p, from_shaker=False)

    '''
    CHANGE OTHER INSTANCES OF PUSH FILTER TO THIS LAYOUT
    '''

    # for i in range(8):
    #     cup_position_list = ['A1', 'A2', 'A3', 'A4','B1','B2','B3','B4']
    #     filter_position_list = ['A1', 'A2', 'A3', 'B1','B2','B3','C1','C2']
    #     shaker_position_list = ['A1', 'B1', 'C1','D1','A3','B3','C3','D3']
    #
    #     cup_position = cup_position_list[i]
    #     filter_position = filter_position_list[i]
    #     shaker_position = shaker_position_list[i]
    #
    #
    #     vial_handling.cup_from_tray(cup_index= cup_position)
    #     vial_handling.cup_to_aligner()
    #     vial_handling.filter_from_tray(filter_index= filter_position, large_filter=True)
    #     vial_handling.align_filter()
    #     vial_handling.insert_filter_lightly()
    #     vial_handling.push_filter(fill_line=True)
    #     vial_handling.cup_from_aligner()
    #     vial_handling.cup_to_shaker(shaker_index= shaker_position, vial_type='push')
    #
    # exit()


    # vial_index = ['B1','B2','B3','B4','B5','B6','B7','C1','C2','C3']

    # for i in vial_index:
        # hplc_handling.run_current_vial(vial_index=i, hplc_runtime=1)

    # for i in range(5):
    #     centrifuge.tube_from_tray('A1')
    #     centrifuge.tube_to_shaker()

        # ts.start_shaking()
        # time.sleep(10)
        # ts.stop_shaking()

        # centrifuge.tube_from_shaker()
        # centrifuge.tube_to_tray('A1')
        # centrifuge.push_tube_down('A1')

    # exit()
    # cups = ['A4','B1','B2','B3','B4']
    # somatic.grab_samplomatic()
    # for i in range(len(cups)):
    #     for x in range(3):
    #         somatic.draw_sample_from_cup(volume_ml=0.001, sample_index=cups[i])
    # somatic.return_samplomatic()
    # exit()
    # quantos_handling.home_z_stage()
    # quantos_handling.open_all_doors()
    # for i in range(10):
    #     quantos_handling.holder_to_quantos(test_mode=True)
    #     quantos_handling.holder_from_quantos(test_mode=True)

    # exit()
    # samplomatic.prime(3)
    # cups = ['A1','A2','A3','A4','B1','B2','B3','B4','C1','C2','C3','C4','D1','D2','D3','D4']
    cups = ['A1','A2','A3','A4','B1','B2','B3','B4','C1','C2','C3','C4']
    filters = ['A1', 'A2', 'A3', 'B1','B2','B3','C1','C2','C3','D1','D2','D3']
    shakers = ['A1', 'B1', 'C1','D1','A2','B2','C2','D2','A3','B3','C3','D3']


    for i in range(len(cups)):
        somatic.grab_samplomatic()
        somatic.aspirate_stock_solvent(volume_ml=0.3, stock_index='bottle')
        somatic.dispense_solvent_in_cup(volume_ml=0.3, sample_index=cups[i])
        somatic.return_samplomatic()

        vial_handling.cup_from_tray(cup_index=cups[i])
        vial_handling.cup_to_aligner()
        vial_handling.filter_from_tray(filter_index=filters[i], large_filter=True)
        vial_handling.align_filter()
        vial_handling.insert_filter(fill_line=False)
        vial_handling.push_filter(fill_line=True)
        vial_handling.cup_from_aligner()
        vial_handling.cup_to_shaker(shaker_index=shakers[i], vial_type='push')
    #
    #
    # # for i in cups:
    # #     vial_handling.cup_from_tray(cup_index=i)
    # #     vial_handling.cup_to_tray(cup_index=i,from_shaker=False)
    #
    exit()

    vials = ['A4','A5']

    for i in vials:
        vial_handling.home()
        vial_handling.vial_from_tray(i, grab_cap=True)
        centrifuge.home()
        cap_handling.uncap()
        cap_handling.vial_from_capper()
        cap_handling.vial_to_holder()
        quantos_handling.dose_with_quantos(solid_weight=5, vial_type='hplc')
        centrifuge.home()
        cap_handling.vial_from_holder()
        cap_handling.vial_to_capper()
        cap_handling.cap()
        cap_handling.home()
        vial_handling.home()

        vial_handling.vial_to_tray(i, grab_cap=True)
    exit()
    # from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, \
    #     quantos_handling, hplc_handling, centrifuge, shaker_handling, cap_handling

    # vial_handling.take_picture_of_vial('A1','A1',25)
    # exit()

    print("finished deck")
    # somatic.grab_samplomatic()
    # somatic.draw_sample_from_shaker(0,'A1')
    # somatic.dispense_hplc_grid('E1')
    # somatic.return_samplomatic()
    # somatic.grab_samplomatic()
    # somatic.dispense_hplc_grid(vial_index='B1',volume_ml=0)
    # somatic.dispense_hplc_grid(vial_index='F1',volume_ml=0)
    # somatic.return_samplomatic()
    # vial_handling = VialHandling(ur=arm, thermoshaker=ts,
    #                              shaker_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/shaker.script',
    #                              vial_sequence_file='C:/Users/User/PycharmProjects/self_driving_solubility/ur/configuration/sequences/vials.script',
    #                              hplc_vial_grid=hplc_vial_grid,
    #                              shaker_grid=shaker_grid, cup_grid=cup_grid, filter_grid_large=filter_grid_large,
    #                              filter_grid_small=filter_grid_small, joint_velocity=60, linear_velocity=100)

    # hplc_handling = HplcHandling(kinova_arm=kinova, vial_handling=vial_handling, hplc_relay=hplc_relay)

    # vial_handling.push_shaker()

    # vial_index = ['B1','B2','B3','B4','B5','B6','B7','C1','C2','C3']

    # somatic.grab_samplomatic()
    # for vial in vial_index:
    #     somatic.dispense_hplc_grid(vial,0)
    # somatic.return_samplomatic()

    # for vial in vial_index:
    #     vial_handling.vial_from_shaker(vial)
    #     vial_handling.home()
    #     quantos_handling.vial_to_holder('hplc_in')
    #     quantos_handling.vial_from_holder('hplc_out')
    #     vial_handling.home()
    #     vial_handling.cup_to_shaker(vial)

    # exit()

    # vials = ['C6','D1', 'D2', 'D3', 'D4']

    # for v in range(len(vials)):
    #     # hplc_handling.run_current_vial(vial_index=vials[v], hplc_runtime=1)
    #     hplc_handling.add_to_queue_and_run_if_ready(vial_index=vials[v],hplc_runtime=2)
    #     time.sleep(45)
    # hplc_handling.(hplc_runtime=2)

    # quantos_handlingrun_remainder_in_queue.open_side_door()
    # time.sleep(1)
    # quantos_handling.close_side_door()
    # stir_handling.stirrer.start_stirring()
    # stir_handling.stirrer.stop_stirring()
    #
    # cap_handling.capper.open()
    # cap_handling.capper.close()
    #
    # # samplomatic.move_needle_down()
    # ts.start_shaking()
    # ts.stop_shaking()
    # quan.front_door_position="open"
    # quan.front_door_position="close"

    # vial_handling.vial_to_tray()
    #vials = ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'B1', 'B2', 'B3']

    # for i in range(5):
    #     centrifuge.remove_cover()
    #     centrifuge.place_cover()


    # vial_handling.vial_from_tray(vial_index='A1')
    # vial_handling.to_handsoff()
    # vial_handling.from_handsoff()
    # vial_handling.to_handsoff('front')
    # vial_handling.from_handsoff('front')
    # quantos_handling.to_left_home()
    # centrifuge.run_centrifuge(30)
    # for i in range(10):
    #     vial_handling.vial_from_tray(vial_index=vials[i], grab_cap=True)
    #     quantos_handling.to_left_home()
    #     cap_handling.uncap()
    #     cap_handling.vial_from_capper(cap_handling.cap_on)
    #     # cap_handling.vial_to_holder(cap_handling.cap_on)
    #
    #     # quantos_handling.switch_to_horizontal()
    #     # quantos_handling.weigh_with_quantos()
    #     # quantos_handling.dose_with_quantos(5,vial_type='hplc')
    #
    #     # quantos_handling.switch_to_vertical()
    #     # cap_handling.vial_from_holder(cap_handling.cap_on)
    #     cap_handling.vial_to_capper(cap_handling.cap_on)
    #     cap_handling.cap()
    #     quantos_handling.to_left_home()
    #     vial_handling.vial_to_tray(vial_index=vials[i])
    #
    # # quantos_handling.weigh_with_quantos()
    # # quantos_handling.holder_to_quantos()
    # # quantos_handling.holder_from_quantos()
    #
    #
    #
    # sys.exit()

print('ur_deck done')
