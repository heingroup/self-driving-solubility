# from ur.configuration import ur_deck
from north_robots.components import GridTray, Location
import pandas as pd
import time

# important paths to files
stock_info_path = r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\stock_info.csv'
cup_info_path = r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\cup_info.csv'
hplc_vial_info_path = r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\hplc_vial_info.csv'
shaker_info_path= r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\shaker_location_info.csv'
filter_info_path = r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\filter_info.csv'
tube_info_path = r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\tube_info.csv'
solid_info_path=r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\solid_info.csv'
kinova_tray_info_path=r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\kinova_tray.csv'
handoff_info_path=r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\handoff_info.csv'
hplc_info_path=r'C:\Users\User\PycharmProjects\self_driving_solubility\ur\configuration\consumables_config\hplc_info.csv'

stocks_df = pd.read_csv(stock_info_path)
cups_df = pd.read_csv(cup_info_path)
hplc_vial_df = pd.read_csv(hplc_vial_info_path)

def read_stocks_file():
    global stocks_df
    stocks_df = pd.read_csv(stock_info_path)

class Chemical:
    def __init__(self,
                 name: str,
                 density: float = None,  # g/mL
                 formula: str = None
                 ):
        self.name = name
        self.density = density
        self.formula = formula


class Container:
    def __init__(self,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        self.max_volume = max_volume
        self.safe_volume = safe_volume
        self.current_volume = current_volume


class DeckContainer(Container):
    def __init__(self,
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        """
        Some kind of container on the deck
        :param tray:
        :param index:
        :param capped:
        """
        super().__init__(max_volume=max_volume,
                         safe_volume=safe_volume,
                         current_volume=current_volume,
                         )
        self.tray: GridTray = tray
        self.index: str = index
        self.capped: bool = capped


class ChemicalContainer(Chemical, DeckContainer):
    def __init__(self,
                 name: str,
                 density: float,  # g/mL
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        Chemical.__init__(self,
                          name=name,
                          density=density)
        DeckContainer.__init__(self,
                               tray=tray,
                               index=index,
                               capped=capped,
                               max_volume=max_volume,
                               safe_volume=safe_volume,
                               current_volume=current_volume,
                               )

def get_clean_cup():
    cups = pd.read_csv(cup_info_path)
    cup = cups[(cups['used'] == False)].first_valid_index()
    if cup is None:
        print("No clean vials, please refill")
        return None
    cup_index = cups.loc[cup, 'cup_index']
    cups.loc[cup, 'used'] = True

    while True:
        try:
            cups.to_csv(cup_info_path, index=False)
            break
        except PermissionError as e:
            time.sleep(1)
    return cup_index
# function to organize vials on the deck
def get_clean_vial(with_stirbar: bool = False, insert: bool = False):
    # find which vial to use
    vials = pd.read_csv(hplc_vial_info_path)
    #print(vials)
    # find first clean vial row
    vial = vials[(vials['used'] == False) & (vials['stirbar'] == with_stirbar) & (vials['insert'] == insert)].first_valid_index()# row index in dataframe
    #print(vial)
    if vial is None:
        # no clean vials
        return None, None
    # read the index and capped status
    vial_index = vials.loc[vial, 'vial_index']
    vial_cap_status = vials.loc[vial, 'capped']
    vials.loc[vial, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            vials.to_csv(hplc_vial_info_path, index=False)
            break
        except PermissionError as e:
            time.sleep(1)
    return vial_index, vial_cap_status

def get_vial_mass(vial_index: str):
    # find which vial to use
    vials = pd.read_csv(hplc_vial_info_path)
    mass = float(vials.loc[vials['vial_index'] == vial_index, 'mass'])
    return mass

def get_filter_mass(filter_index: str, large_filter: bool = True):
    filters = pd.read_csv(filter_info_path)
    mass = float(filters.loc[(filters['filter_index'] == filter_index) & (filters['large_filter'] == large_filter), 'mass'])
    return mass

def save_vial_mass(vial_index: str, mass: float):
    # Save in mg
    vials = pd.read_csv(hplc_vial_info_path)
    vials.loc[vials['vial_index'] == vial_index, 'mass'] = mass
    vials.to_csv(hplc_vial_info_path, index=False)

def find_clean_vials(insert: bool = False):
    vials = pd.read_csv(hplc_vial_info_path)
    clean_vials = vials[(vials['used'] == False) & (vials["insert"] == insert)]
    clean_vial_indexes = clean_vials['vial_index'].to_list()
    return clean_vial_indexes

# function to organize vials on the deck
def get_clean_filter(large_filter: bool = True):
    # find which vial to use
    filters = pd.read_csv(filter_info_path)
    # find first clean vial row
    filter = filters[(filters['used'] == False) & (filters['large_filter'] == large_filter)].first_valid_index()# row index in dataframe
    if filter is None:
        # no clean tubes
        # print('no clean tubes')
        return None
    # read the index and capped status
    filter_index = filters.loc[filter, 'filter_index']
    filters.loc[filter, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            filters.to_csv(filter_info_path, index=False)
            return filter_index
        except PermissionError as e:
            filter_index

def get_clean_tube(predosed: bool = False, stirbar: bool = False):
    tubes = pd.read_csv(tube_info_path)
    tube = tubes[(tubes['used'] == False) & (tubes['predosed'] == predosed) & (tubes['stirbar'] == stirbar)].first_valid_index()
    if tube is None:
        print("No clean tubes, please refill")
        return None
    tube_index = tubes.loc[tube, 'tube_index']
    tubes.loc[tube, 'used'] = True

    while True:
        try:
            tubes.to_csv(tube_info_path, index=False)
            break
        except PermissionError as e:
            time.sleep(1)
    return tube_index
# function to organize vials on the deck


# function to organize vials on the deck
def get_empty_shaker_location(type: str = 'sample'):
    vial = None
    # find which vial to use
    vials = pd.read_csv(shaker_info_path)
    if type=='cup':
        # find first clean vial row
        vial = vials[(vials['used'] == False)& (vials['cup'] == True)].first_valid_index()# row index in dataframe

    if type=='sample':
        # find first clean vial row
        vial = vials[(vials['used'] == False)& (vials['cup'] == False)& (vials['filter'] == False)].first_valid_index()# row index in

    if type=='clean_assembly':
        # find first clean vial row
        vial = vials[(vials['used'] == False)& (vials['cup'] == True)& (vials['filter'] == True)].first_valid_index()# row index in dataframe
        # associate it with the last sample picked
        sample_vial = vials[(vials['used'] == True)& (vials['cup'] == False)& (vials['filter'] == False)].last_valid_index()# row index in dataframe
        sample_index = vials.loc[sample_vial, 'vial_index']
        vials.loc[vial, 'associatedwith'] = sample_index
    if vial is None:
        # no clean vials
        # print('no clean vials')
        return None, None
    vial_index = vials.loc[vial, 'vial_index']
    weight = vials.loc[vial,'weight']
    vials.loc[vial, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            vials.to_csv(shaker_info_path, index=False)
            if type == 'clean_assembly':
                return vial_index, weight
            else:
                return vial_index
        except PermissionError as e:
            vial_index

def handoff_location(to_hplc: bool = True, ur_move: bool=True):
    # HOW TO USE:
    # If UR is moving vial from the tray to the handoff, set to_hplc to true. If it is moving a vial from the handoff
    # to the tray, set to false
    handoffs = pd.read_csv(handoff_info_path)
    # find first clean vial row
    if to_hplc:
        handoff = handoffs[(handoffs['occupied'] == False) & (handoffs['to_hplc'] == to_hplc)].first_valid_index()
    else:
        handoff = handoffs[(handoffs['occupied'] == True) & (handoffs['to_hplc'] == to_hplc)].first_valid_index()

    if handoff is None:
        # no clean tubes
        # print('no clean tubes')
        return None
    # read the index and capped status
    handoff_loc = handoffs.loc[handoff, 'location']
    if ur_move and to_hplc:
        handoffs.loc[handoff, 'occupied'] = True
        handoffs.loc[handoff, 'to_hplc'] = True
    elif ur_move and not to_hplc:
        handoffs.loc[handoff, 'occupied'] = False
        handoffs.loc[handoff, 'to_hplc'] = False
    elif not ur_move and to_hplc:
        handoffs.loc[handoff, 'occupied'] = False
        handoffs.loc[handoff, 'to_hplc'] = False
    elif not ur_move and not to_hplc:
        handoffs.loc[handoff, 'occupied'] = True
        handoffs.loc[handoff, 'to_hplc'] = False
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            handoffs.to_csv(handoff_info_path, index=False)
            return handoff_loc
        except PermissionError as e:
            handoff_loc


def hplc_location(to_hplc: bool = True):
    # HOW TO USE:

    hplc_locs = pd.read_csv(hplc_info_path)
    # find first clean vial row
    if to_hplc:
        handoff = handoffs[(handoffs['occupied'] == False) & (handoffs['to_hplc'] == to_hplc)].first_valid_index()
    else:
        handoff = handoffs[(handoffs['occupied'] == True) & (handoffs['to_hplc'] == to_hplc)].first_valid_index()

    if handoff is None:
        # no clean tubes
        # print('no clean tubes')
        return None
    # read the index and capped status
    handoff_loc = handoffs.loc[handoff, 'location']
    if to_hplc:
        handoffs.loc[handoff, 'occupied'] = True
    if not to_hplc:
        handoffs.loc[handoff, 'occupied'] = False
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            handoffs.to_csv(handoff_info_path, index=False)
            return handoff_loc
        except PermissionError as e:
            handoff_loc



def get_stock_info(name: str = 'acetone'):
    """
    Example use:
        density, tray, index, capped = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    if name == 'None':
        return None, None, None, None

    else:
        # find which vial to use
        stocks = pd.read_csv(stock_info_path)
        # print row which contains that name
        df= stocks[stocks['name'].str.match(name)]
        #return its info
        print(df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0])
        return df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0]



def get_stock_data(name: str = 'acetone'):
    """
    Example use:
        index, current_volume = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    print(df)
    #return its info
    print(df.loc[:,'index'].iloc[0], df.loc[:,'current volume'].iloc[0])
    return df.loc[:,'index'].iloc[0], df.loc[:,'current volume'].iloc[0]

def update_stock_volume(name: str= 'acetone', addition_volume: float = 0):
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    update_volume=df.loc[:, 'current volume'].iloc[0]-addition_volume
    stocks.loc[stocks.name==name,'current volume']=update_volume
    stocks.to_csv(stock_info_path, index=False)
    return

def get_calibration_info(name: str = 'THF'):
    """
    Example use:
        R2, slope, int_y = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that index
    df= stocks[stocks['name'].str.match(name)]
    print(df)
    #return its info
    print(df.loc[:,'slope'].iloc[0], df.loc[:,'y_int'].iloc[0], df.loc[:,'slope_sample'].iloc[0], df.loc[:,'y_int_sample'].iloc[0])
    return df.loc[:,'slope'].iloc[0], df.loc[:,'y_int'].iloc[0], df.loc[:,'slope_sample'].iloc[0], df.loc[:,'y_int_sample'].iloc[0]

def get_kinova_hplc_tray_info(solid: str, solvent: str, solubility: float, index: str):
    """function to keep track of info of the sample passed to kinova deck for hplc, returns next available spot row & column """
    #find which vial to use
    tray_location = pd.read_csv(kinova_tray_info_path)
    tray_location.iloc[-1, 2] = solid
    tray_location.iloc[-1, 3] = solvent
    tray_location.iloc[-1, 4] = solubility
    tray_location.iloc[-1, 5] = index
    if int(tray_location.iloc[-1, 0])>=1:
        if int(tray_location.iloc[-1, 1])>1:
            tray_location= tray_location.append({'row': tray_location.iloc[-1,0],'column': tray_location.iloc[-1,1]-1,
                              }, ignore_index=True)
        else:
            if tray_location.iloc[-1,0]-1 == 0:
                print("empty")

            else:
                tray_location= tray_location.append({'row': tray_location.iloc[-1,0]-1,'column': 6},ignore_index=True)

    tray_location.to_csv(kinova_tray_info_path, sep=',', index=False, mode='w')
    return tray_location.iloc[tray_location.shape[0]-1, 0],tray_location.iloc[tray_location.shape[0]-1, 1]


def get_solid_info(name: str = 'sample'):
    # find which vial to use
    solids = pd.read_csv(solid_info_path)
    # print row which contains that name
    df= solids[solids['name'].str.match(name.lower(),case = False)]
    print(df)
    #return its info
    print( df.loc[:,'index'].iloc[0])
    return df.loc[:,'index'].iloc[0]


# def get_stock(name: str) -> ChemicalContainer:
#     """
#     Return a ChemicalContainer for a chemical using information from the stock csv file
#     :param name: name of chemical
#     :return:
#     """
#     read_stocks_file()
#     info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
#     name = info['name'].values[0]
#     density = info['density'].values[0]
#     tray = f"{info['tray'].values[0]}"
#     index = info['index'].values[0]
#     capped = info['capped'].values[0]
#     max_volume = info['max volume'].values[0]
#     safe_volume = info['safe volume'].values[0]
#     current_volume = info['current volume'].values[0]
#     if tray == 'deck.stock_grid':
#         tray = ur_deck.stock_grid
#     stock = ChemicalContainer(name=name,
#                               density=density,
#                               tray=tray,
#                               index=index,
#                               capped=capped,
#                               max_volume=max_volume,
#                               safe_volume=safe_volume,
#                               current_volume=current_volume,
#                               )
#     return stock


def update_stock_file(name: str,
                      column: str,
                      value):
    """

    :param str, name: stock name
    :param str, column: column heading
    :param value: new value
    :return:
    """
    read_stocks_file()
    info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
    row_index = info.index[0]
    stocks_df.at[row_index, column] = value
    try:
        stocks_df.to_csv(stock_info_path, sep=',', index=False, mode='w')
        time.sleep(1)
    except PermissionError as e:
        pass


if __name__ == '__main__':
    get_solid_info('ice')
    a,b,index,c=get_stock_info('acetone')
    print(a,b,index,c)
    a,b=get_stock_data('acetone')
    print(a,b)