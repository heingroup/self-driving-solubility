from hein_robots.robotics import Location, Cartesian
from hein_robots.universal_robots.ur3 import UR3Arm
from ur.components.samplomatic.samplomatic import Samplomatic
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from north_devices.pumps.tecan_cavro import TecanCavro
from heinsight.vision_utilities.camera import Camera
from ika import Thermoshaker
from hein_robots.grids import Grid
import cv2
import os
import time
from pathlib import Path
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage

class SamplomaticHandling:

    def __init__(self, ur: UR3Arm, samplomatic: Samplomatic, samplomatic_sequence_file: str,
                  hplc_vial_grid_dosing_front: Grid, hplc_vial_grid_dosing_back: Grid,
                 cup_grid_dosing: Grid, shaker_grid_dosing: Grid,
                 tube_grid_dosing: Grid,solvent_grid: Grid, joint_velocity: float = 60,
                 linear_velocity: float = 80, airgap_volume: float = 0.05):
        self.ur = ur
        self.samplomatic = samplomatic
        self.samplomatic_sequence = URScriptSequence(samplomatic_sequence_file)
        self.hplc_vial_grid_dosing_front = hplc_vial_grid_dosing_front
        self.hplc_vial_grid_dosing_back = hplc_vial_grid_dosing_back
        self.cup_grid_dosing = cup_grid_dosing
        self.shaker_grid_dosing = shaker_grid_dosing
        self.tube_grid_dosing = tube_grid_dosing
        self.solvent_grid = solvent_grid
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity
        self.airgap_volume = airgap_volume
        self.front_hplc_list = ['A','B','C','D']
        self.back_hplc_list = ['E','F','G','H','I','J','K']
        self.vial_grid = self.hplc_vial_grid_dosing_front
        self.above_hplc = 'above_hplc_frnt'


    def grab_samplomatic(self):
        self.samplomatic.home_needle()
        self.ur.open_gripper(0)
        self.ur.move_joints(self.samplomatic_sequence.joints['smp_approach'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['smp_approach'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_engage'],
                                     velocity=self.linear_velocity)
        self.ur.open_gripper(0.45)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_above'],
                                 velocity=self.linear_velocity)

    def return_samplomatic(self):
        self.ur.move_joints(self.samplomatic_sequence.joints['smp_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_approach'],
                                 velocity=self.linear_velocity)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_engage'],
                                 velocity=40)
        self.ur.open_gripper(0)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_engage'] + Location(z=1),
                                 velocity=40)
        self.ur.move_to_location(self.samplomatic_sequence.locations['smp_approach'],
                                 velocity=self.linear_velocity)

    def grid_location_from_vial_index(self, vial_index):

        # Two tray exist, front and back. The first row of the back tray starts at 'E' instead of 'A' (see ur_deck.py)
        # This function will check the vial index letter to determine which tray it should interact with

        if vial_index[0] in self.front_hplc_list:
            self.vial_grid = self.hplc_vial_grid_dosing_front
            self.above_hplc = 'above_hplc_frnt'
        elif vial_index[0] in self.back_hplc_list:
            self.vial_grid = self.hplc_vial_grid_dosing_back
            self.above_hplc = 'above_hplc_back'


    def dispense_hplc_grid(self, vial_index: str = 'A1', volume_ml: float = 0.1, vial_depth: int = 25, backend: bool=True,
                           to_bottom: bool = False):

        # ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=vial_index)
        if to_bottom:
            vial_depth = 41
        self.ur.move_joints(self.samplomatic_sequence.joints[self.above_hplc], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index])
        if backend:
            self.samplomatic.dispense_backend_solvent(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=2)
        if not backend:
            self.samplomatic.dispense_sample(vial_depth=vial_depth, volume_ml=volume_ml + (self.airgap_volume / 2))
        self.ur.move_to_location(self.samplomatic_sequence.locations[self.above_hplc] + Location(z=10))
        if not backend:
            self.samplomatic.dispense_sample(vial_depth=0, volume_ml=(self.airgap_volume / 2))

    def dispense_quantos_holder(self, volume_ml: int = 0.1, vial_depth: int = 20, backend: bool=False):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['quantos_holder'], velocity=self.joint_velocity)
        if backend:
            self.samplomatic.dispense_backend_solvent(vial_depth=vial_depth, volume_ml=volume_ml)
        if not backend:
            self.samplomatic.dispense_sample(vial_depth=vial_depth, volume_ml=volume_ml + (self.airgap_volume / 2))
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        if not backend:
            self.samplomatic.dispense_sample(vial_depth=0, volume_ml=(self.airgap_volume / 2))

    def draw_quantos_holder(self, volume_ml: int = 0.1, vial_depth: int = 37, velocity_ml: int = 7):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['quantos_holder'], velocity=self.joint_velocity)
        self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)











    def dispense_quantos_holder(self, volume_ml: int = 0.1, vial_depth: int = 37, velocity_ml: int = 7):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['quantos_holder'], velocity=self.joint_velocity)
        self.samplomatic.dispense_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)

    def push_out_magnet(self, volume: int = 1):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['magnet'], velocity=self.joint_velocity)
        self.samplomatic.dispense_sample(vial_depth=20, volume_ml=volume, pump_velocity=7)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)



















    #USE THIS
    def aspirate_stock_solvent(self, volume_ml, stock_index: str = 'A1', velocity_ml: int = 7,
                               vial_depth: int = 75, airgap: bool = True):
        if airgap:
            self.samplomatic.get_airgap(volume_ml=self.airgap_volume)


        if stock_index == 'bottle':
            self.ur.move_joints(self.samplomatic_sequence.joints['above_bottle'], velocity=self.joint_velocity)
            self.ur.move_to_location(self.samplomatic_sequence.locations['bottle_big_boi'])
            self.samplomatic.aspirate_sample(vial_depth=120, volume_ml=volume_ml, pump_velocity=velocity_ml)
            self.ur.move_to_location(self.samplomatic_sequence.locations['above_bottle'])
        else:
            self.ur.move_joints(self.samplomatic_sequence.joints['above_solvent'], velocity=self.joint_velocity)
            self.ur.move_to_location(self.solvent_grid.locations[stock_index], velocity=self.joint_velocity)
            print(self.solvent_grid.locations[stock_index])
            self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
            self.ur.move_to_location(self.samplomatic_sequence.locations['above_solvent'])

    def above_solvents(self):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_solvent'], velocity=self.joint_velocity)

    #USE THIS
    def dispense_solvent_in_cup(self, volume_ml, sample_index: str = 'A1',
                                velocity_ml: int = 7, vial_depth: int = 30):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cup_grid_dosing.locations[sample_index])
        self.samplomatic.dispense_sample(vial_depth=vial_depth, volume_ml=volume_ml + (self.airgap_volume/2),
                                         pump_velocity=velocity_ml)
        time.sleep(1)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['smp_above'], velocity=self.joint_velocity)
        self.samplomatic.dispense_sample(vial_depth=0, volume_ml=(self.airgap_volume / 2),
                                         pump_velocity=velocity_ml)

    def dispense_solvent_in_tube(self, volume_ml, sample_index: str = 'A3',
                                velocity_ml: int = 7, vial_depth: int = 15 ):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_tubes'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.tube_grid_dosing.locations[sample_index])
        self.samplomatic.dispense_sample(vial_depth=vial_depth, volume_ml=volume_ml + (self.airgap_volume/2),
                                         pump_velocity=velocity_ml)
        time.sleep(1)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_tubes'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.samplomatic.dispense_sample(vial_depth=0, volume_ml=(self.airgap_volume / 2),
                                         pump_velocity=velocity_ml)

    def draw_sample_from_tube(self, volume_ml, sample_index: str = 'A3', velocity_ml: int = 7, vial_depth: int = 51.2, airgap: bool = False):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)
        self.ur.move_joints(self.samplomatic_sequence.joints['above_tubes'], velocity=self.joint_velocity)
        if airgap:
            self.samplomatic.get_airgap(volume_ml=self.airgap_volume)
        self.ur.move_to_location(self.tube_grid_dosing.locations[sample_index])
        self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_to_location(self.samplomatic_sequence.locations['above_tubes'])
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cenfuge'], velocity=self.joint_velocity)


    def draw_sample_from_cup(self, volume_ml, sample_index: str = 'A1', velocity_ml: int = 7, vial_depth: int = 40):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cup_grid_dosing.locations[sample_index])
        self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_to_location(self.samplomatic_sequence.locations['above_cups'])

    def draw_sample_from_cup_with_airgap(self, volume_ml, sample_index: str = 'A1', velocity_ml: int = 7, vial_depth: int = 40):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.samplomatic.get_airgap(volume_ml=self.airgap_volume)
        self.ur.move_to_location(self.cup_grid_dosing.locations[sample_index])
        self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_to_location(self.samplomatic_sequence.locations['above_cups'])


    # vial_depth = 41 at bottom
    def draw_sample_from_vial(self, volume_ml, sample_index: str = 'A1', velocity_ml: int = 7, vial_depth: int = 41, to_bottom: bool = True):

        # ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=sample_index)

        self.ur.move_joints(self.samplomatic_sequence.joints[self.above_hplc], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_grid.locations[sample_index])
        if to_bottom:
            self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        if not to_bottom:
            self.samplomatic.aspirate_sample(vial_depth=37, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_to_location(self.samplomatic_sequence.locations[self.above_hplc])

    def draw_sample_from_vial_with_airgap(self, volume_ml, sample_index: str = 'A1', velocity_ml: int = 7, vial_depth: int = 41, to_bottom: bool = True):

        # ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=sample_index)

        self.ur.move_joints(self.samplomatic_sequence.joints[self.above_hplc], velocity=self.joint_velocity)
        self.samplomatic.get_airgap(volume_ml=self.airgap_volume)
        self.ur.move_to_location(self.vial_grid.locations[sample_index])
        if to_bottom:
            self.samplomatic.aspirate_sample(vial_depth=vial_depth, volume_ml=volume_ml, pump_velocity=velocity_ml)
        if not to_bottom:
            self.samplomatic.aspirate_sample(vial_depth=37, volume_ml=volume_ml, pump_velocity=velocity_ml)
        self.ur.move_to_location(self.samplomatic_sequence.locations[self.above_hplc])

    #vial_depth=46
    # TO DO increase z height for glass filter, the find new vial depth
    # or, bring back to cups
    def draw_sample_from_shaker(self, volume_ml, shaker_index: str = 'A1', velocity_ml: int = 12, vial_depth: int = 46,
                                type: str = 'glass_filter'):
        self.ur.move_joints(self.samplomatic_sequence.joints['above_shaker'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.shaker_grid_dosing.locations[shaker_index])
        if type == 'plastic_filter':
            self.samplomatic.aspirate_sample(vial_depth=41, volume_ml=volume_ml, pump_velocity=velocity_ml)
        elif type == 'glass_filter':
            self.samplomatic.aspirate_sample(vial_depth=46, volume_ml=volume_ml, pump_velocity=velocity_ml)
        elif type == 'hplc':
            self.samplomatic.aspirate_sample(vial_depth=40, volume_ml=volume_ml, pump_velocity=velocity_ml)
            # prev (march 15/22, depth = 42)
        self.ur.move_to_location(self.samplomatic_sequence.locations['above_shaker'])

        #######
        #Combined Fucntions
        #######

if __name__ == "__main__":
    hplc_vial_grid_dosing = Grid(Location(x=202.3, y=243.6, z=145.8, rx=0, ry=0, rz=0), rows=7, columns=4,
                                 spacing=Cartesian(22.3, 22, 0),
                                 offset=Location(rx=-180, ry=0, rz=180))
    stock_grid = Grid(Location(x=156.8, y=64.4, z=208), rows=2, columns=5, spacing=Cartesian(25.5, 52.4),
                      offset=Location(rx=-180, ry=0, rz=90))
    cup_grid_dosing = Grid(Location(x=-115.06, y=199.5, z=190.5, rx=0, ry=0, rz=0), rows=4, columns=6,
                           spacing=Cartesian(20.1, 20.1, 0),
                           offset=Location(rx=180, ry=0, rz=90))
    UR_arm = UR3Arm('192.168.254.88')
    samplomatic = Samplomatic(somatic_stepper_port="COM9")
    somatic = SamplomaticHandling(ur=UR_arm, samplomatic=samplomatic,
                              samplomatic_sequence_file='../../configuration/sequences/samplomatic.script',
                              hplc_vial_grid_dosing=hplc_vial_grid_dosing,cup_grid_dosing=cup_grid_dosing, solvent_grid=stock_grid, joint_velocity=60, linear_velocity=80)



    samplomatic.home_needle()
    # samplomatic.prime()
    # samplomatic.aspirate_sample(volume_ml=0.1, vial_depth=10)
    # samplomatic.dispense_sample(volume_ml=0.1, vial_depth=10)
    # somatic.grab_samplomatic()
    #somatic.aspirate_stock_solvent(stock_index='E1', volume_ml=0.1, vial_depth=10)

    # samplomatic.aspirate_sample(volume_ml=0.1, vial_depth=0)
    # somatic.dispense_hplc_grid(vial_index='A1')
    # samplomatic.aspirate_sample(volume_ml=0.1, vial_depth=0)
    # somatic.dispense_hplc_grid(vial_index='A7')
    # samplomatic.aspirate_sample(volume_ml=0.1, vial_depth=0)
    # somatic.dispense_hplc_grid(vial_index='D1')
    # samplomatic.aspirate_sample(volume_ml=0.1, vial_depth=0)
    # somatic.dispense_hplc_grid(vial_index='D7')
    # somatic.return_samplomatic()

