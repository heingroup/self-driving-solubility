from typing import List, Tuple, Dict, Union
from pathlib import Path
import yaml
import numpy as np
import cv2
import imutils
import time
from ur.components.vision_angle_measurement.roi import ROIManager, ROI  # todo change to make import work if needed
from heinsight.vision_utilities.camera import Camera
from ur.components.relay_board.relay_board import RelayBoard
from ftdi_serial import Serial


MIN_RADIUS = 2


class NoDotFoundException(Exception):
    """Could not find a dot in an image with hsv values within some hsv minimum and maximum values"""

    def __init__(self, msg=None):
        self.msg = msg

    def __str__(self):
        return self.msg


# one of the main functions
def load_images(folder_path: Path) -> List[np.ndarray]:
    """Return list of images in a folder"""
    images = []
    for path in folder_path.iterdir():
        image_path = str(path.absolute())
        image = cv2.imread(image_path)
        images.append(image)
    return images


# one of the main functions
def extract_rectangles(images: List, roi: ROI) -> List[np.ndarray]:
    extracted_rectangles = roi.extract_rectangle(*images)
    return extracted_rectangles


# one of the main functions
def load_colour_references(file_path: Path) -> Dict:
    with open(str(file_path)) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    return data


# one of the main functions
def angle_in_image(image,
                   reference_hsv_limits: Dict[str, Tuple[int, int, int]],
                   center_hsv_limits: Dict[str, Tuple[int, int, int]],
                   interest_hsv_limits: Dict[str, Tuple[int, int, int]],
                   ) -> float:
    """In an image with different coloured dots, find the clockwise angle (degrees)formed by three differently coloured
    dots. The dot colours are specified by the hsv_limits, which is a dictionary:
        {hsv_min: (int, int, int), hsv_max: (int, int, int)}
    that specify the minimum and maximum hsv values to find the coloured dot. There is a reference dot,
    a center dot, and a dot of interest. The line between the reference and center dot acts as the angle 0. The angle
    that gets returned is the clockwise angle formed by the lines center-reference and center-interest
     """
    reference_dot_center = _dot_center(image, reference_hsv_limits['hsv_min'], reference_hsv_limits['hsv_max'])
    print('found reference')
    center_dot_center = _dot_center(image, center_hsv_limits['hsv_min'], center_hsv_limits['hsv_max'])
    print('found center')
    interest_dot_center = _dot_center(image, interest_hsv_limits['hsv_min'], interest_hsv_limits['hsv_max'])
    print('found interest')
    clockwise_angle = _find_angle(reference_dot_center, center_dot_center, interest_dot_center)
    return clockwise_angle


def _dot_center(image, hsv_min: Tuple, hsv_max) -> Tuple[int, int]:
    """Return (x, y) location (origin top-left of an image) for the center of a coloured dot within the hsv limits
    in an image"""
    image_contours = _process_image_find_contours(image, hsv_min, hsv_max)
    dot_center = _find_center(image_contours)
    if dot_center is None:
        print('cannot find dot center')
        raise NoDotFoundException(
            f'Could not find a coloured dot in image within the hsv limits {hsv_min} and {hsv_max}')
    return dot_center


def _process_image_find_contours(image, hsv_min, hsv_max):
    image = _process_image(image)
    image = _mask_image(image, hsv_min, hsv_max)
    cv2.imshow('masked', image)
    cv2.waitKey(1)
    # find contours of the white pixels in the mask image
    image_contours = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    image_contours = imutils.grab_contours(image_contours)
    return image_contours


def _process_image(image):
    """Process an image (blur, convert to hsv)"""
    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    return hsv


def _mask_image(image, hsv_min, hsv_max):
    """Return a mask image (black and white image), where white pixels indicate pixels where the hsv values are
    within hsv_min and hsv_max"""
    mask = cv2.inRange(image, hsv_min, hsv_max)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    return mask


def _find_center(contours) -> Union[Tuple[int, int], None]:
    """Return a tuple of (x, y) for the location of the center of a the largest contour (origin in top-left of an
    image) given a list of contours, and if the contour is at least larger than a minimum radius"""
    if len(contours) == 0:
        return None
    contour = max(contours, key=cv2.contourArea)
    ((x, y), radius) = cv2.minEnclosingCircle(contour)
    M = cv2.moments(contour)
    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
    # only proceed if the radius meets a minimum size
    if radius > MIN_RADIUS:
        return center


def _find_angle(reference: Tuple[int, int], center: Tuple[int, int], interest: Tuple[int, int]) -> float:
    """Return angle in degrees between a reference point and some other point of interest. The center point is used as
    the origin that connects the two points. The angle returned in the clockwise angle from the center-reference
    line to the center-interest line. The tuples passed are (x, y) for x and y values from an image (top-left
    origin). This is important because the math in this function to find the relative locations of the reference and
    point of interest assumes this."""
    # use the center point as the origin, therefore need find the reference and interest points relative to the
    # center point. To find the relative x point we do reference_x - center_x, but to find the relative y point
    # we do center_y - reference_y
    relative_reference = (reference[0] - center[0], center[1] - reference[1])
    relative_interest = (interest[0] - center[0], center[1] - interest[1])
    clockwise_angle = _clockwise_angle_between(relative_reference, relative_interest)
    return clockwise_angle


# one of the main functions
def annotate_dots_angle_on_image(image,
                                 reference_hsv_limits: Dict[str, Tuple[int, int, int]],
                                 center_hsv_limits: Dict[str, Tuple[int, int, int]],
                                 interest_hsv_limits: Dict[str, Tuple[int, int, int]],
                                 ) -> np.ndarray:
    clockwise_angle = angle_in_image(image, reference_hsv_limits, center_hsv_limits, interest_hsv_limits)
    drawn_image = image.copy()

    center_image_contours = _process_image_find_contours(image, center_hsv_limits['hsv_min'],
                                                         center_hsv_limits['hsv_max'])
    reference_image_contours = _process_image_find_contours(image, reference_hsv_limits['hsv_min'],
                                                            reference_hsv_limits['hsv_max'])
    interest_image_contours = _process_image_find_contours(image, interest_hsv_limits['hsv_min'],
                                                           interest_hsv_limits['hsv_max'])
    drawn_image = _draw_contour_outline(drawn_image, center_image_contours)
    drawn_image = _draw_contour_outline(drawn_image, reference_image_contours)
    drawn_image = _draw_contour_outline(drawn_image, interest_image_contours)

    reference_dot_center = _dot_center(image, reference_hsv_limits['hsv_min'], reference_hsv_limits['hsv_max'])
    center_dot_center = _dot_center(image, center_hsv_limits['hsv_min'], center_hsv_limits['hsv_max'])
    interest_dot_center = _dot_center(image, interest_hsv_limits['hsv_min'], interest_hsv_limits['hsv_max'])
    drawn_image = _annotate_angle_on_image(drawn_image, clockwise_angle, reference_dot_center, center_dot_center,
                                           interest_dot_center)
    return drawn_image


def _draw_contour_outline(image, image_contours) -> np.ndarray:
    """Return an image with a circle drawn on to outline the largest contour in the list of contours. Contour must be
    bigger than a minimum radius"""
    if len(image_contours) == 0:
        return image
    contour = max(image_contours, key=cv2.contourArea)
    ((x, y), radius) = cv2.minEnclosingCircle(contour)
    # only proceed if the radius meets a minimum size
    if radius > MIN_RADIUS:
        cv2.circle(image, (int(x), int(y)), int(radius), (0, 255, 255), 2)
        return image


def _annotate_angle_on_image(image,
                             clockwise_angle: float,
                             reference: Tuple[int, int], center: Tuple[int, int], interest: Tuple[int, int],
                             ) -> np.ndarray:
    """Return an image, where the clockwise angle (degrees) between the line from a reference to center points,
    and a line from the interest to center points is annotated in the top-left corner of the image.
    Use the points to draw the angle on the image.
    The points are (x, y) using the top-left corner of an image as the origin"""
    drawn_image = image.copy()
    drawn_image = _draw_angle(drawn_image, center, reference, interest)
    text_height = 20
    cv2.putText(drawn_image,
                f'Angle (deg, clockwise): {round(clockwise_angle, 2)}',
                (10, text_height),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                (0, 0, 255),
                2)
    return drawn_image


def _draw_angle(image, point_1, point_2, point_3) -> np.ndarray:
    """Return an image. On the image draw an angle by connecting points 1 to 2 and points 1 to 3. Point 1 is the
    center point. Points are tuples (x, y) using the top-left corner of an image as the origin"""
    image = image.copy()
    cv2.line(image, point_1, point_2, (0, 255, 0), 2)
    cv2.line(image, point_1, point_3, (0, 255, 0), 2)
    return image


# see https://stackoverflow.com/questions/31735499/calculate-angle-clockwise-between-two-points
def _clockwise_angle_between(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return np.rad2deg((ang1 - ang2) % (2 * np.pi))


def _counterclockwise_angle_between(p1, p2):
    counter_clockwise = _clockwise_angle_between(p2, p1)
    return counter_clockwise


class VisionDotAngleMeasurementError(Exception):
    pass


class VisionDotAngleMeasurement:
    def __init__(self, relay: RelayBoard, roi_config_path: Union[str, Path], colour_limit_path: Union[str, Path], roi_name: str = 'roi',
                 camera_index: int = 0):
        self.relay = relay
        self.roi_name = roi_name
        self.roi_config_path = roi_config_path
        self.roi_manager = ROIManager()

        if Path(roi_config_path).exists():
            self.roi_manager.load_data(Path(roi_config_path))
            self.roi = self.roi_manager.roi(roi_name)

        self.colour_limits = load_colour_references(Path(colour_limit_path))

        for key, limits in self.colour_limits.items():
            limits['hsv_min'] = eval(limits['hsv_min'])
            limits['hsv_max'] = eval(limits['hsv_max'])

        self.camera = cv2.VideoCapture(camera_index, cv2.CAP_DSHOW)
        self.camera.set(cv2.CAP_PROP_BUFFERSIZE, 1)

    def get_image(self, retries: int = 10):
        while True:
            if retries <= 0:
                raise VisionDotAngleMeasurementError(f'Cannot capture image after 5 retries')

            # read a few frames to clear out any old frames in the buffer (hacky!!)
            for i in range(5):
                self.camera.read()
                time.sleep(0.1)

            ret, image = self.camera.read()

            if ret:
                return image

            time.sleep(0.1)
            retries -= 1

    def measure_angle(self, show_image: bool = False, retries: int = 20, respin: int = 3) -> float:
        image = self.get_image()
        extracted_rectangles = self.roi.extract_rectangle(image)

        if show_image:
            self.show_annotated_image(image)

        try:
            return angle_in_image(extracted_rectangles, self.colour_limits['reference'], self.colour_limits['center'],
                                  self.colour_limits['location_1'])
        except NoDotFoundException:
            if retries <= 0:
                self.relay.set_output(4, True)
                time.sleep(1)
                self.relay.set_output(4, False)
                time.sleep(10)
                retries = 20
                respin -= 1
            if respin <= 0:
                raise NoDotFoundException(f'Cannot measure angle, no dots found in image')
            time.sleep(1)
            return self.measure_angle(retries=retries - 1,respin=respin, show_image=show_image)

    def annotate_image(self, image: np.array) -> np.array:
        extracted_rectangles = self.roi.extract_rectangle(image)
        return annotate_dots_angle_on_image(extracted_rectangles, self.colour_limits['reference'],
                                            self.colour_limits['center'], self.colour_limits['location_1'])

    def show_annotated_image(self, image: np.array, window_name: str = 'Angle Measurement'):
        try:
            annotated_image = self.annotate_image(image)
            cv2.imshow(window_name, annotated_image)
        except NoDotFoundException:
            image_with_roi = self.roi.roi(image)
            cv2.imshow('FAILED MEASUREMENT', image_with_roi)

        cv2.waitKey(1)

    def select_roi(self, save: bool = False):
        image = self.get_image()

        try:
            self.roi_manager.remove_rois('roi')
        except KeyError:
            pass

        self.roi_manager.add_roi(image, 'rectangle', self.roi_name, 'Select ROI')
        self.roi = self.roi_manager.roi(self.roi_name)

        if save:
            print(Path(self.roi_config_path))
            self.roi_manager.save_data(Path(self.roi_config_path))


if __name__ == '__main__':

    camera = cv2.VideoCapture(1, cv2.CAP_DSHOW)
    ret, frame = camera.read()
    colour_limit_data_path = Path(r'colour_limits.yaml')
    roi_selection_path = Path.cwd().joinpath('roi_selection.json')
    relay = RelayBoard(Serial(device_serial='FT000001'))
    angle_measurement = VisionDotAngleMeasurement(relay, roi_selection_path, colour_limit_data_path, camera_index=1)

    angle_measurement.select_roi(True)

    while True:
        try:
            angle = angle_measurement.measure_angle(show_image=True)
            print(f'angle: {angle}')
        except NoDotFoundException:
            pass
        time.sleep(0.1)

    # angle_measurement.show_annotated_image(frame)

    # while True:
    #     roi = angle_measurement.roi_manager.roi('roi')
    #     ret, frame = camera.read()
    #     ret, frame = camera.read()
    #     try:
    #         angle = angle_measurement.measure_angle()
    #         extracted_roi = roi.extract_rectangle(frame)
    #         angle_image = angle_measurement.annotate_image(extracted_roi)
    #         frame_with_roi = roi.roi(frame)
    #         cv2.imshow('live', angle_image)
    #         cv2.imshow('full view', frame_with_roi)
    #         cv2.waitKey(1)
    #     except NoDotFoundException:
    #         cv2.destroyWindow('live')
    #         cv2.destroyWindow('full view')
    #         frame_with_roi = roi.roi(frame)
    #         cv2.imshow('Failed measurement', frame_with_roi)
    #         cv2.waitKey(0)
    #         cv2.destroyAllWindows()
    #
    #     print(f'angle: {angle}')

    #
    # colour_limit_data_path = Path(r'colour_limits.yaml')
    # dot_images_folder = Path(r'dot images')
    # result_angle_images_folder = Path(r'result angle images')

    # ----------------------------------------------------
    # todo
    # do once to select and save an roi selection to a JSON file. once there is this file it will be used to load and
    # crop images afterwards
    # rm = ROIManager()
    # rm.add_roi(roi_type=ROI.rectangle_roi,  name='roi', image=reference_image)
    # # save the selected roi image in the folder where this script is
    # roi_image = rm.rois_image(reference_image)
    # cv2.imwrite(str(Path.cwd().joinpath('roi image.png')), roi_image)
    # # save the roi manager in the folder where this script is so it can be loaded again later
    # rm.save_data(roi_selection_path)
    # ----------------------------------------------------

    # # load ROI selection to crop future images
    # rm = ROIManager()
    # rm.load_data(roi_selection_path)
    # roi = rm.roi('roi')
    #
    # # get limits for colours for the dots
    # colour_limits = load_colour_references(colour_limit_data_path)
    # reference_hsv_limits: Dict = colour_limits['reference']
    # center_hsv_limits: Dict = colour_limits['center']
    # location_1_hsv_limits: Dict = colour_limits['location_1']
    # location_2_hsv_limits: Dict = colour_limits['location_2']
    # # the tuples are loaded in as a string of the tuple, so convert into actual tuple of (int, int, int)
    # for limits in [reference_hsv_limits, center_hsv_limits, location_1_hsv_limits, location_2_hsv_limits]:
    #     limits['hsv_min'] = eval(limits['hsv_min'])
    #     limits['hsv_max'] = eval(limits['hsv_max'])
    #
    # # load images, extract roi rectangles
    # images = load_images(dot_images_folder)
    # extracted_rectangles = extract_rectangles(images, roi)
    #
    # # loop through images that things work for locations 1 and 2, save to results folder. use angle_in_image and
    # # annotate_dots_angle_on_image
    # for index, image in enumerate(extracted_rectangles):
    #     print(f'analyzing image {index}')
    #     try:
    #         angle_between_reference_location_1 = angle_in_image(image, reference_hsv_limits, center_hsv_limits, location_1_hsv_limits)
    #     except NoDotFoundException as e:
    #         print(f'No dot found for reference or location 1 in image index {index}')
    #     try:
    #         angle_between_reference_location_2 = angle_in_image(image, reference_hsv_limits, center_hsv_limits, location_2_hsv_limits)
    #     except NoDotFoundException as e:
    #         print(f'No dot found for reference or location 2 in image index {index}')
    #
    #     try:
    #         annotated_image_location_1 = annotate_dots_angle_on_image(image, reference_hsv_limits, center_hsv_limits, location_1_hsv_limits)
    #         cv2.imwrite(str(result_angle_images_folder.joinpath(f'{index} - location 1.png')), annotated_image_location_1)
    #     except NoDotFoundException as e:
    #         cv2.imwrite(str(result_angle_images_folder.joinpath(f'{index} - location 1.png')), image)
    #     try:
    #         annotated_image_location_2 = annotate_dots_angle_on_image(image, reference_hsv_limits, center_hsv_limits, location_2_hsv_limits)
    #         cv2.imwrite(str(result_angle_images_folder.joinpath(f'{index} - location 2.png')), annotated_image_location_2)
    #     except NoDotFoundException as e:
    #         cv2.imwrite(str(result_angle_images_folder.joinpath(f'{index} - location 2.png')), image)
    #
    # print('done')
