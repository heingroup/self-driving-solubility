from hein_robots.robotics import Location
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from heinsight.vision_utilities.camera import Camera
from ika import Thermoshaker
from hein_robots.grids import Grid
import cv2
import os
import time
from pathlib import Path
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage

def align_check(imagesDirectory):
    for subdir, dirs, files in os.walk(imagesDirectory):

        for file in files:
            imageFile = os.path.join(subdir, file)
            if file.endswith('png'):
                print(file)
                # im1 = im.crop((left, top, right, bottom))

                # jpgfile = Image.open(imageFile).convert('L').crop((520, 130, 1100, 900))
                jpgfile = Image.open(imageFile).convert('L').crop((520, 130, 780, 320))
                # width, height = jpgfile.size
                image_pixels = np.asarray(jpgfile)
                # plt.imshow(jpgfile)
                # plt.show()

                labeled, nr_objects = ndimage.label(image_pixels<30)
                # plt.imshow(labeled)
                # plt.show()
                print(nr_objects)
                for i in range(0,nr_objects):
                    total=sum(sum(labeled==i))
                    # print(total)
                    if total>2000 and total<10000:
                        rows, cols = np.where(labeled==i)
                        print("X: " + str(np.average(cols)))
                        print("Y: " + str(np.average(rows)))

                        plt.imshow(labeled==i)
                        plt.show()
                    # break
                        return np.average(cols),np.average(rows)

class FilterHandling:

    def __init__(self, cam: Camera, ur: UR3Arm, thermoshaker: Thermoshaker,cap_sequence_file: str, shaker_sequence_file: str,
                 common_zone_sequence_file: str, shaker_grid: Grid, cup_grid: Grid, joint_velocity: float = 60, linear_velocity: float = 200,
                 ):
        self.cam = cam
        self.ur = ur
        self.thermoshaker = thermoshaker
        self.cap_sequence = URScriptSequence(cap_sequence_file)
        self.shaker_sequence = URScriptSequence(shaker_sequence_file)
        self.common_zone_sequence = URScriptSequence(common_zone_sequence_file)
        self.shaker_grid = shaker_grid
        self.cup_grid=cup_grid
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity

    def filter_vial_from_common_zone(self):
        COVER_SEQUENCE_NAMES = ['Waypoint_1', 'Waypoint_2','Waypoint_3', 'engage', 'safe']
        self.ur.open_gripper(0.5)
        self.ur.move_to_location(self.common_zone_sequence['Waypoint_1'], velocity=self.linear_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'],velocity=self.joint_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['safe'],velocity=self.joint_velocity)
        #self.ur.move_to_location(self.common_zone_sequence['safe'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['engage']+Location(z=8), velocity=self.linear_velocity)
        # self.ur.open_gripper(0.85)
        self.ur.open_gripper(0.80)
        self.ur.move_to_location(self.common_zone_sequence['safe'], velocity=self.linear_velocity)

    def filter_vial_to_common_zone(self):
        COVER_SEQUENCE_NAMES = ['Waypoint_1', 'Waypoint_2','Waypoint_3', 'engage', 'safe']
        self.ur.move_to_location(self.common_zone_sequence['Waypoint_1'], velocity=self.linear_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'],velocity=self.joint_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['safe'],velocity=self.joint_velocity)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=30), velocity=20)
        # arm = UR3Arm('192.168.254.88')
        # arm.move_joints([0, 0, 0, 0, 0, 90], velocity=30, acceleration=20, relative=True)
        self.ur.move_to_location(self.common_zone_sequence['engage']+Location(z=10), velocity=20)
        self.ur.open_gripper(0.50)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=45), velocity=20)
        self.ur.open_gripper(0.9)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=22), velocity=20)
        self.ur.move_to_location(self.common_zone_sequence['safe'], velocity=self.linear_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'],velocity=self.joint_velocity)

    def filter_vial_to_common_zone_highspeed(self):
        COVER_SEQUENCE_NAMES = ['Waypoint_1', 'Waypoint_2', 'Waypoint_3', 'engage', 'safe']
        self.ur.move_to_location(self.common_zone_sequence['Waypoint_1'], velocity=100)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'], velocity=100)
        self.ur.move_joints(self.common_zone_sequence.joints['safe'], velocity=100)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=30), velocity=20)
        # arm = UR3Arm('192.168.254.88')
        # arm.move_joints([0, 0, 0, 0, 0, 90], velocity=30, acceleration=20, relative=True)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=10), velocity=20)
        self.ur.open_gripper(0.50)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=45), velocity=20)
        self.ur.open_gripper(0.9)
        self.ur.move_to_location(self.common_zone_sequence['engage'] + Location(z=22), velocity=20)
        self.ur.move_to_location(self.common_zone_sequence['safe'], velocity=100)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'], velocity=100)

    def filter_vial_to_shaker(self,index: str= 'A1',filter:bool=True):
        if filter is False:
            #self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'], velocity=self.joint_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=50),velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=27), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] ,velocity=5)
        else:
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=55),velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=27),velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=10), velocity=5)
        self.ur.open_gripper(0.50)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=35),velocity=self.linear_velocity)
    def push_filter_in_shaker(self,index: str= 'A1',filter:bool=True):
        self.ur.open_gripper(0.95)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=55),velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=10),velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=6), velocity=15)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=2), velocity=5)
        time.sleep(3)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=35),velocity=self.linear_velocity)
        self.ur.open_gripper(0.50)

    def filter_to_cup_grid(self,index:str='A1', cup:bool=False):
        self.ur.open_gripper(0.8)
        if cup is False:
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160), velocity=self.linear_velocity)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=5) ,velocity=40)
            self.ur.open_gripper(0.40)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160),velocity=self.linear_velocity)
        else:
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160), velocity=self.linear_velocity)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=30), velocity=40)
            self.ur.open_gripper(0.50)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160), velocity=self.linear_velocity)

    def filter_from_cup_grid(self,index:str='A1'):
        self.ur.open_gripper(0.5)
        self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160),velocity=self.linear_velocity)
        self.ur.move_to_location(self.cup_grid.locations[index] ,velocity=40)
        self.ur.open_gripper(0.90)
        self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=160),velocity=self.linear_velocity)
    def filter_vial_from_shaker(self,index: str,filter:bool=False):
        #if not filter:
            #self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'], velocity=self.joint_velocity)

        if filter:
            self.ur.open_gripper(0.65)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=25), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=-1), velocity=self.linear_velocity)
            self.ur.open_gripper(0.9)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=12), velocity=20)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=50), velocity=self.linear_velocity)
        else:
            self.open_shaker_gripper()
            self.ur.open_gripper(0.65)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=50), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=-1), velocity=self.linear_velocity)
            self.ur.open_gripper(0.85)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=50), velocity=self.linear_velocity)

    def close_shaker_gripper(self):
        self.ur.open_gripper(0.4)
        self.ur.open_gripper(0.9)
        self.ur.move_to_location(self.common_zone_sequence['approach_back'] + Location(z=40), velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_back'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_back']+Location(x=30), velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_back'] + Location(z=40), velocity=self.linear_velocity)
        #self.ur.move_to_location(self.common_zone_sequence['moveup'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.4)

    def open_shaker_gripper(self):
        self.ur.open_gripper(0.4)
        self.ur.open_gripper(0.9)
        self.ur.move_to_location(self.common_zone_sequence['approach_front'] + Location(z=40), velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_front'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_front']+Location(x=-30), velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_front'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['approach_front']+ Location(z=40), velocity=self.linear_velocity)
        self.ur.open_gripper(0.4)

    def open_snap_cap(self):
        self.ur.open_gripper(0.5)
        self.ur.move_joints(self.cap_sequence.joints['Waypoint_16'], velocity=self.joint_velocity)

        self.ur.move_joints(self.cap_sequence.joints['Waypoint_7'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cap_sequence['engagecap'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.78)
        self.ur.move_to_location(self.cap_sequence['Waypoint_1'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.33)
        self.ur.move_to_location(self.cap_sequence['Waypoint_2'], velocity=self.linear_velocity)
        #self.ur.move_to_location(self.cap_sequence['Waypoint_8'], velocity=self.linear_velocity)
        #self.ur.open_gripper(0.78)
        #self.ur.move_to_location(self.cap_sequence['Waypoint_2'], velocity=self.linear_velocity)
        #self.ur.move_joints(self.cap_sequence.joints['Waypoint_3'], velocity=self.joint_velocity)
        #self.ur.move_to_location(self.cap_sequence['Waypoint_4'], velocity=self.linear_velocity)
        #self.ur.move_to_location(self.cap_sequence['Waypoint_5'], velocity=self.linear_velocity)
        #self.ur.open_gripper(0.6)
        #self.ur.move_to_location(self.cap_sequence['Waypoint_6'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_14'], velocity=self.linear_velocity)
        self.ur.move_joints(self.cap_sequence.joints['Waypoint_16'], velocity=self.joint_velocity)


    def close_snap_cap(self):
        self.ur.move_joints(self.cap_sequence.joints['Waypoint_16'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_14'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_9'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_10'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.8)
        self.ur.move_to_location(self.cap_sequence['Waypoint_9'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_7'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_11'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.5)
        self.ur.move_to_location(self.cap_sequence['Waypoint_12'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.9)
        self.ur.move_to_location(self.cap_sequence['Waypoint_13'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cap_sequence['Waypoint_14'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_joints(self.cap_sequence.joints['Waypoint_7'], velocity=self.joint_velocity)
        self.ur.move_joints(self.cap_sequence.joints['Waypoint_16'], velocity=self.joint_velocity)

    def defilter(self, index:str = 'A1', defiltration_velocity: float=3,vision_check: bool = False):

        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=40), velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=25), velocity=self.linear_velocity)
        if vision_check:
            try:
                self.thermoshaker.stop_shaking()
            except:
                pass
            # Before photo position
            self.ur.move_joints(self.shaker_sequence.joints['shakeroffsetloc'], velocity=self.joint_velocity)
            imagesDirectoryBefore = 'C:\\Users\\User\\PycharmProjects\\automated_solubility_h1\\n9\\configuration\\shakerlocationimages\\B5'

            # Take before photo
            image_name = 'Before.png'
            image = self.cam.take_photo(name=image_name, save_photo=True)
            if image is None:
                image = self.cam.take_photo(name=image_name, save_photo=True)
            else:
                image = [image]

            # Analyze before photo
            xx, yy = align_check(imagesDirectoryBefore)
            y=(yy-115.46)/-5.939
            x =(xx-180.83)/10.62

            # close the shaker
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=27), velocity=self.linear_velocity)

            # manual check
            #self.thermoshaker.stop_shaking()
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(x=x, y=y, z=30), velocity=80)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(x=x, y=y, z=-1), velocity=80)
            self. ur.open_gripper(0.95)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=20), velocity=4)

        else:
            self.ur.open_gripper(0.6)
            self.ur.move_to_location(self.shaker_grid.locations[index]  + Location(z=25), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index]+ Location(z=-0.5), velocity=20)
            self.ur.open_gripper(0.85)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=10), velocity=defiltration_velocity)
            time.sleep(0.5)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=15), velocity=10)
            time.sleep(0.5)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=18), velocity=2)
            time.sleep(0.5)
        self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=37), velocity=self.linear_velocity)


    def cone_on(self):
        self.ur.open_gripper(0.4)
        self.ur.move_joints(self.common_zone_sequence.joints['conesh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.common_zone_sequence['coneengage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.7)
        self.ur.move_joints(self.common_zone_sequence.joints['conesh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.common_zone_sequence['czsafe'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['czengage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.3)
        self.ur.move_to_location(self.common_zone_sequence['czsafe'], velocity=self.linear_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['conesh'], velocity=self.joint_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'],velocity=self.joint_velocity)


    def cone_off(self):
        self.ur.move_to_location(self.common_zone_sequence['czsafe'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.common_zone_sequence['czengage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.7)
        self.ur.move_to_location(self.common_zone_sequence['czsafe'], velocity=self.linear_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['conesh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.common_zone_sequence['coneengage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.3)
        self.ur.move_joints(self.common_zone_sequence.joints['conesh'], velocity=self.joint_velocity)
        self.ur.move_joints(self.common_zone_sequence.joints['Waypoint_2'],velocity=self.joint_velocity)

    def test_grid(self):
        reaction_vial_positions = [
            'A1', 'A3', 'A5',
            'B1', 'B3', 'B5',
            'C1', 'C3', 'C5',
            'D1', 'D3', 'D5']
        for i , index in enumerate(reaction_vial_positions):
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=27), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] , velocity=self.linear_velocity)
            self.ur.open_gripper(0.87)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=35), velocity=self.linear_velocity)
            self.ur.move_to_location(self.shaker_grid.locations[index] , velocity=self.linear_velocity)
            self.ur.open_gripper(0.4)
            self.ur.move_to_location(self.shaker_grid.locations[index] + Location(z=35), velocity=self.linear_velocity)

    def test_cup_grid(self):
        reaction_cup_positions = [
            'A1', 'A2', 'A3',
            'B1', 'B2', 'B3',
            'C1', 'C2', 'C3',
            'D1', 'D2', 'D3']
        for i , index in enumerate(reaction_cup_positions):
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=27), velocity=self.linear_velocity)
            self.ur.move_to_location(self.cup_grid.locations[index] , velocity=self.linear_velocity)
            self.ur.open_gripper(0.87)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=35), velocity=self.linear_velocity)
            self.ur.move_to_location(self.cup_grid.locations[index] , velocity=self.linear_velocity)
            self.ur.open_gripper(0.4)
            self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=35), velocity=self.linear_velocity)
        self.ur.move_to_location(self.cup_grid.locations[index] + Location(z=80), velocity=self.linear_velocity)

