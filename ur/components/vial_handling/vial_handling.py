from hein_robots.robotics import Location
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from ur.components.camera_handling.camera_handling import CameraHandling
from heinsight.vision_utilities.camera import Camera
from ika import Thermoshaker
from hein_robots.grids import Grid
import cv2
import os
import time
from pathlib import Path
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage

class VialHandling:

    def __init__(self, ur: UR3Arm, thermoshaker: Thermoshaker, camera: CameraHandling, shaker_sequence_file: str,
                 vial_sequence_file: str,shaker_grid: Grid, cup_grid: Grid, filter_grid_large: Grid,
                 filter_grid_small: Grid, hplc_vial_grid_front: Grid, hplc_vial_grid_back: Grid,
                 joint_velocity: float = 60, linear_velocity: float = 100):
        self.ur = ur
        self.thermoshaker = thermoshaker
        self.camera = camera
        self.shaker_sequence = URScriptSequence(shaker_sequence_file)
        self.vial_sequence = URScriptSequence(vial_sequence_file)
        self.shaker_grid = shaker_grid
        self.cup_grid = cup_grid
        self.hplc_vial_grid_front = hplc_vial_grid_front
        self.hplc_vial_grid_back = hplc_vial_grid_back
        self.filter_grid_large = filter_grid_large
        self.filter_grid_small = filter_grid_small
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity
        self.front_hplc_list = ['A','B','C','D']
        self.back_hplc_list = ['E','F','G','H','I','J','K']
        self.vial_grid = self.hplc_vial_grid_front
        self.above_hplc = 'above_hplc_frnt'

    def home(self):
        print(self.shaker_sequence.joints['home'])
        self.ur.move_joints(self.shaker_sequence.joints['home'], velocity=self.joint_velocity)

    def open_gripper(self):
        self.ur.open_gripper(0)

    def close_shaker_gripper(self):
        self.ur.open_gripper(0.5)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_back'] + Location(z=40),
                                     velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_back'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_back'] + Location(x=29),
                                     velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_back'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_back'] + Location(z=40),
                                     velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)

    def open_shaker_gripper(self):
        self.ur.open_gripper(0.5)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_front'] + Location(z=40),
                                     velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_front'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_front'] + Location(x=-29),
                                     velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_front'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['approach_front'] + Location(z=40),
                                     velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)

    def cup_to_shaker(self, shaker_index: str = 'A1', vial_type: str = 'hplc', grab_cap: bool = False):
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40), velocity=self.linear_velocity)
        self.thermoshaker.start_shaking()
        if vial_type == 'hplc':
            self.ur.open_gripper(0.78) # prev was 0.77
            if grab_cap:
                self.ur.move_to_location(self.shaker_grid.locations[shaker_index]+ Location(z=15), velocity=10)
            else:
                self.ur.move_to_location(self.shaker_grid.locations[shaker_index]+ Location(z=7), velocity=10)
        else:
            self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=3), velocity=10)
        self.thermoshaker.stop_shaking()
        self.ur.open_gripper(0.55)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40), velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_sequence.locations['safe_above'], velocity=self.linear_velocity)

    def cup_from_shaker(self, shaker_index: str = 'A1'):
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.63)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                 velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index], velocity=self.linear_velocity)
        self.ur.open_gripper(0.80)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                 velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)

    def vial_from_shaker(self, shaker_index: str = 'A1'):
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.65)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                 velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=-1), velocity=self.linear_velocity)
        self.ur.open_gripper(0.825) # prev was 0.82
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                 velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)

    def push_shaker(self):
        self.ur.move_joints(self.shaker_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['shaker_push'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['shaker_push'] + Location(x=-40.2), velocity=20)
        self.ur.move_to_location(self.vial_sequence.locations['shaker_push'], velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['home'], velocity=self.joint_velocity)

    def push_filter_in_shaker(self, shaker_index: str = 'A1', plastic_filter: bool = False):
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.85)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                     velocity=self.linear_velocity)
        if plastic_filter:
            self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=4.5), velocity=15)
        else:
            self.ur.move_to_location(self.shaker_grid.locations[shaker_index] +Location(z=3), velocity=15)
        time.sleep(2)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=40),
                                 velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)

    def pull_filter_in_shaker(self, shaker_index: str = 'A1'):
        self.close_shaker_gripper()
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index]  + Location(z=25), velocity=self.linear_velocity)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index]+ Location(z=-0.5), velocity=20)
        self.ur.open_gripper(0.85)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index] + Location(z=10), velocity=10)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.shaker_grid.locations[shaker_index]  + Location(z=25), velocity=self.linear_velocity)
        self.ur.move_joints(self.shaker_sequence.joints['safe_above'], velocity=self.joint_velocity)
        self.open_shaker_gripper()

    def cup_from_tray(self, cup_index: str = 'A1'):
        self.ur.move_joints(self.vial_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.6) # was 0.6
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=20), velocity=self.linear_velocity)
        self.ur.move_to_location(self.cup_grid.locations[cup_index], velocity=self.linear_velocity)
        self.ur.open_gripper(0.75) # prev. grippers were 0.76
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=60), velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['above_cups'], velocity=self.joint_velocity)


    def cup_to_tray(self, cup_index: str = 'A1', from_shaker: bool = True):
        self.ur.move_joints(self.vial_sequence.joints['above_cups'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=60), velocity=self.linear_velocity)
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=20), velocity=self.linear_velocity)
        self.ur.open_gripper(0.75)
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=9), velocity=10)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.cup_grid.locations[cup_index] + Location(z=20), velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['above_cups'], velocity=self.joint_velocity)

    def grid_location_from_vial_index(self, vial_index):

        #Two tray exist, front and back. The first row of the back tray starts at 'E' instead of 'A' (see ur_deck.py)
        #This function will check the vial index letter to determine which tray it should interact with

        if vial_index[0] in self.front_hplc_list:
            self.vial_grid = self.hplc_vial_grid_front
            self.above_hplc = 'above_hplc_frnt'
        elif vial_index[0] in self.back_hplc_list:
            self.vial_grid = self.hplc_vial_grid_back
            self.above_hplc = 'above_hplc_back'

    def vial_from_tray(self, vial_index: str = 'A1', grab_cap: bool = False):

        #ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=vial_index)

        offset = Location()
        if grab_cap:
            offset = Location(z=7)
        self.ur.move_to_location(self.vial_sequence.locations[self.above_hplc], velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=15), velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + offset - Location(z=1), velocity=self.linear_velocity)
        if grab_cap:
            self.ur.open_gripper(0.82)
        else:
            self.ur.open_gripper(0.825) # prev. was 0.81
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=30), velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints[self.above_hplc], velocity=self.joint_velocity)
        self.home()


    def vial_from_tray_loc(self, vial_index: str = 'A1', grab_cap: bool = False):

        self.ur.move_to_location(self.vial_sequence.locations["home_2"], velocity=self.linear_velocity)

        #ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=vial_index)

        offset = Location()
        if grab_cap:
            offset = Location(z=4)
        self.ur.move_to_location(self.vial_sequence.locations[self.above_hplc], velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=15), velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + offset - Location(z=1), velocity=self.linear_velocity)
        self.ur.open_gripper(0.82)
        # self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=30), velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=10), velocity=self.linear_velocity)


    def above_hplc_tray_front(self):
        self.ur.move_to_location(self.vial_sequence.locations['above_hplc_frnt'], velocity=self.linear_velocity)

    def vial_to_tray(self, vial_index: str = 'A1', grab_cap: bool = False):

        #ENSURE THIS FUNCTION STARTS ANY FUNCTION THAT INTERACTS WITH THE HPLC VIAL TRAYS
        self.grid_location_from_vial_index(vial_index=vial_index)

        self.ur.move_joints(self.vial_sequence.joints[self.above_hplc], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index]+ Location(z=30), velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=5), velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.vial_grid.locations[vial_index] + Location(z=15), velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_sequence.locations[self.above_hplc], velocity=self.linear_velocity)
        self.home()

    def filter_from_tray(self, filter_index: str = 'A1', large_filter: bool = True):
        self.ur.move_joints(self.vial_sequence.joints['above_filters'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.63)
        if large_filter:
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index] + Location(z=40), velocity=self.linear_velocity)
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index], velocity=self.linear_velocity)
            self.ur.open_gripper(0.81) # prev was 0.81
            # 2023-09-14 0.82, return to this value if claws are replaced
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index] + Location(z=40), velocity=self.linear_velocity)
        else:
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index], velocity=self.linear_velocity)
            self.ur.open_gripper(1)
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['above_filters'], velocity=self.joint_velocity)

    def filter_to_tray(self, filter_index: str = 'A1', large_filter: bool = True):
        self.ur.move_joints(self.vial_sequence.joints['above_filters'], velocity=self.joint_velocity)
        if large_filter:
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index], velocity=self.linear_velocity)
            self.ur.open_gripper(0.6)
            self.ur.move_to_location(self.filter_grid_large.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
        else:
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index], velocity=self.linear_velocity)
            self.ur.open_gripper(0.6)
            self.ur.move_to_location(self.filter_grid_small.locations[filter_index] + Location(z=40),
                                  velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['above_filters'], velocity=self.joint_velocity)

    def cup_to_aligner(self):
        self.ur.move_joints(self.vial_sequence.joints['above_holder'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['holder_approach'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['holder_engage'], velocity=30)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.vial_sequence.locations['above_holder'], velocity=self.linear_velocity)

    def cup_from_aligner(self):
        self.ur.move_joints(self.vial_sequence.joints['above_holder'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.6) #prev was 0.6
        self.ur.move_to_location(self.vial_sequence.locations['holder_engage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.75) # prev. grippers were 0.75
        self.ur.move_to_location(self.vial_sequence.locations['above_holder'], velocity=50)
        self.ur.open_gripper(0.74) # prev was 0.74



    def align_filter(self):
        self.ur.move_joints(self.vial_sequence.joints['align_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['align_engage'], velocity=60)
        self.ur.move_to_location(self.vial_sequence.locations['align_above'], velocity=360)

    def insert_filter(self, fill_line: bool = True):
        self.ur.move_joints(self.vial_sequence.joints['above_holder'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['holder_approach'] - Location(x=0.5, y=0.5),
                                 velocity=self.linear_velocity)
        self.ur.move_to_location(
            self.vial_sequence.locations['holder_approach'] - Location(z=7.1) - Location(x=0.5, y=0.5), velocity=5)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.vial_sequence.locations['holder_approach'], velocity=self.linear_velocity)

    def push_filter(self, fill_line: bool = True, pull_filter: bool = False):
        self.ur.move_to_location(self.vial_sequence.locations['holder_approach'] + Location(z=10), velocity=self.linear_velocity)
        self.ur.open_gripper(0.9)
        if pull_filter:
            self.ur.move_to_location(self.vial_sequence.locations['holder_approach'] - Location(z=19), velocity=5)
        elif fill_line:
            self.ur.move_to_location(self.vial_sequence.locations['holder_approach'] - Location(z=16), velocity=5)
        else:
            self.ur.move_to_location(self.vial_sequence.locations['holder_approach'] - Location(z=8), velocity=5)
        self.ur.move_to_location(self.vial_sequence.locations['holder_approach'], velocity=self.linear_velocity)
        #2023-11-22, changed velocity from 20 to 5

    def vial_to_handoff(self, index:str='back'):
        if index == 'back':
            location = self.vial_sequence.locations['hplc_back']
        else:
            location = self.vial_sequence.locations['hplc_front']
        self.ur.move_joints(self.vial_sequence.joints['mid'], velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['back_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(location+Location(z=60), velocity=self.linear_velocity)
        self.ur.move_to_location(location+Location(z=2), velocity=self.linear_velocity)
        self.ur.open_gripper(0.5)
        self.ur.move_to_location(location+Location(z=60), velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['mid'], velocity=self.joint_velocity)

    def vial_from_handoff(self, index:str='front'):
        if index == 'back':
            location = self.vial_sequence.locations['hplc_back']
        else:
            location = self.vial_sequence.locations['hplc_front']
        self.ur.move_joints(self.vial_sequence.joints['mid'], velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['back_sh'], velocity=self.joint_velocity)
        self.ur.open_gripper(0.5)
        self.ur.move_to_location(location+Location(z=60), velocity=self.linear_velocity)
        self.ur.move_to_location(location, velocity=self.linear_velocity)
        self.ur.open_gripper(0.85)
        self.ur.move_to_location(location+Location(z=60), velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['mid'], velocity=self.joint_velocity)
        self.home()

    def vial_to_camera(self, angle: bool=False):
        self.ur.move_to_location(self.vial_sequence.locations['camera_straight'] + Location(z=100), velocity=self.linear_velocity)
        if not angle:
            self.ur.move_to_location(self.vial_sequence.locations['camera_straight'],velocity=self.linear_velocity)
        elif angle:
            self.ur.move_to_location(self.vial_sequence.locations['camera_angle'],velocity=self.linear_velocity)

    def vial_from_camera(self):
        self.ur.move_to_location(self.vial_sequence.locations['camera_straight'] + Location(z=100),
                                 velocity=self.linear_velocity)

    def take_picture_of_vial(self, vial_index, shaker_index, temperature):
        self.thermoshaker.stop_shaking()
        self.vial_from_shaker(shaker_index=shaker_index)
        self.vial_to_camera(angle=True)
        file_name = str(vial_index) + '_' + str(temperature) + '_' + 'angle'
        self.camera.take_picture(file_name=file_name)
        self.vial_to_camera(angle=False)
        file_name = str(vial_index) + '_' + str(temperature) + '_' + 'bottom'
        self.camera.take_picture(file_name=file_name)
        self.vial_from_camera()
        self.cup_to_shaker(shaker_index=shaker_index)

if __name__ == "__main__":
    UR_arm = UR3Arm('192.168.254.88')
    arm = UR_arm
    spin_sequence = '../../UR/configuration/sequences/shaker.script'

    arm.move_joints()