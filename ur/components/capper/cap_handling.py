# import serial as serial_
import time

# import niraapad.backends
from hein_robots.robotics import Location


from ur.components.capper.capper import Capper
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence

class CapHandling:
    def __init__(self, ur: UR3Arm, capper:Capper, capper_sequence_file: str,
                 joint_velocity: float = 60, linear_velocity: float = 80):
        self.ur = ur
        self.capper = capper
        self.capper_sequence = URScriptSequence(capper_sequence_file)
        # self.capper.home()
        self.cap_on = True
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity

    def close(self):
        self.capper.close()

    def open(self):
        self.capper.open()

    def home(self):
        self.ur.move_joints(self.capper_sequence.joints['mid'], velocity=self.joint_velocity)

    def uncap(self):
        self.capper.open()
        self.ur.move_joints(self.capper_sequence.joints['safe_height'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.8, velocity=0.5)
        self.capper.close()
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage']+Location(z=1), velocity=self.linear_velocity)
        self.capper.loosen()
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.ur.move_joints(self.capper_sequence.joints['station_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['station_engage'], velocity=self.linear_velocity)
        #location was  + Location(z=0.5)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.capper_sequence.locations['station_sh'], velocity=self.linear_velocity)
        self.cap_on = False

    def cap(self):
        self.ur.open_gripper(0.95)
        self.ur.move_to_location(self.capper_sequence.locations['station_engage'] + Location(z=2), velocity=self.linear_velocity)
        time.sleep(3)
        ##Location below has been changed from 1.5
        self.ur.move_to_location(self.capper_sequence.locations['station_sh'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        ## location below was + z= 0.5
        self.ur.move_to_location(self.capper_sequence.locations['station_engage'] + Location(z=0.5), velocity=self.linear_velocity)
        #location above was  + Location(z=0.75)
        self.ur.open_gripper(0.77, velocity=0.5)
        self.ur.move_to_location(self.capper_sequence.locations['station_sh'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage'] + Location(z=1.5), velocity=15) ####################
        # time.sleep(1)
        # self.ur.open_gripper(0.6, velocity=0.5)
        # self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        # self.ur.open_gripper(1)
        # self.ur.move_to_location(self.capper_sequence.locations['cap_engage']+Location(z=1), velocity=self.linear_velocity)
        # # time.sleep(1)
        # self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        # self.ur.open_gripper(0.6)

        # self.ur.move_to_location(self.capper_sequence.locations['cap_engage'], velocity=self.linear_velocity)
        # self.ur.open_gripper(0.79, velocity=0.5)
        self.capper.tighten()
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage'] - Location(z=4), velocity=self.linear_velocity)
        self.ur.open_gripper(0.8, velocity=0.5)
        self.capper.open()
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.cap_on = True
        # self.ur.open_gripper(0.6)

    def vial_to_holder(self):
        offset, gripper = self.cap_offset(self.cap_on)
        self.ur.move_joints(self.capper_sequence.joints['mid'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['holder_v_sh'], velocity=self.linear_velocity)
        self.ur.open_gripper(gripper)
        self.ur.move_to_location(self.capper_sequence.locations['holder_cap']+offset, velocity=self.linear_velocity)
        self.ur.open_gripper(0.4)
        self.ur.move_to_location(self.capper_sequence.locations['holder_v_sh'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['mid'], velocity=self.linear_velocity)

    def vial_from_holder(self):
        offset, gripper = self.cap_offset(self.cap_on)
        self.ur.move_to_location(self.capper_sequence.locations['mid'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['holder_v_sh'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.59)
        self.ur.move_to_location(self.capper_sequence.locations['holder_cap']+offset, velocity=self.linear_velocity)
        self.ur.open_gripper(gripper, velocity=0.5)
        self.ur.move_to_location(self.capper_sequence.locations['holder_v_sh'], velocity=self.linear_velocity)
        self.ur.open_gripper(gripper + 0.01, velocity=0.5)
        self.ur.move_to_location(self.capper_sequence.locations['mid'], velocity=self.linear_velocity)

    def vial_from_capper(self):
        offset, gripper = self.cap_offset(self.cap_on)
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage']+offset-Location(z=1.5), velocity=self.linear_velocity)
        self.ur.open_gripper(gripper + 0.1, velocity=0.5)
        self.capper.open()
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['mid'], velocity=self.linear_velocity)

    def vial_to_capper(self):
        offset, gripper = self.cap_offset(self.cap_on)
        self.capper.open()
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.capper_sequence.locations['cap_engage'] + offset + Location(z=2), velocity=self.linear_velocity)
        self.capper.close()
        self.ur.open_gripper(0.59, velocity=0.5)
        self.ur.move_to_location(self.capper_sequence.locations['safe_height'], velocity=self.linear_velocity)

    def set_cap_status(self, cap_status):
        self.cap_on = cap_status

    def cap_offset(self, cap_on):
        if cap_on:
            offset = Location(z=0)
            gripper = 0.79
        else:
            offset = Location(z=-5)
            gripper = 0.815
            #working at 0.825 Nov 17/23
        return offset, gripper

    '''
    November 9/23 changelog
    Installed new grippers, increased width
    Prior error rate ~1/4
    Adjusted from_holder gripper strength to 0.835, added additonal regrip step (+0.015) in this function
    Adjusted cartesian spacing for hplc_vial_grid_front to reduce tilt of vial in 
    
    to test
    does using just fresh caps work better?
    error rates of waters vs agilent vs unmarked

    '''




# if __name__ == "__main__":
#     x = Capper("COM38")
#     ur = UR3Arm('192.168.254.88')
#     capper = CapHandling(ur,x,'../../configuration/sequences/capper.script')
#     # for i in range(5):
#     capper.vial_from_holder()
#     capper.uncap()
#     capper.vial_to_holder()
#     capper.vial_from_holder()
#
#
#     capper.cap()

# if __name__ == "__main__":
#     x = Capper("COM38")
#     ur = UR3Arm('192.168.254.88')
    # capper = CapHandling(ur,x,'../../configuration/sequences/capper.script')
#
#     capper.vial_to_holder()
#     capper.vial_from_holder()
#     capper.open()
#     capper.vial_to_holder()
#     capper.vial_from_holder()
#
#
#     capper.cap()