import json
import pandas as pd
from datetime import datetime
import os
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np


class DataHandling:
    def __init__(self):
        self.filepath = str
        self.hplc_directory = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data'


    def set_workflow(self, workflow: str = 'levi'):
        print(os.getcwd())
        if workflow == 'levi':
            self.filepath = './data/levi_results.json'
        elif workflow == 'cooling_crystal':
            self.filepath = './data/cooling_crystal.json'
        elif workflow == '3_temp':
            self.filepath = './data/3_temp.json'
        elif workflow == 'multi_temp':
            self.filepath = './data/multi_temp.json'
        elif workflow == 'wash':
            self.filepath = './data/multi_wash_results.json'
        elif workflow == 'standard_curve':
            self.filepath = './data/standard_curve.json'
        elif workflow == 'settling_time':
            self.filepath = './data/settling_time.json'
        elif workflow == 'cooling_cryst':
            self.filepath = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/cooling_crystal/data/log.json'
        else:
            print('invalid workflow, check spelling')

    def new_sample(self):
        with open(self.filepath) as fp:
            listObj = json.load(fp)
            listObj.append({"date": str(datetime.now()),
            })
        with open(self.filepath, 'w') as json_file:
            json.dump(listObj, json_file)

    def save_experiments_as_csv(self):
        with open(self.filepath) as fp:
            listObj = json.load(fp)
            data = pd.json_normalize(listObj)
            data.to_csv('levi_results.csv')

    def save_value(self, data_name, data_value):
        #updates last experiment info
        with open(self.filepath) as fp:
            listObj = json.load(fp)
        listObj[-1][data_name]=data_value
        with open(self.filepath, 'w') as json_file:
            json.dump(listObj, json_file)

    def overwrite_value(self,i, data_name, data_value):
        #updates "i" experiment info
        with open(self.filepath) as fp:
            listObj = json.load(fp)
        listObj[i][data_name]=data_value
        with open(self.filepath, 'w') as json_file:
            json.dump(listObj, json_file)

    def read_value(self, i , data_name):
        #reads ith element from the end
        with open(self.filepath) as fp:
            listObj = json.load(fp)
        return listObj[i][data_name]

    def count_experiments(self):
        with open(self.filepath) as fp:
            listObj = json.load(fp)
            return int(len(listObj))

    def find_most_recent_hplc_data(self):
        # Get a list of subdirectories in the specified directory
        subdirectories = next(os.walk(self.hplc_directory))[1]

        if not subdirectories:
            print("No subdirectories found.")
        else:
            # Convert folder names to datetime objects and store them in a list
            date_objects = [datetime.strptime(subdir, '%Y-%m-%d') for subdir in subdirectories]

            # Find the most recent date
            most_recent_date = max(date_objects)

            # Convert the most recent date back to the 'YYYY-MM-DD' format as a string
            most_recent_date_str = most_recent_date.strftime('%Y-%m-%d')

            # Find the corresponding folder name
            current_date_folder = os.path.join(self.hplc_directory, most_recent_date_str)

        # Get the current date and time
        files = [os.path.join(current_date_folder, file) for file in os.listdir(current_date_folder) if
                 os.path.isfile(os.path.join(current_date_folder, file))]

        # Sort the files based on creation time (most recent first)
        files.sort(key=lambda x: os.path.getctime(x), reverse=True)

        # Return the name of the most recently created file
        if files:
            return os.path.abspath(files[0])
        else:
            return None

    def make_standard_curve(self, hplc_file, number_of_samples, initial_mass, initial_volume, dilution_factor,
                            compound_name, output_folder, output_json, min_r2):
        # Read the CSV file into a pandas DataFrame, excluding the first column
        df = pd.read_csv(hplc_file, index_col="sample")

        # Get the column names excluding the first column
        columns = df.columns[1:]

        # Dictionary to store the results
        results_dict = {
            'date': pd.Timestamp.now().isoformat(),
            'compound_name': compound_name,
            'dilution_factor': dilution_factor,
            'peaks': {}
        }

        concentrations = [initial_mass / initial_volume]
        for _ in range(1, number_of_samples):
            concentrations.append(concentrations[-1] / dilution_factor)
        concentrations_copy = concentrations

        for column in columns:
            first_run_removed = False
            concentrations_copy = concentrations

            # Select the column of interest
            compound_data = df[column].values
            compound_data_copy = compound_data

            # Perform linear regression using scipy.stats
            slope, intercept, r_value, _, _ = stats.linregress(concentrations_copy, compound_data_copy)

            # Calculate R-squared value
            r2 = r_value ** 2

            if r2 < min_r2:
                concentrations_copy = concentrations_copy[1:]
                compound_data_copy = compound_data_copy[1:]
                first_run_removed = True

                # Perform linear regression using scipy.stats
                slope, intercept, r_value, _, _ = stats.linregress(concentrations_copy, compound_data_copy)

                # Calculate R-squared value
                r2 = r_value ** 2

            # Create a dictionary for the result of the current peak label
            peak_dict = {
                'slope': slope,
                'intercept': intercept,
                'r2': r2,
                'first_run_removed': first_run_removed
            }

            # Add the peak dictionary to the results dictionary
            results_dict['peaks'][column] = peak_dict

        # Read the previous runs from the existing JSON file
        with open(output_json, 'r') as json_file:
            previous_runs = json.load(json_file)

        # Append the current run dictionary to the list of previous runs
        previous_runs.append(results_dict)

        # Save the combined results to the output JSON file
        with open(output_json, 'w') as json_file:
            json.dump(previous_runs, json_file)

        # Save the raw data to a CSV file
        current_date = datetime.now().strftime('%m-%d-%Y')
        output_folder = os.path.join(output_folder, current_date)
        os.makedirs(output_folder, exist_ok=True)
        file_counter = 1
        while True:
            output_file = os.path.join(output_folder, f'{compound_name}_dilution_{dilution_factor}_{file_counter}.csv')
            if not os.path.exists(output_file):
                break
            file_counter += 1

        # Save the raw data to a CSV file
        raw_data = pd.DataFrame({'Concentration': concentrations[:len(compound_data)], **df.iloc[:, 1:]})
        raw_data.to_csv(output_file, index=False)

    def check_r2_threshold(self, file_path, dilution_factor, threshold):
        # Read JSON data from file
        try:
            with open(file_path, 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            print("File not found")
            return False
        except json.JSONDecodeError as e:
            print("Error decoding JSON:", str(e))
            return False

        # Filter dictionaries based on dilution factor
        filtered_dicts = [d for d in data if d.get("dilution_factor") == dilution_factor]

        # Check if any dictionaries match the dilution factor
        if not filtered_dicts:
            print(f"No dictionaries found with dilution_factor: {dilution_factor}")
            return False

        # Get the most recent dictionary from the filtered list
        most_recent_dict = filtered_dicts[-1]

        # Check if the "peaks" key is present
        if "peaks" not in most_recent_dict:
            print("No 'peaks' key in the most recent dictionary")
            return False

        # Iterate over the "peaks" dictionary and check the "r2" values
        for peak, values in most_recent_dict["peaks"].items():
            if "r2" not in values:
                print(f"No 'r2' value for peak '{peak}'")
                return False

            r2 = values["r2"]
            if r2 < threshold:
                print(f"r2 value for peak '{peak}' is below the threshold")
                return False

        # All "r2" values are above the threshold
        return True

    def get_hplc_runtime(self, solid_name: str, csv_file: str):
        df = pd.read_csv(csv_file)
        runtime = df.loc[df['solid_name'] == solid_name, 'hplc_runtime_min'].values[0]
        return runtime

    def get_hplc_settings(self, solid_name: str, csv_file: str):
        df = pd.read_csv(csv_file)
        method = df.loc[df['solid_name'] == solid_name, 'hplc_method'].values[0]
        injection_volume_ul = df.loc[df['solid_name'] == solid_name, 'injection_volume_ul'].values[0]
        return method, injection_volume_ul

if __name__ == '__main__':
    dh = DataHandling()
    std_curve_output_json = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/standard_curve/data/standard_curve.json'

    with open(std_curve_output_json, "r") as file:
        json_data = json.load(file)
        print(json_data)

    hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/results.csv'
    output_json = 'results.json'
    output_folder = 'output'

    # dh.make_standard_curve(hplc_file,number_of_samples=4,initial_mass=6,initial_volume=1, min_r2=0.9, dilution_factor=3,
    #                                 compound_name='ND-1-81', output_json=output_json, output_folder=output_folder)
    #
    # file_path = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/data/standard_curve.json'
    # if dh.check_r2_threshold(file_path, dilution_factor=4, threshold=0.95):
    #     print('continue')