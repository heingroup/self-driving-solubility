# import serial as serial_
from ftdi_serial import Serial

import time


class SamplomaticStepper:
    def __init__(self, port, baud=115200, bytesize=8, timeout=20):
        # self.somatic = serial_.Serial(port=port, baudrate=baud, bytesize=bytesize,
        #                             timeout=timeout, stopbits=serial_.STOPBITS_ONE)
        self.somatic = Serial(device_port=port, baudrate=baud, data_bits=bytesize, read_timeout=timeout)
        self.motor_resolution = 200
        self.micro_steps = 4
        self.distance_per_revolution_mm = 2
        self.max_speed = 1000 # in steps/s
        self.position = 0

        print(f"Established connection with {self.somatic} at {port}")

    def connect(self):
        self.somatic.__init__()


    def disconnect(self):
        # put capper in its default state of "closed" before closing connection
        self.home()
        self.somatic.close()
        print("Disconnected Samplomatic")

    def set_max_speed(self, speed):
        # speed is given in steps/s
        cmd = "SCR"
        self.max_speed = speed
        self._execute(cmd)

    def move_needle(self, steps):
        cmd = f"SPS {steps}"
        self._execute(cmd)
        self.position += steps

    def move_needle_up(self, steps):
        cmd = f"SPS {-steps}"
        self._execute(cmd)
        self.position -= steps

    def set_acceleration(self, acl):
        cmd = f"SIR {acl}"
        self._execute(cmd)

    def set_max_homing_speed(self, speed):
        cmd = f"SHR {speed}" 
        self._execute(cmd)

    def set_homing_direction(self, dir):
        cmd = f"SHD {dir}"
        self._execute(cmd)

    def stop_motor(self):
        cmd = "STP"
        self._execute(cmd)
        
    def home(self):
        #print('home command')
        cmd = "HME"
        self._execute(cmd)
        # print(self.position)
        self.position = 0
        #print('done home command')

        
    def _turn_off(self):
        print("DEPRECATED")
        print("the method _turn_off() is deprecated and is no longer necessary. Please do not use it. Not sure why I put this in anyway")
        cmd = "RLY"
        self._execute(cmd)       

    def get_steps_to_go(self):
        cmd = "QCP"
        steps = self._execute(cmd)
        return steps

    def get_target_pos(self):
        cmd = "QTP"
        target_pos = self._execute(cmd)
        return target_pos

    def get_max_speed(self):
        cmd = "QCR"
        max_speed = self._execute(cmd)
        return max_speed

    def get_acceleration(self):
        cmd = "QIR"
        acl = self._execute(cmd)
        return acl

    def get_is_running(self):
        cmd = "BSY"
        is_running = self._execute(cmd)
        return is_running

    def _execute(self, cmd, wait=True):
        ans = None
        self.somatic.write(cmd.encode())

        # Serial device returns "-4" when action complete
        while True:
            # ans = self.somatic.readline().decode("ascii").strip()
            ans = self.somatic.read_line().decode("ascii").strip()
            if ans == "-4":
                break

        # for _ in range(5):

        #     try:
        #         ans = self.somatic.readline().decode("ascii").strip()
        #         print(ans)
        #     except UnicodeDecodeError:
        #         pass

        #     if ans == "r":

        #         break

        # print(f"Send: {cmd!a} received: {ans!a}")
        return ans


    def move_in_mm(self, distance):
        # distance in mm
        revolutions_to_rotate = distance / self.distance_per_revolution_mm
        
        steps_to_rotate = self.motor_resolution * self.micro_steps * revolutions_to_rotate
        run_time = steps_to_rotate / self.max_speed

        # print(f"{revolutions_to_rotate=}\n{steps_to_rotate=}\n{run_time=}")
        self.move_needle(steps=steps_to_rotate)
        
        # time.sleep(run_time*1.5)

    def move_in_mm_up(self, distance):
        # distance in mm
        revolutions_to_rotate = abs(distance) / self.distance_per_revolution_mm

        steps_to_rotate = self.motor_resolution * self.micro_steps * revolutions_to_rotate
        run_time = steps_to_rotate / self.max_speed

        # print(f"{revolutions_to_rotate=}\n{steps_to_rotate=}\n{run_time=}")
        self.move_needle_up(steps=steps_to_rotate)

        # time.sleep(run_time * 1.5)



# def main():
#     x = SamplomaticStepper("COM9")
#     print('connected')
#     # print(x.get_max_speed())
#     x.move_in_mm(5)
#     print("BOTTOM")
#     # input("Move?")
#     # x.move(5000)
#     # time.sleep(3)
#     x.home()
#     print("HOMED")
#     # pass


if __name__ == "__main__":
    x = SamplomaticStepper("COM9")
    x.move_in_mm(-20)
    time.sleep(3)
    x.home()
