from hein_robots.robotics import Location, Cartesian
from typing import Optional
import math
import time
from ur.components.relay_board.relay_board import RelayBoard
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.robotics import Location
from hein_robots.grids import Grid, StaticGrid
from ur.components.vision_angle_measurement.measurement import VisionDotAngleMeasurement, NoDotFoundException
from ur.components.vial_handling.vial_handling import VialHandling

tube_grid = Grid(Location(x=122.0, y=-306.5, z=47.99, rx=0, ry=0, rz=0), rows=3, columns=6,
                 spacing=Cartesian(-19.7, 25.2, 0), offset=Location(rx=178.9, ry=0.9612, rz=-175.9))

class Centrifuge:
    def __init__(self, ur: UR3Arm, angle_measurement: VisionDotAngleMeasurement, vial_handling: VialHandling,
                 centrifuge_sequence_file: str, vial_sequence_file: str, relay: RelayBoard,
                 measurement_offset: Location = Location(y=100, z=50), safe_offset: float = 35,
                 plunge_depth: float = -4, centrifuge_start_angle: float = 230,
                 centrifuge_fudge_angle: float = 23,
                 centrifuge_radius: float = 16, show_images: bool = False, joint_velocity: float = 60,
                 linear_velocity: float = 80, rotate_velocity: float = 10,
                 tube_grid: Grid = tube_grid):
        self.ur = ur
        self.relay = relay
        self.angle_measurement = angle_measurement
        self.vial_handling = vial_handling
        self.centrifuge_sequence = URScriptSequence(centrifuge_sequence_file)
        self.vial_sequence = URScriptSequence(vial_sequence_file)
        self.centrifuge_location = self.centrifuge_sequence['center'].translate(x=-2)
        self.centrifuge_location_joints = self.centrifuge_sequence.joints['center']
        # self.centrifuge_camera_joints = self.centrifuge_sequence.joints['camera_position']

        self.measurement_offset = measurement_offset
        self.safe_offset = safe_offset
        self.plunge_depth = plunge_depth
        self.centrifuge_start_angle = centrifuge_start_angle
        self.centrifuge_fudge_angle = centrifuge_fudge_angle
        self.show_images = show_images
        self.centrifuge_radius = centrifuge_radius

        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity
        self.rotate_velocity = rotate_velocity
        self.tube_grid = tube_grid

    def home(self):
        self.ur.move_joints(self.centrifuge_sequence.joints['home'], velocity=self.joint_velocity)

    def tube_from_tray(self, tube_index: str = 'A1', filter: bool = False):
        """
        UR getting capped eppendorf tube with filter from ur tube tray
        :param tube_index: index of tube in tray
        """
        offset = Location()
        if filter:
            offset = Location(z=5)
        engage_location = self.tube_grid.locations[tube_index] + offset + Location(z=-1.5)
        safe_location = engage_location + Location(z=60)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(0.78)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)

    def tube_to_tray(self, tube_index: str = 'A1', filter: bool = False):
        """
        UR placing capped eppendorf tube with filter to ur tube tray
        :param tube_index: index of tube in tray
        """
        offset = Location()
        if filter:
            offset = Location(z=5)
        engage_location = self.tube_grid.locations[tube_index] + offset
        safe_location = engage_location + Location(z=60)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)

    #Active_area defined as 'A3' and 'C3'. All sampling, filter switches, dosing and sampling occur here
    def tube_to_active_area(self, tube_index: str = 'A1', filter: bool = False, front: bool = True):
        self.tube_from_tray(tube_index = tube_index, filter=filter)
        if front:
            self.tube_to_tray(tube_index = 'A3', filter=filter)
        else:
            self.tube_to_tray(tube_index='C3', filter=filter)

    def tube_from_active_area(self, tube_index: str = 'A1', filter: bool = False, front: bool = True):
        if front:
            self.tube_from_tray(tube_index='A3', filter=filter)
        else:
            self.tube_from_tray(tube_index='C3', filter=filter)
        self.tube_to_tray(tube_index = tube_index, filter = filter)

    def pickup_rod(self):
        """UR picks up centrifuge rod from tube tray F2"""
        safe_location = self.tube_grid.locations['F3'] + Location(z=80)
        engage_location = self.tube_grid.locations['F3'] + Location(z=16)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['home'], velocity=self.linear_velocity)

    def place_rod(self):
        """UR place centrifuge rod to tube tray F3"""
        safe_location = self.tube_grid.locations['F3'] + Location(z=80)
        engage_location = self.tube_grid.locations['F3'] + Location(z=16)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(self.vial_sequence.locations['home'], velocity=self.linear_velocity)

    def spin_centrifuge(self, centrifuge_duration: float = 4):
        self.relay.set_output(4, True)
        time.sleep(centrifuge_duration)
        self.relay.set_output(4, False)

    def is_spinning(self):
        is_spinning = self.relay.get_output(4)
        return is_spinning

    def run_centrifuge(self, centrifuge_duration:  float = 15, tube_index: str = 'front', cover_off: bool = False):
        """
        take centrifuge tube from station and run
        :param centrifuge_duration:
        :param tube_index:
        :return:
        """
        if tube_index == 'front':
            tray_index = 'A3'
        else:
            tray_index = 'C3'
        # rotate centrifuge to correct location
        self.rotate_centrifuge()
        # move vial to centrifuge
        self.tube_from_tray(tray_index)
        self.place_vial_in_centrifuge()
        #
        # rotate_centrifuge_inverse()

        # place cover on centrifuge
        if not cover_off:
            self.place_cover()
        # run centrifuge
        self.spin_centrifuge(centrifuge_duration)
        time.sleep(3)
        # remove cover
        if not cover_off:
            self.remove_cover()
        # rotate centrifuge to correct location
        self.rotate_centrifuge()
        # move vial to transfer location
        self.pickup_vial_from_centrifuge()
        self.tube_to_tray(tray_index)

    def pickup_vial_from_centrifuge(self):
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        # f.self.ur.move_joints(ur_deck.VIAL_SEQUENCE.joints['r_pickup1'], velocity=self.joint_velocity)
        # self.ur.move_to_location(ur_deck.VIAL_SEQUENCE['r_pickup1'], velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['rotor_engage'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.9)
        self.ur.move_to_location(self.vial_sequence['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def place_vial_in_centrifuge(self):
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['rotor_engage'], velocity=self.joint_velocity)
        # self.ur.move_to_location(ur_deck.VIAL_SEQUENCE['r_pickup1'], velocity=self.linear_velocity)
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.close_gripper(1)
        self.ur.move_joints(self.vial_sequence.joints['push_vial'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def pickup_cover_from_centrifuge(self):
        # move arm out of the view of camera
        self.ur.move_joints(self.centrifuge_sequence.joints['center_sh'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.05)
        # self.ur.move_joints(ur_deck.CENTRIFUGE_SEQUENCE.joints['cent_cover_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['cover_engage'] +Location(z=3), velocity=self.joint_velocity)
        self.ur.close_gripper(0.60)
        self.ur.move_joints(self.centrifuge_sequence.joints['center_sh'], velocity=self.joint_velocity)

    def place_cover_in_safe_location(self):
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_mid'], velocity=self.joint_velocity)
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['cover'] +Location(z=3), velocity=self.joint_velocity)
        self.ur.close_gripper(0.1)
        self.ur.move_to_location(self.centrifuge_sequence['cover_sh'], velocity=self.joint_velocity)
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_mid'], velocity=self.joint_velocity)

    def pickup_cover_from_safe_location(self):
        self.ur.close_gripper(0.1)
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['cover']+Location(z=3), velocity=self.joint_velocity)
        self.ur.close_gripper(0.60)
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_sh'], velocity=self.joint_velocity)
        self.ur.move_joints(self.centrifuge_sequence.joints['cover_mid'], velocity=self.joint_velocity)


    def place_cover_on_centrifuge(self):
        self.ur.move_joints(self.centrifuge_sequence.joints['center_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['cover_engage']+Location(z=3), velocity=self.linear_velocity)
        self.ur.close_gripper(0.1)
        self.ur.move_to_location(self.centrifuge_sequence['center_sh'], velocity=self.linear_velocity)

    def remove_cover(self):
        self.pickup_cover_from_centrifuge()
        self.place_cover_in_safe_location()

    def place_cover(self):
        self.pickup_cover_from_safe_location()
        self.place_cover_on_centrifuge()

    def open_tube(self, index: str = 'front'):
        """
        Pop open eppendorf tube at selected cap station
        :param index: (str) front or back
        """
        if index is 'front':
            location = self.tube_grid.locations['A3']
        else:
            location = self.tube_grid.locations['C3']
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        self.ur.move_to_location(location + Location(z=9), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(z=20), velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(location + Location(z=4.12), velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        self.ur.close_gripper(.6)
        self.ur.move_to_location(location + Location(z=7.71), velocity=self.linear_velocity)
        self.ur.close_gripper(0.82)
        # prev 0.85
        self.ur.move_to_location(location + Location(y=11.7, z=8.52), velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(location + Location(y=-13.3, z=9), velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        self.ur.move_to_location(location + Location(y=23.7, z=9), velocity=self.linear_velocity)
        # self.ur.move_to_location(location + Location(y=23.7, z=-0.98), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(y=23.7, z=-3), velocity=self.linear_velocity)
        time.sleep(2)
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)

    def re_open_tube(self, index: str = 'front'):
        """
        Pop open eppendorf tube at selected cap station
        :param index: (str) front or back
        """
        if index is 'front':
            location = self.tube_grid.locations['A3']
        else:
            location = self.tube_grid.locations['C3']
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(y=-13.3, z=9), velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        self.ur.move_to_location(location + Location(y=23.7, z=9), velocity=self.linear_velocity)
        # self.ur.move_to_location(location + Location(y=23.7, z=-0.98), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(y=23.7, z=-3), velocity=self.linear_velocity)
        time.sleep(2)
        self.ur.move_to_location(location + Location(y=23.7, z=30), velocity=self.linear_velocity)


    def close_tube(self, index: str = 'front'):
        """
        Push close eppendorf tube at selected cap station
        :param index: (str) 'front' or 'back'
        """
        if index is 'front':
            location = self.tube_grid.locations['A3']
        else:
            location = self.tube_grid.locations['C3']
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)
        # self.ur.close_gripper(0)
        # self.ur.move_to_location(location + Location(y=27.7, z=4.12), velocity=self.linear_velocity)
        # self.ur.close_gripper(0.85)
        # self.ur.move_to_location(location + Location(y=27.7, z=9.01), velocity=self.linear_velocity)
        self.ur.close_gripper(0)
        self.ur.move_to_location(location + Location(y=31.7, z=13.99), velocity=self.joint_velocity)
        self.ur.close_gripper(1)
        self.ur.move_to_location(location + Location(z=14.01), velocity=self.joint_velocity)
        self.ur.move_to_location(location + Location(z=7.5), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(z=120), velocity=self.linear_velocity)

    def filter_to_tube(self, index: str = 'front'):
        """
        *** TUBE MUST BE OPEN
        UR place filter at indicated cap station
        :param index: (str) 'front'/'back'
        """
        if index is 'front':
            location = self.tube_grid.locations['A3']
        else:
            location = self.tube_grid.locations['C3']
        engage_location = location + Location(z=5)
        safe_location = location + Location(z=120)
        above_location = location + Location(z=25)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(above_location + Location(y=-5))
        self.ur.move_to_location(above_location)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)

    def filter_from_tube(self, index: str = 'front'):
        """
        ** TUBE MUST BE OPEN
         UR pick up filter from indicated cap station
         :param index: (str) 'front'/'back'
         """
        if index is 'front':
            location = self.tube_grid.locations['A3']
        else:
            location = self.tube_grid.locations['C3']
        engage_location = location + Location(z=3)
        safe_location = location + Location(z=120)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)
        self.ur.move_to_location(engage_location, velocity=self.linear_velocity)
        self.ur.close_gripper(0.75)
        self.ur.move_to_location(safe_location, velocity=self.linear_velocity)

    def relative_index_location(self, index: int, angle: float, radius: [float] = None):
        centrifuge_radius = radius if radius is not None else self.centrifuge_radius
        index_angle = index * 60.0 + angle
        return Location(
            x=math.cos(math.radians(index_angle)) * centrifuge_radius,
            y=math.sin(math.radians(index_angle)) * centrifuge_radius
        )

    def measure_centrifuge_angle(self) -> float:
        # arm.move_joints(centrifuge_camera_joints, velocity=self.joint_velocity)
        return self.centrifuge_start_angle - self.angle_measurement.measure_angle()

    def rotate_centrifuge(self):
        self.pickup_rod()
        angle = self.measure_centrifuge_angle()
        index_location = self.centrifuge_location + self.relative_index_location(0, angle)
        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=-self.plunge_depth), velocity=self.linear_velocity)
        final_angle = self.centrifuge_fudge_angle
        rotation = angle - final_angle
        lower_centrifuge_location = self.centrifuge_location.translate(z=-self.plunge_depth)
        midpoint = lower_centrifuge_location + self.relative_index_location(0, angle / 2)
        endpoint = lower_centrifuge_location + self.relative_index_location(0, 0)
        fudge_endpoint = lower_centrifuge_location + self.relative_index_location(0, self.centrifuge_fudge_angle)
        print(abs(rotation))
        if abs(rotation) > 80:
            self.ur.move_circular(midpoint, endpoint, velocity=self.rotate_velocity)
        else:
            self.ur.move_to_location(endpoint, velocity=self.rotate_velocity)
        self.ur.move_to_location(fudge_endpoint, velocity=self.rotate_velocity)
        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.place_rod()

    def switch_filter(self, leave_front_empty: bool = True):
        """
        switch filter between front and back
        :return:
        """
        self.filter_from_tube('back')
        self.tube_to_tray(tube_index='B3', filter=True)
        self.re_open_tube('back')
        self.filter_from_tube('front')
        self.filter_to_tube('back')
        if not leave_front_empty:
            self.tube_from_tray(tube_index='B3', filter=True)
            self.filter_to_tube('front')
        # self.close_tube('front')
        # self.close_tube('back')

    def push_tube_down(self, tube_index: str = 'A3', open: bool = False):
        location = self.tube_grid.locations[tube_index]
        self.ur.move_to_location(location + Location(z=60), velocity=self.linear_velocity)
        self.ur.close_gripper(1)
        if not open:
            self.ur.move_to_location(location + Location(z=9.5), velocity=self.linear_velocity)
        if open:
            self.ur.move_to_location(location + Location(z=7.5), velocity=self.linear_velocity)
        self.ur.move_to_location(location + Location(z=60), velocity=self.linear_velocity)

    def vial_to_stirrer(self):
        self.ur.move_to_location(self.centrifuge_sequence['stir_engage'] - Location(z=12), velocity=self.linear_velocity)
        self.ur.close_gripper(0)
        self.ur.move_to_location(self.centrifuge_sequence['stir_approach'], velocity=self.linear_velocity)

    def vial_from_stirrer(self):
        self.ur.move_to_location(self.centrifuge_sequence['stir_engage'] - Location(z=12), velocity=self.linear_velocity)
        self.ur.close_gripper(0.8)
        self.ur.move_to_location(self.centrifuge_sequence['stir_approach'], velocity=self.linear_velocity)

    def tube_to_stirrer(self, tube_index: str = 'A3'):
        self.tube_from_tray(tube_index)
        self.ur.move_joints(self.centrifuge_sequence.joints['stir_approach'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['stir_engage'] + Location(z=-3), velocity=self.linear_velocity)

    def tube_from_stirrer(self, tube_index: str = 'A3'):
        self.ur.move_to_location(self.centrifuge_sequence['stir_approach'], velocity=self.linear_velocity)
        self.tube_to_tray(tube_index)
        self.push_tube_down(tube_index)

    def tube_to_shaker(self):
        self.home()
        self.vial_handling.home()
        self.ur.move_joints(self.centrifuge_sequence.joints['shaker_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_approach'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.85)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_engage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_approach'], velocity=self.linear_velocity)
        self.ur.move_joints(self.centrifuge_sequence.joints['shaker_above'], velocity=self.joint_velocity)

    def tube_from_shaker(self):
        self.ur.open_gripper(0.6)
        self.vial_handling.push_shaker()
        self.ur.move_joints(self.centrifuge_sequence.joints['shaker_above'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_approach'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.2)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_engage'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.85)
        self.ur.move_to_location(self.centrifuge_sequence['shaker_approach'], velocity=self.linear_velocity)
        self.ur.open_gripper(0.80)
        self.ur.move_joints(self.centrifuge_sequence.joints['shaker_above'], velocity=self.joint_velocity)
        self.vial_handling.home()
        self.home()










if __name__ == "__main__":
    from ftdi_serial import Serial
    print('start')
    relay = RelayBoard(Serial(device_serial='FT000001'))

    UR_arm = UR3Arm('192.168.254.88')
    arm = UR_arm
    centrifuge_angle_measurement = VisionDotAngleMeasurement(
        '../../../UR/components/vision_angle_measurement/roi_selection.json',
        '../../../UR/components/vision_angle_measurement/colour_limits.yaml', camera_index=2)
    centrifuge = Centrifuge(arm, centrifuge_angle_measurement,
                        centrifuge_sequence_file='../../../UR/configuration/sequences/centrifuge_new.script',
                        vial_sequence_file='../../../UR/configuration/sequences/cent_tube.script',
                        tube_grid=tube_grid, relay=relay)
    # centrifuge.home()
    # centrifuge.filter_from_tube()
    # centrifuge.ur.move_joints(centrifuge.vial_sequence.joints['r_safe_height1'], velocity=centrifuge.joint_velocity)
    # centrifuge.home()
    # centrifuge.switch_filter(leave_front_empty=False)
    for i in range(10):
        centrifuge.run_centrifuge(3, cover_off=True)

    # open_tube('back')
    # close_tube('back')
    # tube_from_tray('A1')

    # home_shaker()
    # push_filter_in_shaker(shaker_index='A3')
    # push_filter_in_shaker(shaker_index='B4')
    # open_shaker_gripper()
    # close_shaker_gripper()

    # holder_to_quantos()
    # holder_from_quantos()
    # tube_from_tray()
    # tube_to_capstation()
    # open_tube()
    # tube_from_opencap()
    # to_innertube_holder()
    # tube_to_quantos()
    # tube_from_quantos()
    # from_innertube_holder()
    # tube_to_opencap(1)
    # tube_from_tray('B1')
    # tube_to_capstation('back')
    # open_tube('back')
    # filter_from_capstation('back')
    # filter_to_tube_holder()
    # filter_from_capstation()  # filter
    # filter_to_capstation('back')  # Filter
    # close_tube('back')  # add a push
    # filter_from_tube_holder()
    # filter_to_capstation()  # filter
    # close_tube()
    # filter_from_capstation()
    # tube_to_tray()
    # filter_from_capstation('back')
    # tube_to_tray('B1')
