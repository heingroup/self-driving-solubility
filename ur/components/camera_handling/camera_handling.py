import cv2
import os
import datetime
import time

class CameraHandling:
    def __init__(self):
        self.cam = cv2.VideoCapture(0, cv2.CAP_DSHOW) # connect to the default camera
        self.folder = 'C:\\Users\\User\\PycharmProjects\\self_driving_solubility\\ur\\components\\camera_handling\\pic_archive'

    def take_picture(self, file_name):
        file_name = file_name + '.png'
        self.cam.set(3, 1920)
        self.cam.set(4, 1080)
        ret, frame = self.cam.read() # read a frame from the camera
        today = datetime.date.today().strftime('%Y-%m-%d') # get the current date as a string
        date_folder = os.path.abspath(os.path.join(self.folder, today)) # create the date folder path
        if not os.path.exists(date_folder): # create date folder if it doesn't exist
            os.mkdir(date_folder)
        file_path = os.path.join(date_folder, file_name) # construct the file path
        cv2.imwrite(file_path, frame) # save the frame as an image file
        time.sleep(1)

if __name__ == "__main__":
    camera = CameraHandling()
    cam = cv2.VideoCapture(0, cv2.CAP_DSHOW)

    camera.take_picture(file_name='A1_20')


