import time

from hein_robots.robotics import Location
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from mtbalance import ArduinoAugmentedQuantos
from hein_robots.universal_robots.ur3 import UR3Arm

# quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)
# del(quan)
# quan = ArduinoAugmentedQuantos('192.168.254.2', 7, logging_level=10)

def vial_type_handler(vial_type:str):
    quantos_move = quantos_move_by_vial[vial_type]
    holder_offset = holder_offset_by_vial[vial_type]
    gripper = gripper_by_vial[vial_type]
    return quantos_move, holder_offset, gripper

quantos_move_by_vial = {
    'hplc_in': 5675,
    'hplc_out': 5675,
    'hplc': 5675,# 5900 6600
    'centrifuge': 5950,
    'push_filter': 0,
    'glass_cup': 6050,
    'cent_adapter': 0
}
holder_offset_by_vial = {
    'hplc_in': Location(z=-1),  # previously z=-6
    'hplc_out': Location (z=-6),
    'hplc': Location(z=-2),
    'centrifuge': Location(z=-3.5),
    'push_filter': Location(z=-1),
    'glass_cup': Location(z=-8),
    'cent_adapter': Location(z=-9)
}
gripper_by_vial = {
    'hplc_in': 0.77,  # 6600
    'hplc_out': 0.78,
    'hplc': 0.78,
    'centrifuge': 0.75,
    'push_filter': 0.80,
    'glass_cup':0.755,
    'cent_adapter': 0.78
}

class QuantosHandling:
    def __init__(self, ur: UR3Arm, quantos: ArduinoAugmentedQuantos, quantos_sequence_file: str,
                 joint_velocity: float = 60, linear_velocity: float = 80):
        self.ur = ur
        self.quan = quantos
        self.QUANTOS_SEQUENCE = URScriptSequence(quantos_sequence_file)
        self.JOINT_VELOCITY = joint_velocity
        self.LINEAR_VELOCITY = linear_velocity

    def open_all_doors(self):
        self.quan.open_side_doors()
        self.quan.front_door_position = 'open'

    def holder_to_quantos(self, test_mode: bool = False):
        """
        UR pick up holder from tray, place filter holder in quantos
        """
        if not test_mode:
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)
            self.quan.open_side_doors()
            self.quan.front_door_position = 'open'
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['holder_sh'], velocity=self.JOINT_VELOCITY)
        self.ur.close_gripper(0)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_engage'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0.35)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_engage'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_sh'], velocity=self.LINEAR_VELOCITY)
        if not test_mode:
            self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_sh'], velocity=self.LINEAR_VELOCITY)
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)
            self.quan.close_side_doors()
            self.quan.front_door_position = 'close'

    def holder_from_quantos(self, test_mode: bool = False):
        """
        UR picks up filter holder from quantos
        :param test_mode: bypass qunatos door movement for the purpose of testing
        :return:
        """
        if not test_mode:
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)
            self.quan.open_side_doors()
            self.quan.front_door_position = 'open'
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['holder_sh'], velocity=self.JOINT_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_engage'], velocity=self.LINEAR_VELOCITY)
        # self.ur.close_gripper(0.37)
        self.ur.close_gripper(0.35)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['quantos_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_engage'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_sh'], velocity=self.LINEAR_VELOCITY)
        if not test_mode:
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)
            self.quan.close_side_doors()
            self.quan.front_door_position = 'close'
            self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)

    def switch_to_vertical(self):
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)

    def to_left_home(self):
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['midpoint_v'], velocity=self.LINEAR_VELOCITY)

    def switch_to_horizontal(self):
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=self.JOINT_VELOCITY)

    def vial_to_holder(self, vial_type: str = 'centrifuge'):
        """
        ur put vial to quantos holder
        :param vial_type: 'centrifuge', 'hplc', 'push_filter'
        :return:
        """
        _, offset, gripper = vial_type_handler(vial_type)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'] + Location(x=1), velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_engage'] + offset + Location(x=1), velocity=25)
        self.ur.close_gripper(0.3)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)

    def vial_to_holder_loc(self, vial_type: str = 'centrifuge'):
        """
        ur put vial to quantos holder
        :param vial_type: 'centrifuge', 'hplc', 'push_filter'
        :return:
        """
        _, offset, gripper = vial_type_handler(vial_type)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['midpoint_v'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'] + Location(x=1),
                                 velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_engage'] + offset + Location(x=1),
                                 velocity=25)
        self.ur.close_gripper(0.3)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['midpoint_v'], velocity=self.LINEAR_VELOCITY)

    def vial_from_holder(self, vial_type: str = 'centrifuge'):
        """
        ur pick up vial from quantos holder
        :param vial_type: 'centrifuge', 'hplc', 'push_filter'
        :return:
        """
        _, offset, gripper = vial_type_handler(vial_type)
        self.ur.close_gripper(0.3)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_engage']+offset, velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(gripper)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['holder_v_sh'] , velocity=self.LINEAR_VELOCITY)
        self.ur.move_joints(self.QUANTOS_SEQUENCE.joints['midpoint_v'], velocity=self.JOINT_VELOCITY)

    def grab_centrifuge_adapter(self):
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'] + Location(z=200),
                                 velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0.78)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'] + Location(z=200), velocity=self.LINEAR_VELOCITY)

    def return_centrifuge_adapter(self):
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'] + Location(z=200), velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'], velocity=self.LINEAR_VELOCITY)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['cent_adapter'] + Location(z=200),
                                 velocity=self.LINEAR_VELOCITY)

    def zero_quantos(self):
        # tare
        self.quan.front_door_position = "close"
        self.quan.zero()

    def open_side_door(self):
        self.quan.open_side_doors()

    def close_side_door(self):
        self.quan.close_side_doors()

    def weigh_with_quantos(self, take_out: bool = True):
        """
        UR takes holder in, weigh out, UR takes holder out
        :param take_out:
        :return: weight
        """
        self.quan.home_z_stage()
        self.zero_quantos()
        self.holder_to_quantos()
        time.sleep(5)
        weight = float(self.quan.weight_immediately)
        print(weight)
        if weight < 0.001:
            input("no object is on")
        # deck.quan.front_door_position = "open"
        if take_out:
            self.holder_from_quantos()
        return weight  # in g

    def go_in_quantos_wrong_way(self):
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['side'], velocity=self.LINEAR_VELOCITY)
        self.ur.move_to_location(self.QUANTOS_SEQUENCE.locations['side_in'], velocity=self.LINEAR_VELOCITY)

    def home_z_stage(self):
        self.quan.home_z_stage()



    def dose_with_quantos(self, solid_weight: float = 0, vial_type: str = 'hplc', bring_in: bool = True):
        """

        :param solid_weight: in mg
        :return:
        """
        steps = quantos_move_by_vial[vial_type]
        initial_solid_weight = solid_weight
        #
        if bring_in:
            self.quan.home_z_stage()
            self.holder_to_quantos()
            # z stage down

        self.quan.move_z_stage(steps=steps)
        # close door
        self.quan.front_door_position = "close"
        # dose
        self.quan.target_mass = solid_weight  #mg
        self.quan.target_volume = solid_weight * 0.001 #ml for seltzer
        # Quantos TEST
        self.quan.start_dosing(wait_for=True)
        # try:
        x = self.quan.sample_data.quantity
        # print("sample data: ", x)
        # x = float(self.quan.weight_immediately)
        # except ValueError:
        # x = self.quan.weight_immediately
        # self.quan.wait_for()  # sleep a bit to wait for it\

        # x0=deck.quan.weight_immediately
        # x = float(x)
        # print(solid_weight-x)
        # loop to fix dosing error
        # while solid_weight-(x*1000) > 5:
        #    deck.quan.target_mass = float(solid_weight-(x*1000)) # mg
        #    deck.quan.start_dosing(wait_for=True)
        #    y = deck.quan.sample_data.quantity
        #    x += float(y)
        mass_no_units = float(x) * 1000  # convert mass to mg
        total_dosed = mass_no_units
        # todo try this second attempt condition
        while total_dosed < initial_solid_weight * 0.9:  # if dosed nothing on the first try , try dosing again
            solid_weight -= mass_no_units
            self.quan.target_mass = solid_weight  # mg
            self.quan.start_dosing(wait_for=True)
            x = self.quan.sample_data.quantity
            # x = float(self.quan.weight_immediately)

            mass_no_units = float(x) * 1000  # convert mass to mg
            total_dosed += mass_no_units
            print("second attempt because of no dosing on the first attempt")

        print('dosed ', total_dosed, 'mg')
        self.quan.home_z_stage()

        if bring_in:
            # open door
            self.quan.front_door_position = "open"
            self.holder_from_quantos()

        # return mass solid dosed in g
        return total_dosed

        # raise ValueError('Unable to dose Quantos after 3 retries')
        # TEST end


if __name__ == "__main__":



    UR_arm = UR3Arm('192.168.254.88')
    arm = UR_arm
    # Quantos
    # middlebox: COM7
    # no middlebox: COM13
    quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)
    # quan.home_z_stage()
    # quan.move_z_stage(steps=6000)
    # quan.start_dosing(wait_for=True)
    # quan.home_z_stage()
    # quan.front_door_position = 'open'
    # quan.front_door_position = 'close'

    # quan.unlock_dosing_pin_position()
    #
    QUANTOS_SEQUENCE_FILE = '../../configuration/sequences/quantos.script'
    qh = QuantosHandling(arm,quan,QUANTOS_SEQUENCE_FILE)
    # qh.switch_to_horizontal()
    # quan.front_door_position = 'open'
    # quan.front_door_position = 'close'
    # qh.open_side_door()
    # qh.quan.home_z_stage()
    for i in range(5):
        # qh.holder_to_quantos()
        mass_dosed = qh.dose_with_quantos(2)
        print(mass_dosed)
        # qh.holder_from_quantos()
        # qh.weigh_with_quantos()
    # qh.ur.move_joints(qh.QUANTOS_SEQUENCE.joints['midpoint_h'], velocity=qh.JOINT_VELOCITY)
    # qh.quan.lock_dosing_pin_position()
    # qh.dose_with_quantos(5,'hplc')
    # qh.dose_with_quantos(5,'push_filter')

    # qh.quan.unlock_dosing_pin_position()
    # qh.quan.set_home_direction(0)

    # qh.vial_from_holder()
    # qh.vial_to_holder()
