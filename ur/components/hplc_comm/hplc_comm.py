import socket


class HPLC_Comm:
    def __init__(self, ip: str, port: int):
        self.ip = ip
        self.port = port


    def send_tcp_message(self, message):
        """
        Send a TCP message to a receiver at the given IP address and port.
        Returns True if the message was sent successfully, False otherwise.
        """
        try:
            # Create a TCP socket and connect to the receiver
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((self.ip, self.port))

            # Send the message to the receiver
            sock.sendall(message.encode())

            # Close the socket and return success
            sock.close()
            return True
        except Exception as e:
            print(f"Error sending TCP message: {e}")
            return False

if __name__ == '__main__':
    ip = '137.82.65.97'
    port = 29620
    message = 'go'
