import logging
from rpc_gateway.server import Server

# from ur.configuration.ur_deck import *
class Printer:
    def __init__(self):
        pass

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

GATEWAY_URL = 'wss://rpc.heinlab.com'
auth_key = 'f74b294022a996c125c2380adfaa66bb'
rpc_server = Server(GATEWAY_URL, auth_key=auth_key)


rpc_server.register('ur_centrifuge', Printer)

rpc_server.start()

print('done')