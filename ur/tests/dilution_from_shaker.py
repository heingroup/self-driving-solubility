import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling
from scipy import stats
import json
from datetime import datetime
import time

number_of_samples = 8
sample_volume = 0.02
dilution_factor = 50

#DOING FROM CUP, CHANGE BACK TO SHAKER
shaker_list = ['A1', 'A1', 'A1', 'A1', 'A1']

samplomatic.prime(0.5)
vial_handling.push_shaker()
somatic.grab_samplomatic()
for i in range(number_of_samples):
    somatic.draw_sample_from_shaker(volume_ml=sample_volume, shaker_index='A1')
    # somatic.draw_sample_from_cup(volume_ml=sample_volume, sample_index='A1')
    if (sample_volume*dilution_factor) <= 0.3:
        vial_index = deck_consumables.get_clean_vial(insert=True)[0]
    else:
        vial_index = deck_consumables.get_clean_vial(insert=False)[0]
    somatic.dispense_hplc_grid(vial_index=vial_index, volume_ml=sample_volume*dilution_factor)
    somatic.return_samplomatic()
    samplomatic.prime(0.3)
    somatic.grab_samplomatic()

somatic.return_samplomatic()