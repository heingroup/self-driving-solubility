import time
from ur.configuration.ur_deck import somatic, samplomatic
from ur.configuration import deck_consumables

sample_volume = 0.07
dilution_factor = 10
number_of_samples = 2

samplomatic.prime(2)
somatic.grab_samplomatic()
for i in range(number_of_samples):
    somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3')
    if (sample_volume * dilution_factor) <= 0.3:
        vial_index = deck_consumables.get_clean_vial(insert=True)[0]
    elif (sample_volume*dilution_factor) < 0.6:
        print('invalid dispense volume, check and run again')
        exit()
    else:
        vial_index = deck_consumables.get_clean_vial(insert=False)[0]
    somatic.dispense_hplc_grid(vial_index=vial_index, volume_ml=sample_volume*dilution_factor)
somatic.return_samplomatic()
