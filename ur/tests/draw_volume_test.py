import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, quantos_handling
from scipy import stats
import json
from datetime import datetime

def new_sample():
    with open('draw_volume_test.json') as fp:
        listObj = json.load(fp)
        listObj.append({'date': str(datetime.now()),
                        'solvent':'None',
                        'slurry_volume':'None',
                        'sample_volume':'None',
                        'actual_volume':'None',})
        with open('draw_volume_test.json', 'w') as json_file:
            json.dump(listObj, json_file)

def save_value(data_name, data_value):
    with open('draw_volume_test.json') as fp:
        listObj = json.load(fp)
    listObj[-1][data_name] = data_value
    with open('draw_volume_test.json', 'w') as json_file:
        json.dump(listObj, json_file)

if __name__ == '__main__':

    number_of_samples = 2
    solvent = 'heptane'
    slurry_volume = 0.35
    sample_volume = 0.1
    solvent_density = deck_consumables.get_stock_info(solvent)[0]
    stock_index = deck_consumables.get_stock_info(solvent)[2]

    samplomatic.prime(1)

    for i in range(number_of_samples):
        #Get indexes
        cup_index = deck_consumables.get_clean_cup()
        filter_index = deck_consumables.get_clean_filter()
        shaker_index = deck_consumables.get_empty_shaker_location('sample')

        #dose solvent in cup
        samplomatic.prime(0.1)
        somatic.grab_samplomatic()
        somatic.aspirate_stock_solvent(volume_ml=slurry_volume, stock_index=stock_index)
        somatic.dispense_solvent_in_cup(volume_ml=slurry_volume, sample_index=cup_index)
        somatic.return_samplomatic()

        #Place filter in cup and move to shaker
        vial_handling.cup_from_tray(cup_index=cup_index)
        vial_handling.cup_to_aligner()
        vial_handling.filter_from_tray(filter_index=filter_index, large_filter=True)
        vial_handling.align_filter()
        vial_handling.insert_filter()
        vial_handling.cup_from_aligner()
        vial_handling.cup_to_shaker(shaker_index=shaker_index)

        #Get clean HPLC vial and weigh
        vial_index = deck_consumables.get_clean_vial()[0]
        vial_handling.vial_from_tray(vial_index=vial_index)
        vial_handling.home()
        quantos_handling.vial_to_holder(vial_type='hplc')
        vial_mass = quantos_handling.weigh_with_quantos()
        quantos_handling.switch_to_vertical()
        vial_handling.home()

        #Filter vial
        vial_handling.push_filter_in_shaker(shaker_index=shaker_index)
        vial_handling.cup_from_shaker(shaker_index=shaker_index)
        vial_handling.cup_to_tray(cup_index=cup_index)

        #Draw solvent and dispense in weighed vial
        samplomatic.prime(volume_ml=0.1)
        somatic.grab_samplomatic()
        somatic.draw_sample_from_cup_with_airgap(volume_ml=sample_volume, sample_index=cup_index)
        vial_handling.home()
        somatic.dispense_quantos_holder(volume_ml=sample_volume)
        somatic.return_samplomatic()

        #Weigh vial with quantos holder in it
        vial_handling.home()
        quantos_handling.switch_to_horizontal()
        vial_solvent_mass = quantos_handling.weigh_with_quantos()
        solvent_mass = vial_solvent_mass - vial_mass
        actual_volume = solvent_mass/solvent_density

        quantos_handling.vial_from_holder(vial_type='hplc')
        vial_handling.home()
        vial_handling.vial_to_tray(vial_index)

        new_sample()
        save_value('solvent', solvent)
        save_value('slurry_volume', str(slurry_volume))
        save_value('sample_volume', str(sample_volume))
        save_value('actual_volume', str(actual_volume))






