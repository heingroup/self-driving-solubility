import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, quantos_handling
from scipy import stats
import json
from datetime import datetime

def new_calibration():
    with open('../experiments/data/pump_calibration.json') as fp:
        listObj = json.load(fp)
        listObj.append({'date': str(datetime.now()),
                        'solvent':'None',
                        'slope':'None',
                        'intercept':'None',
                        'r_squared':'None'})
        with open('../experiments/data/pump_calibration.json', 'w') as json_file:
            json.dump(listObj, json_file)

def save_value(data_name, data_value):
    with open('../experiments/data/pump_calibration.json') as fp:
        listObj = json.load(fp)
    listObj[-1][data_name] = data_value
    with open('../experiments/data/pump_calibration.json', 'w') as json_file:
        json.dump(listObj, json_file)

if __name__ == '__main__':
    n = 0
    solvent = 'THF'
    input_volumes = [0.5, 0.7, 0.9, 1.1, 1.3]
    actual_volumes = []
    solvent_density = deck_consumables.get_stock_info(solvent)[0]
    stock_index = deck_consumables.get_stock_info(solvent)[2]

    # samplomatic.prime(2)

    for i in input_volumes:
        # vial_index = deck_consumables.get_clean_vial()[0]
        # vial_handling.vial_from_tray(vial_index=vial_index)
        # vial_handling.home()

        # quantos_handling.vial_to_holder(vial_type='hplc')

        vial_mass = quantos_handling.weigh_with_quantos()

        quantos_handling.switch_to_vertical()
        vial_handling.home()

        if solvent == 'THF':
            samplomatic.prime(0.1)
            somatic.grab_samplomatic()
            somatic.dispense_quantos_holder(volume_ml=i, backend = True)
            somatic.return_samplomatic()
        else:
            somatic.grab_samplomatic()
            if n == 0:
                somatic.aspirate_stock_solvent(volume_ml=0.3, stock_index=stock_index)
            somatic.aspirate_stock_solvent(volume_ml=i, stock_index=stock_index)
            somatic.dispense_quantos_holder(volume_ml=i)
            somatic.return_samplomatic()

        vial_handling.home()
        quantos_handling.switch_to_horizontal()

        vial_solvent_mass = quantos_handling.weigh_with_quantos()

        solvent_mass = vial_solvent_mass - vial_mass
        actual_volume = solvent_mass/solvent_density

        actual_volumes.append(actual_volume)

        quantos_handling.vial_from_holder(vial_type='hplc')
        vial_handling.home()
        vial_handling.vial_to_tray(vial_index)
        n += 1

    df = pd.DataFrame({'input_volume':input_volumes, 'actual_volume':actual_volumes})
    df.to_csv('pump_calibration.csv', index=False)

    slope, intercept, r_value, p_value, std_err = stats.linregress(actual_volumes, input_volumes)

    print(slope)
    print(intercept)
    print(r_value)

    r_squared = r_value**2

    new_calibration()
    save_value('solvent',solvent)
    save_value('slope',str(slope))
    save_value('intercept', str(intercept))
    save_value('r_squared', str(r_squared))




