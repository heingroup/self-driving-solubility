import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, quantos_handling,centrifuge, cap_handling
import time
import logging
from datetime import datetime


if __name__ == '__main__':
    #SETTINGS - CHANGE BEFORE YOU BEGIN RUN

    #CHECKLIST - DID YOU...?
    #CHANGE LEVI_CONFIG
    #REFILL SOLVENTS
    #REFILL CUPS
    #REFILL FILTERS
    #MAKE SURE ALIGNER AND SHAKER ARE EMPTY
    number_of_samples = 2
    initial_volume = 1
    solid_mass_mg = 5.92
    dilution_factor = 10
    sample_volume = 0.06
    use_quantos = False
    predosed_vial_index = 'A1'
    solvent = 'THF'
    slope, intercept, slope_sample, intercept_sample = deck_consumables.get_calibration_info(solvent)
    initial_volume_calibrated = initial_volume * slope + intercept

    vial_handling.home()

    for n in range(number_of_samples):
        if n==0:
            if use_quantos:
                vial_index = deck_consumables.get_clean_vial()[0]
                vial_handling.vial_from_tray(vial_index = vial_index, grab_cap=True)
                vial_handling.home()
                centrifuge.home()

                #uncap
                cap_handling.uncap()
                cap_handling.vial_from_capper(cap_handling.cap_on)
                cap_handling.vial_to_holder(cap_handling.cap_on)

                quantos_handling.switch_to_horizontal()
                dosed_mass = quantos_handling.dose_with_quantos(solid_weight=float(solid_mass_mg),
                                                            vial_type='hplc')
                print(dosed_mass)
                quantos_handling.switch_to_vertical()

                cap_handling.vial_from_holder(cap_handling.cap_on)
                cap_handling.vial_to_capper(cap_handling.cap_on)
                cap_handling.cap()

                #cap

                centrifuge.home()
                vial_handling.home()
                vial_handling.vial_to_tray(vial_index=vial_index)
            else:
                vial_index = predosed_vial_index

            samplomatic.prime(volume_ml=2)
            somatic.grab_samplomatic()
            somatic.dispense_hplc_grid(volume_ml=initial_volume_calibrated, vial_index=vial_index)
            somatic.return_samplomatic()

            vial_handling.vial_from_tray(predosed_vial_index)
            vial_handling.cup_to_shaker('A1')
            ts.start_shaking()
            time.sleep(2 * 60)
            ts.stop_shaking()
            vial_handling.cup_from_shaker('A1')
            vial_handling.vial_to_tray(predosed_vial_index)

        new_vial_index = deck_consumables.get_clean_vial()[0]
        sample_volume_calibrated = (sample_volume * slope_sample) + intercept_sample
        samplomatic.prime(0.2)
        somatic.grab_samplomatic()
        somatic.draw_sample_from_vial(volume_ml=sample_volume, sample_index=vial_index)
        dispense_volume_calibrated = (sample_volume * dilution_factor * slope) + intercept
        somatic.dispense_hplc_grid(volume_ml=sample_volume*dilution_factor, vial_index=new_vial_index)
        somatic.return_samplomatic()

        vial_index = new_vial_index
    # print(dosed_mass)




















