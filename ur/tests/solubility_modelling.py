    # Parse the HPLC data based on number of samples / duplicates
    # Should also factor in dilution factor
    # Link it with experiment parameters (high temp, low temp, cooling rate, solvent, solid loading)
    # Calculate % yield of product of interest and % purity (peak area pdt / sum of peak area)
    ## pandas to json or pandas to csv


import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

class solubility_modelling_temp:
    def __init__(self, csv_directory, ext_temperature, temperature_values, dilution_factor, product, m, b,
                 solvent_A, solvent_B, solvent_ratio, solid_name):
        self.csv_directory = csv_directory
        self.ext_temperature = ext_temperature
        self.temperature_values = temperature_values
        self.dilution_factor = dilution_factor
        self.product = product
        self.solubility_dfs = []
        self.m = m
        self.b = b
        self.solvent_A = solvent_A
        self.solvent_B = solvent_B
        self.solvent_ratio = solvent_ratio
        self.solid_name = solid_name

    def plot_solubility_data(self, analysis_df, ext_temperature, m, b, A0_optimized, B0_optimized, C0_optimized,
                             solubility_mg_ml_data):
        temperature_range = np.linspace(0, ext_temperature, 100)

        # Calculate the peak area for the given temperature range using the optimized parameters
        peak_area_range = self.vant_hoff_eq(temperature_range, A0_optimized, B0_optimized, C0_optimized)

        solubility_range = (peak_area_range - b) / m

        # Plot the original data, the fitted curve, and the new range of temperatures (solid loading)
        if solvent_B == 'None':
            graph_label = f'Solubility for 0-{ext_temperature}°C in {solvent_A}'
        else:
            graph_label = f'Solubility for 0-{ext_temperature}°C in {solvent_ratio} {solvent_A}:{solvent_B}'

        if solvent_B == 'None':
            graph_title = f'{solid_name} in {solvent_A}'
        else:
            graph_title = f'{solid_name} in {solvent_ratio} {solvent_A}:{solvent_B}'

        plt.scatter(analysis_df.iloc[:, 0], solubility_mg_ml_data, label='Data')
        plt.plot(temperature_range, solubility_range, label=graph_label, color='red', linestyle='dashed')
        plt.xlabel('Temperature (°C)', fontname='Arial')
        plt.ylabel('Solid Loading (mg/mL)', fontname='Arial')
        plt.legend()
        plt.title(f'Modelled Solid Loading (mg/mL) for {graph_title} 0-{ext_temperature} °C', fontname='Arial')
        plt.show()

    def vant_hoff_eq(self, temperature, A0, B0, C0):
        R = 8.314  # Gas constant in J/(mol K)
        T = temperature + 273.15  # Convert temperature to Kelvin

        # Ensure A0, C0, and T are within valid ranges to avoid log issues
        epsilon = 1e-10  # Small value to avoid division by zero
        A0_safe = np.maximum(A0, epsilon)
        C0_safe = np.maximum(C0, epsilon)
        T_safe = np.maximum(T, epsilon)

        # Calculate the solubility
        return C0_safe * np.exp(np.log(A0_safe) - B0 / (R * T_safe))

    def process_csv_files(self, display, save, save_path=None):
        if os.path.isdir(self.csv_directory):
            print("CSV directory exists")
            # Get a sorted list of CSV files in the directory
            csv_files = sorted(
                [filename for filename in os.listdir(self.csv_directory) if filename.endswith('.csv')])

            for i, filename in enumerate(csv_files):
                file_path = os.path.join(self.csv_directory, filename)

                # Read the CSV file into a DataFrame
                df = pd.read_csv(file_path)

                # Replace the first column with the respective temperature value
                df.iloc[:, 0] = self.temperature_values[i % len(self.temperature_values)]

                # Save the modified DataFrame back to the CSV file
                df.to_csv(file_path, index=False)

                # Select the first column (arbitrary) and product columns to create a new DataFrame
                solubility_df = df[['Unnamed: 0', self.product]]

                # Append the new DataFrame to the list
                self.solubility_dfs.append(solubility_df)

            # Combine all DataFrames into one
            analysis_df = pd.concat(self.solubility_dfs, ignore_index=True)

            # Filter rows where the solubility is not zero
            analysis_df = analysis_df[analysis_df.iloc[:, 1] != 0]

            # Apply a dilution factor
            analysis_df.iloc[:, 1] *= self.dilution_factor

            temperature_data = analysis_df.iloc[:, 0].values
            peak_area_data = analysis_df.iloc[:, 1].values

            solubility_mg_ml_data = (peak_area_data - b) / m

            # Fit the Vant Hoff Equation to the data to estimate A0, B0, and C0
            params, covariance = curve_fit(self.vant_hoff_eq, analysis_df.iloc[:, 0], analysis_df.iloc[:, 1],
                                           p0=[1.0, 1.0, 1.0])
            A0_optimized, B0_optimized, C0_optimized = params

            print(A0_optimized)
            print(B0_optimized)
            print(C0_optimized)

            # plot the function using the plot_solubility_data function
            if display:
                self.plot_solubility_data(analysis_df, self.ext_temperature, m, b, A0_optimized, B0_optimized,
                                          C0_optimized, solubility_mg_ml_data)

            # Extrapolate to a new temperature
            extrapolated_solubility = self.vant_hoff_eq(ext_temperature, A0_optimized, B0_optimized, C0_optimized)
            print(extrapolated_solubility)

            # Change peak area to solubility
            predicted_solubility = (extrapolated_solubility - b) / m
            if solvent_B == 'None':
                print(
                    f'Extrapolated impure solid loading at {ext_temperature}°C in {solvent_A}: {predicted_solubility:.2f} mg/mL')
            else:
                print(
                    f'Extrapolated impure solid loading at {ext_temperature}°C in {solvent_ratio} {solvent_A}:{solvent_B}: {predicted_solubility:.2f} mg/mL')

            data = {
                'solid_name': self.solid_name,
                'solvent_A': self.solvent_A,
                'solvent_B': self.solvent_B,
                'solvent_ratio': self.solvent_ratio,
                'A_optimized': A0_optimized,
                'B_optimized': B0_optimized,
                'C_optimized': C0_optimized,
                'ext_temperature in °C': self.ext_temperature,
                'predicted_solubility in mg/mL': predicted_solubility
            }

            # Convert the dictionary to a DataFrame
            result_df = pd.DataFrame(data, index=[0])

            # Save the DataFrame to a CSV file
            if save and save_path:
                result_df.to_csv(save_path, index=False)
                print(f'Data saved to {save_path}')

        else:
            print('Invalid path. Please provide a valid directory path.')


# extrapolated temperature can be changed to be a value taken from the Ax optimizer
ext_temperature = 40
csv_directory = '../experiments/hplc_data/2023-09-20'
# temperature values are the temperatures at which the solubility points were measured.
# the program reads them in order, so please input the temperatures in the order they were measured.
temperature_values = np.array([15.0, 25.0, 35.0, 45.0])
# dilution factor can be found in a configuration file.
dilution_factor = 10
product = 'product_240'
solvent_A = 'heptane'
solvent_B = 'toluene'
solvent_ratio = '1:0'
solid_name = 'Pyrethroic Acid'
save_path = '../self_driving/components/solubility/data/output.csv'  # output data path
# assign m and b to be the slope & intercept of your peak-area-to-solubility-mg/ml line
m = 301.2
b = 22.6
# To create an instance of the class and initialize it:
instance = solubility_modelling_temp(csv_directory, ext_temperature, temperature_values, dilution_factor, product, m, b,
                                solvent_A, solvent_B, solvent_ratio, solid_name)
# Set to True or False depending on whether you want the modelled curve to be shown
instance.process_csv_files(False, True, save_path)


