import torch
from ax.service.ax_client import AxClient
from ax.service.utils.instantiation import ObjectiveProperties
from ax.modelbridge.factory import get_sobol
from ax.utils.notebook.plotting import render, init_notebook_plotting
from ax.modelbridge.cross_validation import cross_validate
from ax.plot.diagnostic import interact_cross_validation
from ax.plot.pareto_utils import compute_posterior_pareto_frontier
from ax.plot.pareto_frontier import plot_pareto_frontier






# for saving and loading data
from ax.storage.json_store.load import load_experiment
from ax.storage.json_store.save import save_experiment
import pandas as pd
import numpy as np
import os
import json
import csv
import random
from ur.self_driving.configuration.self_driving_config import cryst
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm

# ADD NAME OF COMPOUND AND SOLVENT. These must be saved with the output from the optimization and will be used to load previous data
# into the optimizer

## I would reccomend making another fodler in this 'optimization' folder called 'data', with a folder for each compound we work on and another
## file for each solvent we do.
## ie for solvent = 'heptane' and compound = 'pyrethroic acid', the file should be optimization/data/pyrethroic acid/heptane.csv
## therefore, if we need to start the optimizer using already aquirred data, we just need to read this csv
solvent_A = 'heptane'
solvent_B = 'None'
solvent_ratio = 1
solid = 'pyrethroic acid'
duplicates = 3
product_label = 'product_240'
save_file_path = f"data\json\{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.json"
number_of_runs_before_stop = 2
# Number of samples in initial search
init_samples = 0
run_init_samples = False

#If true, will attempt all init runs. If False, will output all parameters and get user to do it
automated_init_runs = False

# Number of total experiments
niter = 20

# If previous data already exists for the solid/solvent, set this to True and follow these steps:
load_prior_data = False
## in data/previous_data, copy the sample_csv and fill in with your data
## name the new csv as '{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.csv
## then, set load_prior_data to False
## set these columns with the names being used in this optimiation
param_cols = ['temp_diff','low_temp','cooling_rate']
outcome_cols = ['yield','purity','rejection']

# ADD CODE TO CHECK FOR ALREADY EXISTING CSV AND READ DATA FROM IT
# If it finds a csv, it should modify the number of init_samples and niter (ie if it took 7 samples already, it should know
# to collect 0 init_samples and 29 niter

def save_new_json_file(file_path):
    data = []
    with open(file_path, 'w') as json_file:
        json.dump(data, json_file)

ax_client = AxClient()
exist = os.path.exists(save_file_path)
if os.path.exists(save_file_path):
    ax_client = AxClient.load_from_json_file(save_file_path)

    # THIS PART DOESNT WORK
    with open(save_file_path, 'r') as file:
        data = json.load(file)
    completed_experiments = len(data['experiment']['trials'])
    niter = niter - completed_experiments + init_samples
    run_init_samples = False

else:
    # os.makedirs(f"data\json\{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.json")
    save_new_json_file(save_file_path)

    ax_client.create_experiment(
        name="Noah_experiment",
        parameters=[
            {
                "name": "temp_diff",
                "type": "choice",
                "values": np.arange(20, 60, 5).tolist(),
                "value_type":'float'
            },
            {
                "name": "low_temp",
                "type": "choice",
                "values": np.arange(5, 20, 5).tolist(),
                "value_type": 'float'
            },
            {
                "name": "cooling_rate",
                "type": "choice",
                "values": np.arange(0.25, 1, 0.25).tolist(),
                "value_type": 'float'
            }

        ],
        objectives={
            # `threshold` arguments are optional
            "yield": ObjectiveProperties(minimize=False, threshold=60),
            "purity": ObjectiveProperties(minimize=False, threshold=80),
            "rejection": ObjectiveProperties(minimize=False, threshold=60)
        },
        overwrite_existing_experiment=True,
    )


def evaluate(params):

    # Planning
    ################################

    # params will be input as a dictionary (i.e. {temp_diff: XX, low_temp: YY, cooling_rate: ZZ})
    # Use this format to get the values from the dictionary (x1, x2 will be the names of the parameters above)
    # x1, x2, x3 = params.get("x1"), params.get("x2"), params.get("x3")

    # Before running the cooling crystallization, the system must:
    ## Modify system_config.csv with the relevant parameters (all 3 will go in system_config.csv)
    ### I recomend making a function in cooling_crystal.py for th

    print(params)
    low_temp, temp_diff, cooling_rate = params.get('low_temp'), params.get('temp_diff'), params.get('cooling_rate')
    cryst.set_sample_config(solid_name=solid, low_temp=low_temp, temp_diff=temp_diff, duplicates=duplicates,
                            solvent_A=solvent_A, solvent_B=solvent_B, solvent_ratio=solvent_ratio, product=product_label,
                            user_fills_vials=False)
    cryst.set_system_config(low_temp=low_temp, temp_diff=temp_diff, cooling_rate=cooling_rate)
    ##########################################################################
    cryst.run_cooling_crystallization()
    #######################################################################
    hplc_file = data_handling.find_most_recent_hplc_data()
    hplc_dir = os.path.dirname(hplc_file)
    hplc_filename = str(os.path.basename(hplc_file))
    summary_file = os.path.join(hplc_dir,'cooling_crystal_summary',f'{hplc_filename}')

    cryst.average_hplc_duplicates(hplc_file=hplc_file, output_file=summary_file, remove_outliers=True)

    # cryst.evaporation_peak_area_correction(csv_file = summary_file, json_file=(os.getcwd() + '/data/log.json'), overwrite=False)

    output_directory = os.getcwd() + '/data' + f'/{solid}'
    output_file = cryst.process_data(input_csv=summary_file, output_directory=output_directory, hplc_file=hplc_file,
                                     product_name='product_240', impurity_name='isomer_210')
    results_dict = cryst.process_experimental_results(csv_file_path=output_file,
                                                      yield_enabled=True, rejection_enabled=True, purity_enabled=True)

    print(results_dict)
    print(params)
    print('add results to optimization/data/previous_data/pyrethroic acid_heptane_None_1.csv, then delete data/json/pyrethroic acid_heptane_None_1.csv')

    return results_dict


if load_prior_data:
    prior_data = f'data/previous_data/{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.csv'
    prev_data = pd.read_csv(prior_data)
    length = prev_data.shape[0]
    print(prev_data)
    for i in range(length):
        params = {col: float(prev_data.at[i,col]) for col in param_cols}
        print(params)
        outcomes = {col: float(prev_data.at[i,col]) for col in outcome_cols}
        print(outcomes)
        _, trial_index = ax_client.attach_trial(parameters=params)
        ax_client.complete_trial(trial_index=trial_index, raw_data=outcomes)
        ax_client.save_to_json_file(save_file_path)


m = get_sobol(ax_client.experiment.search_space)
gr = m.gen(n=init_samples)

if run_init_samples:
    if automated_init_runs:
        for i in range(init_samples):
            # print(gr.arms[i].parameters)

            ax_client.attach_trial(gr.arms[i].parameters)
            ax_client.complete_trial(trial_index=i, raw_data=evaluate(gr.arms[i].parameters))
            ax_client.save_to_json_file(save_file_path)
    else:
        for i in range(init_samples):
            parameters = gr.arms[i].parameters
            print(f'parameters {i+1} is: {parameters}')
        exit()

runs = 0
for i in range(niter):
    parameters, trial_index = ax_client.get_next_trial()
    # Local evaluation here can be replaced with deployment to external system.
    ax_client.complete_trial(trial_index=trial_index, raw_data=evaluate(parameters))
    # save res
    ## CHANGE TO SAVE CSV AS DESCRIBED ABOVE
    ax_client.save_to_json_file(save_file_path)

    runs += 1
    if runs == number_of_runs_before_stop:
        exit()

    # ONCE ALL THESE CHANGES HAVE BEEN MADE:
    ## Test the system using fake data. I would replace all of the code related to cryst.run_cooling_crystallization() and
    ## replace it with yield = input() and purity = input() (or just use a random number generator). We will use this to check
    ## for any errors in the optimizer

print(ax_client.generation_strategy.trials_as_df)
best_parameters = ax_client.get_pareto_optimal_parameters()
print(best_parameters)

objectives = ax_client.experiment.optimization_config.objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[1].metric,
    secondary_objective=objectives[0].metric,
    absolute_metrics=["yield", "purity"],
    num_points=10,
)
render(plot_pareto_frontier(frontier, CI_level=0.90))






