from ax.service.ax_client import AxClient
from ax.service.utils.instantiation import ObjectiveProperties
from ax.modelbridge.factory import get_sobol
from ax.utils.notebook.plotting import render, init_notebook_plotting
from ax.modelbridge.cross_validation import cross_validate
from ax.plot.diagnostic import interact_cross_validation
from ax.plot.pareto_utils import compute_posterior_pareto_frontier,get_observed_pareto_frontiers
from ax.plot.pareto_frontier import plot_pareto_frontier, interact_pareto_frontier
from ax.plot.contour import interact_contour
from ax.modelbridge.generation_strategy import GenerationStrategy, GenerationStep
from ax import Models


import numpy as np
import os
import json
import csv
import random
import pandas as pd

from ax.plot.scatter import (
    interact_fitted,
    plot_objective_vs_constraints,
    tile_fitted,
)

# for saving and loading data
from ax.storage.json_store.load import load_experiment
from ax.storage.json_store.save import save_experiment

solvent_A = 'heptane'
solvent_B = 'None'
solvent_ratio = 1
solid = 'pyrethroic acid'
save_file_path = f"data\json\{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.json"
load_from_json = False

if load_from_json:
    ax_client = AxClient.load_from_json_file(save_file_path)

else:
    gs = GenerationStrategy(
        steps=[
            GenerationStep(
                model=Models.GPEI,
                num_trials=-1,
                max_parallelism=1,
            ),
        ]
    )

    ax_client = AxClient(generation_strategy=gs)
    ax_client.create_experiment(
        name="Noah_experiment",
        parameters=[
            {
                "name": "temp_diff",
                "type": "range",
                "bounds": [20, 60],
                # "type": "choice",
                # "values": np.arange(20, 60, 5).tolist(),
                "value_type": 'float'
            },
            {
                "name": "low_temp",
                "type": "range",
                "bounds": [5, 20],
                # "type": "choice",
                # "values": np.arange(5, 20, 5).tolist(),
                "value_type": 'float'
            },
            {
                "name": "cooling_rate",
                "type": "range",
                "bounds": [0.25, 1],
                # "type": "choice",
                # "values": np.arange(0.25, 1, 0.25).tolist(),
                "value_type": 'float'
            }

        ],
        objectives={
            # `threshold` arguments are optional
            "yield": ObjectiveProperties(minimize=False),
            "purity": ObjectiveProperties(minimize=False),
            "rejection": ObjectiveProperties(minimize=False),
        }
    )

    param_cols = ['temp_diff', 'low_temp', 'cooling_rate']
    outcome_cols = ['yield', 'purity', 'rejection']
    prior_data = f'data/previous_data/{solid}_{solvent_A}_{solvent_B}_{solvent_ratio}.csv'
    prev_data = pd.read_csv(prior_data)
    length = prev_data.shape[0]
    print(prev_data)
    for i in range(length):

        params = {col: float(prev_data.at[i, col]) for col in param_cols}
        print(params)
        outcomes = {col: float(prev_data.at[i, col]) for col in outcome_cols}
        print(outcomes)
        _, trial_index = ax_client.attach_trial(parameters=params)
        ax_client.complete_trial(trial_index=trial_index, raw_data=outcomes)

print('next:', ax_client.get_next_trial())
print('model:', ax_client.generation_strategy.model)
print('best:', ax_client.get_pareto_optimal_parameters)

model = ax_client.generation_strategy.model
# render(interact_contour(model, metric_name='yield'))
# exit()

# data = ax_client.experiment.fetch_data()


cv_results = cross_validate(model)
render(interact_cross_validation(cv_results))

exit()
# pareto = get_observed_pareto_frontiers(experiment=ax_client.experiment, data=data, arm_names=["1_0","2_0","3_0","4_0","5_0"])
# render(interact_pareto_frontier(pareto, CI_level=0.90))

objectives = ax_client.experiment.optimization_config.objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[1].metric,
    secondary_objective=objectives[0].metric,
    absolute_metrics=["purity", "yield"],
    num_points=5,
)
render(plot_pareto_frontier(frontier, CI_level=0.95))