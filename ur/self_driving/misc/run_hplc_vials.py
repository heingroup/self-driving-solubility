import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling
from scipy import stats
import json
from datetime import datetime
import time

if __name__ == '__main__':
    vial_list = ['G1','G2','G3','G4','H1','H2','H3','H4']
    number_of_samples = len(list)