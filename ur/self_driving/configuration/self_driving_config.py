from ur.self_driving.components.standard_curve.standard_curve import StandardCurve
from ur.self_driving.components.solubility.solubility import Solubility
from ur.self_driving.components.reslurry.reslurry import Reslurry
from ur.self_driving.components.cooling_crystal.cooling_crystal import Cooling_Crystal

from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm, cap_handling
import os
import time

##
####
######
#TO-DO
#Add a "check consumbles before run" prompt, tells user to add vials and update csv if not enough
######
####
##

solid_name = 'pyrethroic acid'

compound_settings_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/configuration/compound_settings.csv'
hplc_runtime = data_handling.get_hplc_runtime(solid_name, compound_settings_csv) + 0.75
hplc_method, injection_volume = data_handling.get_hplc_settings(solid_name, compound_settings_csv)

# hplc_comm.send_tcp_message(f'set_sequence_table+{hplc_method}+{injection_volume}')

std_curve_output_folder = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/standard_curve/data'
std_curve_output_json = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/standard_curve/data/standard_curve.json'
average_std_curve_output_json = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/standard_curve/data/standard_curve_averaged.json'


standard_curve = StandardCurve(solid_name=solid_name, hplc_runtime=hplc_runtime, output_json=std_curve_output_json,
                               output_folder=std_curve_output_folder, average_output_json=average_std_curve_output_json,
                               vial_handling=vial_handling,hplc_handling=hplc_handling,hplc_comm=hplc_comm,
                               samplomatic=samplomatic,somatic=somatic,ts=ts,data_handling=data_handling)

solubility_config_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/solubility/solubility_config.csv'
solubility_output_folder = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/solubility/data'
solubility_solvent_bank_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/solubility/solvent_bank.csv'
solubility_cosmo_solvent_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/solubility/cosmo_solvents.csv'

solubility = Solubility(solid_name=solid_name, lower_range=5, upper_range=50, hplc_runtime=hplc_runtime,
                        config_csv=solubility_config_csv,
                        output_folder=solubility_output_folder, cosmo_csv=solubility_cosmo_solvent_csv,
                        solvent_bank_csv=solubility_solvent_bank_csv,
                        vial_handling=vial_handling,hplc_handling=hplc_handling,hplc_comm=hplc_comm,
                        samplomatic=samplomatic,somatic=somatic,ts=ts,data_handling=data_handling,
                        quantos_handling=quantos_handling, cap_handling=cap_handling,centrifuge=centrifuge)

reslurry_config_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/reslurry/reslurry_config.csv'
experiment_log_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/reslurry/data/experiment_log.csv'


reslurry = Reslurry(solid_name=solid_name, hplc_runtime=hplc_runtime,reslurry_config_csv=reslurry_config_csv,
                    experiment_log_csv=experiment_log_csv,vial_handling=vial_handling,hplc_handling=hplc_handling,
                    hplc_comm=hplc_comm,samplomatic=samplomatic,somatic=somatic,ts=ts,data_handling=data_handling,
                    quantos_handling=quantos_handling, centrifuge=centrifuge,shaker_handling=shaker_handling)

sample_config_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/cooling_crystal/sample_config.csv'
system_config_csv = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/cooling_crystal/system_config.csv'


cryst = Cooling_Crystal(solid_name=solid_name, hplc_runtime=hplc_runtime, sample_config_csv=sample_config_csv, system_config_csv=system_config_csv,
                        vial_handling=vial_handling, hplc_handling=hplc_handling, hplc_comm=hplc_comm,
                        samplomatic=samplomatic, somatic=somatic, ts=ts, data_handling=data_handling,
                        quantos_handling=quantos_handling, shaker_handling=shaker_handling, cap_handling=cap_handling)

if __name__ == '__main__':

    #Runs HPLC vials in list
    compound = 'benzocaine'
    hplc_runtime = data_handling.get_hplc_runtime(solid_name, compound_settings_csv) + 0.75

    vial_list = ['G1', 'G2', 'G3', 'G4', 'H1', 'H2', 'H3', 'H4']
    number_of_samples = len(vial_list)

    hplc_comm.send_tcp_message('solubility_' + str(number_of_samples))
    time.sleep(60)
    hplc_handling.run_blank()

    for v in vial_list:
        hplc_handling.run_current_vial(vial_index=v,hplc_runtime=hplc_runtime)

    hplc_handling.return_last_vial()


    print('done')
