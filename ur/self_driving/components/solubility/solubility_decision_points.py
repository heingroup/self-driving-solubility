from ur.self_driving.configuration.self_driving_config import standard_curve
from ur.self_driving.configuration.self_driving_config import solubility
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm
import random

if __name__ == '__main__':

    '''
    reminder to change slurry_volume in solubility_config depnding on if push filter or hplc vials are being used
    hplc vials: 1 - 1.1 mL
    push filter: 0.3 - 0.35 mL
    '''
    # If True, set cosmo_solvents.csv with the solvent systems you want to test
    # If False, deck will use a standardized 8 solvent bank (see solvent_bank.csv)
    cosmo_solvents = True
    peak_label = 'product_240'

    solid_loading_percent = 2.5
    slurry_volume = 0.40
    sample_volume = 0.06
    dilution_factor = 10
    
    temp = 15
    time_temp_min = 100
    cup_index = '..\configuration\consumables_config\cup_info.csv'
    filter_index = '..\configuration\consumables_config\filter_info.csv'

    # solubility.vial_handling.home()
    # solubility.vial_handling.cup_from_aligner()
    # solubility.vial_handling.cup_to_tray('A2')
    # solubility.vial_handling.home()
    # solubility.somatic.aspirate_stock_solvent(0,'E2')
    # solubility.somatic.return_samplomatic()
    # solubility.vial_handling.home()
    # slurry_volume_list = [0.3,0.35,0.4,0.45]
    # solubility.test_dilution_at_diff_slurry_volume(premade_stock_index='A1',slurry_volume_list=slurry_volume_list,
    #                                                sample_volume=0.06, dilution_factor=10,use_hplc=False)
    # exit()
    # solubility.vial_handling.home()
    # solubility.assemble_cup_and_filter(cup_index='A1', filter_index='A1')
    # solubility.measure_solubilities_hplc_vials(temp=35, time_temp=time_temp_min, duplicates=3, rest_time_min=6)
    solubility.measure_solubilities_no_hplc(temp=temp, time_temp=time_temp_min, duplicates=1)
    # solubility.measure_solubilities(temp=temp, time_temp=time_temp_min, duplicates=2)
    print('solubility trial finished')

    exit()

    # List of temperature values you want to use
    temperatures = [15,25,35,45]
    predosed_vial_list = [['A1','A2'],['A3','A4'],['A5','A6']]

    # Iterate over the temperature values and call the function
    # for i in range(len(temperatures)):

# solubility.set_solubilty_config(slurry_volume=slurry_volume, sample_volume=sample_volume,
        #                                 dilution_factor=dilution_factor,
        #                                 solid_loading_percent=solid_loading_percent, predosed_list=predosed_vial_list[i],
        #                                 predosed_cups=False,
        #                                 solvent_bank=False)

        # Measures solubilty for selected solvents
        # solubility.measure_solubilities(temp=temperatures[i], time_temp=time_temp_min)

        # need output csv files or is that automatic?

    # Saves solubilty peak area and converts major peak to % loading
    # Also ranks as high, medium, low or undetected based on parameters defined in self_driving_config.py
    # slope, intercept = standard_curve.get_slope_and_intercept(peak_label = peak_label)
    # print(slope)
    # print(intercept)
    # hplc_file = data_handling.find_most_recent_hplc_data()
    # print(f'{hplc_file} is the file')
    # solubility.save_solubilities_and_convert_to_loading(hplc_file=hplc_file,temp=temp,major_peak_label=peak_label,
    #                                                     slope=slope, intercept=intercept, dilution_factor=dilution_factor)

    # Makes decisions for 2nd round of solubility measurements


    # For cooling crystallization/washing, have a list of similar solvents or alternatives
    # If one seems to work best, try others from same class (no cosmo) or from similar cosmo



