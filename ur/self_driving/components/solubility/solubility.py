from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.quantos_handling.quantos_handling import QuantosHandling
from ur.components.capper.cap_handling import CapHandling
from ur.components.ur3_centrifuge.centrifuge import Centrifuge
from ur.components.samplomatic.samplomatic import Samplomatic
from ika import Thermoshaker
from ur.components.data_handling.data_handling import DataHandling
import time
from ur.configuration import deck_consumables
import pandas as pd
import json
from datetime import datetime
import os
import matplotlib.pyplot as plt
from scipy import stats






class Solubility:
    def __init__(self, solid_name, hplc_runtime: float, lower_range: int, upper_range: int, config_csv: str,output_folder: str,
                 cosmo_csv: str, solvent_bank_csv: str, vial_handling: VialHandling, hplc_handling: HplcHandling,
                 hplc_comm : HPLC_Comm, samplomatic : Samplomatic, somatic : SamplomaticHandling, ts : Thermoshaker,
                 data_handling: DataHandling, quantos_handling: QuantosHandling, cap_handling: CapHandling, centrifuge: Centrifuge):
        self.solid_name = solid_name
        self.hplc_runtime = hplc_runtime
        self.lower_range = lower_range
        self.upper_range = upper_range
        self.config_csv = config_csv
        self.output_folder = output_folder
        self.cosmo_csv = cosmo_csv
        self.solvent_bank_csv = solvent_bank_csv
        self.vial_handling = vial_handling
        self.hplc_comm = hplc_comm
        self.hplc_handling = hplc_handling
        self.samplomatic = samplomatic
        self.somatic = somatic
        self.ts = ts
        self.data_handling = data_handling
        self.quantos_handling = quantos_handling
        self.cap_handling = cap_handling
        self.centrifuge = centrifuge
        self.shake_speed = 600

    def assemble_cup_and_filter(self, cup_index, filter_index, fill_line: bool = True):
        self.vial_handling.cup_from_tray(cup_index=cup_index)
        self.vial_handling.cup_to_aligner()
        self.vial_handling.filter_from_tray(filter_index=filter_index, large_filter=True)
        self.vial_handling.align_filter()
        self.vial_handling.insert_filter(fill_line=False)
        self.vial_handling.push_filter(fill_line=fill_line)
        self.vial_handling.cup_from_aligner()

    def measure_solubilities(self, temp, time_temp, duplicates):
        config = pd.read_csv(self.config_csv, index_col=False)
        number_of_samples = config.shape[0]
        # self.hplc_comm.send_tcp_message('solubility_' + str(number_of_samples*duplicates))
        shaker_index = ['A1', 'B1', 'C1', 'D1', 'A3', 'B3', 'C3', 'D3', 'C5', 'D5']
        #
        # for i in range(number_of_samples):
        #     df = config.iloc[i]
        #     cup_index = df['cup_index']
        #     solvent_A = df["solvent_A"]
        #     solvent_B = df["solvent_B"]
        #     solvent_ratio = float(df["solvent_ratio"])
        #     solid_mass = float(df['mass_mg'])
        #     slurry_volume = float(df["slurry_volume"])
        #     filter_index = deck_consumables.get_clean_filter(large_filter=True)
        #
        #     stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
        #     stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]
        #
        #     if cup_index == 'None':
        #         cup_index = deck_consumables.get_clean_cup()
        #         df['cup_index'] = cup_index
        #
        #         self.vial_handling.cup_from_tray(cup_index=cup_index)
        #         self.vial_handling.home()
        #
        #         self.quantos_handling.vial_to_holder(vial_type='glass_cup')
        #         solid_mass = self.quantos_handling.dose_with_quantos(solid_weight=solid_mass, vial_type='glass_cup')
        #         self.quantos_handling.vial_from_holder(vial_type='glass_cup')
        #
        #         df["mass_mg"] = solid_mass
        #
        #         self.vial_handling.home()
        #         self.vial_handling.cup_to_tray(cup_index=cup_index)
        #
        #     config.iloc[i] = df
        #
        #     slurry_volume_A = slurry_volume * solvent_ratio
        #     slurry_volume_B = slurry_volume * (1 - solvent_ratio)
        #
        #     if i == 0:
        #         self.samplomatic.prime(2.5)
        #         self.somatic.grab_samplomatic()
        #         self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
        #     else:
        #         self.samplomatic.prime(1)
        #         self.somatic.grab_samplomatic()
        #         self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
        #
        #     self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)
        #     if stock_index_B != None:
        #         self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)
        #
        #     self.somatic.dispense_solvent_in_cup(volume_ml=slurry_volume, sample_index=cup_index)
        #     self.somatic.return_samplomatic()
        #
        #     ################################
        #     self.assemble_cup_and_filter(cup_index=cup_index, filter_index=filter_index, fill_line=True)
        #     self.vial_handling.cup_to_shaker(shaker_index=shaker_index[i], vial_type='push')
        #     #################################
        #
        # config.to_csv(self.config_csv, index=False)
        config = pd.read_csv(self.config_csv)
        #
        # self.samplomatic.prime(1)
        # self.ts.set_speed = self.shake_speed
        # self.ts.start_shaking()
        # self.ts.set_temperature = temp
        # time.sleep(1)
        # self.ts.start_tempering()
        # time.sleep(time_temp * 60)
        # self.ts.stop_shaking()
        # self.hplc_handling.run_blank()

        for i in range(number_of_samples):
            df = config.iloc[i]
            cup_index = df["cup_index"]
            sample_volume = float(df["sample_volume"])
            dilution_factor = float(df["dilution_factor"])
            diluent_volume = sample_volume * dilution_factor

            self.vial_handling.push_filter_in_shaker(shaker_index=shaker_index[i])
            self.vial_handling.cup_from_shaker(shaker_index=shaker_index[i])
            self.vial_handling.cup_to_tray(cup_index=cup_index)

            for d in range(duplicates):
                if d == 0:
                    self.samplomatic.prime(volume_ml=2)
                else:
                    self.samplomatic.prime(0.2)

                self.somatic.grab_samplomatic()
                if diluent_volume <= 0.3:
                    diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                else:
                    diluent_vial_index = deck_consumables.get_clean_vial()[0]
                print(f'diluent_vial is {diluent_vial_index}')
                self.somatic.draw_sample_from_cup(volume_ml=sample_volume, sample_index=cup_index)
                self.somatic.dispense_hplc_grid(volume_ml=diluent_volume, vial_index=diluent_vial_index)
                print(f'diluent solvent for duplicate {i}_{d} dispensed')
                self.somatic.return_samplomatic()

                self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=diluent_vial_index,
                                                                 hplc_runtime=self.hplc_runtime)

        self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)
        self.hplc_handling.return_last_vial()
        self.hplc_handling.process_data_and_send_csv()
        time.sleep(10)

    def measure_solubilities_no_hplc(self, temp, time_temp, duplicates):
        config = pd.read_csv(self.config_csv, index_col=False)
        number_of_samples = config.shape[0]
        shaker_index = ['A1', 'B1', 'C1', 'D1', 'A3', 'B3', 'C3', 'D3', 'C5', 'D5']

        for i in range(number_of_samples):
            df = config.iloc[i]
            cup_index = df['cup_index']
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            solid_mass = float(df['mass_mg'])
            slurry_volume = float(df["slurry_volume"])
            filter_index = deck_consumables.get_clean_filter(large_filter=True)

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            if cup_index == 'None':
                cup_index = deck_consumables.get_clean_cup()
                df['cup_index'] = cup_index

                self.vial_handling.cup_from_tray(cup_index=cup_index)
                self.vial_handling.home()

                self.quantos_handling.vial_to_holder(vial_type='glass_cup')
                solid_mass = self.quantos_handling.dose_with_quantos(solid_weight=solid_mass, vial_type='glass_cup')
                self.quantos_handling.vial_from_holder(vial_type='glass_cup')

                df["mass_mg"] = solid_mass

                self.vial_handling.home()
                self.vial_handling.cup_to_tray(cup_index=cup_index)

            config.iloc[i] = df

            slurry_volume_A = slurry_volume * solvent_ratio
            slurry_volume_B = slurry_volume * (1 - solvent_ratio)

            if i == 0:
                self.samplomatic.prime(2.5)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
            else:
                self.samplomatic.prime(1)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)

            self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)
            if stock_index_B != None:
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)

            self.somatic.dispense_solvent_in_cup(volume_ml=slurry_volume, sample_index=cup_index)
            self.somatic.return_samplomatic()

            ################################
            self.assemble_cup_and_filter(cup_index=cup_index, filter_index=filter_index, fill_line=True)
            self.vial_handling.cup_to_shaker(shaker_index=shaker_index[i], vial_type='push')
            #################################

        config.to_csv(self.config_csv, index=False)
        config = pd.read_csv(self.config_csv)

        self.samplomatic.prime(1)
        self.ts.set_speed = self.shake_speed
        self.ts.start_shaking()
        self.ts.set_temperature = temp
        time.sleep(1)
        self.ts.start_tempering()
        time.sleep(time_temp * 60)
        self.ts.stop_shaking()

        for i in range(number_of_samples):
            df = config.iloc[i]
            cup_index = df["cup_index"]
            sample_volume = float(df["sample_volume"])
            dilution_factor = float(df["dilution_factor"])
            diluent_volume = sample_volume * dilution_factor

            self.vial_handling.push_filter_in_shaker(shaker_index=shaker_index[i])
            self.vial_handling.cup_from_shaker(shaker_index=shaker_index[i])
            self.vial_handling.cup_to_tray(cup_index=cup_index)

            for d in range(duplicates):
                if d == 0:
                    self.samplomatic.prime(volume_ml=2)
                else:
                    self.samplomatic.prime(0.2)

                self.somatic.grab_samplomatic()
                if diluent_volume <= 0.3:
                    diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                else:
                    diluent_vial_index = deck_consumables.get_clean_vial()[0]
                print(f'diluent_vial is {diluent_vial_index}')
                self.somatic.draw_sample_from_cup(volume_ml=sample_volume, sample_index=cup_index)
                self.somatic.dispense_hplc_grid(volume_ml=diluent_volume, vial_index=diluent_vial_index)
                print(f'diluent solvent for duplicate {i}_{d} dispensed')
                self.somatic.return_samplomatic()

    def measure_solubilities_hplc_vials(self, temp, time_temp, duplicates, rest_time_min):
        config = pd.read_csv(self.config_csv, index_col=False)
        number_of_samples = config.shape[0]
        self.hplc_comm.send_tcp_message('solubility_' + str(number_of_samples * duplicates))
        print('message sent to HPLC')
        shaker_index = ['A1', 'B1', 'C1', 'D1', 'A3', 'B3', 'C3', 'D3', 'C5', 'D5']

        for i in range(number_of_samples):
            df = config.iloc[i]
            vial_index = df['cup_index']
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            solid_mass = float(df['mass_mg'])
            slurry_volume = float(df["slurry_volume"])
            filter_index = deck_consumables.get_clean_filter(large_filter=True)

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            if vial_index == 'None':
                vial_index = deck_consumables.get_clean_vial()[0]
                df['cup_index'] = vial_index

                # Replace with vial weighing / decapping

                # system gathers empty vial mass and returns it to 'midpoint'
                self.vial_handling.vial_from_tray(vial_index=vial_index, grab_cap=True)
                self.vial_handling.home()
                self.quantos_handling.vial_to_holder(vial_type='hplc')
                vial_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
                self.quantos_handling.vial_from_holder(vial_type='hplc')
                df['vial_mass'] = vial_mass
                print(f'mass of empty vial {i} recorded and vial returned to midpoint')

                # system de-caps vial, doses the vial, and re-caps the vial
                self.cap_handling.uncap()
                self.cap_handling.vial_from_capper()
                self.cap_handling.vial_to_holder()
                dosed_mass = self.quantos_handling.dose_with_quantos(solid_weight=float(solid_mass),
                                                                     vial_type='hplc')
                print(f'vial {i} dosed')
                df['mass_mg'] = dosed_mass
                self.centrifuge.home()
                self.cap_handling.vial_from_holder()
                self.cap_handling.vial_to_capper()
                self.cap_handling.cap()
                self.cap_handling.home()
                self.vial_handling.home()
                print(f'vial {i} returned to home')


            config.iloc[i] = df

            slurry_volume_A = slurry_volume * solvent_ratio
            slurry_volume_B = slurry_volume * (1 - solvent_ratio)

            if i == 0:
                self.samplomatic.prime(2.5)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
            else:
                self.samplomatic.prime(1)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)

            self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)
            print(f'stock solvent_A for i = {i} acquired')

            if stock_index_B != None:
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)
                print(f'stock solvent_B for i = {i} acquired')

            self.somatic.dispense_hplc_grid(volume_ml=slurry_volume, vial_index=vial_index)
            self.somatic.return_samplomatic()
            print(f'stock solvent dispensed into sample {i}')

            self.vial_handling.vial_from_tray(vial_index=vial_index)
            self.vial_handling.cup_to_shaker(shaker_index=shaker_index[i], vial_type='hplc')
            print(f'sample {i} moved from tray to shaker')

        print('all samples moved to shaker')
        config.to_csv(self.config_csv, index=False)
        config = pd.read_csv(self.config_csv)

        self.samplomatic.prime(1)
        self.ts.set_speed = self.shake_speed
        self.ts.start_shaking()
        print('shaker, start shaking!')
        self.ts.set_temperature = temp
        time.sleep(1)
        self.ts.start_tempering()
        time.sleep(time_temp * 60)
        self.ts.stop_shaking()
        print('shaker, stop shaking!')
        self.vial_handling.push_shaker()
        print('shaker reset to base position')
        time.sleep(rest_time_min*60)
        self.hplc_handling.run_blank()
        print('message sent to hplc to run blank')

        for i in range(number_of_samples):
            df = config.iloc[i]
            vial_index = df["cup_index"]
            sample_volume = float(df["sample_volume"])
            dilution_factor = float(df["dilution_factor"])
            diluent_volume = sample_volume * dilution_factor

            for d in range(duplicates):
                if d == 0:
                    self.samplomatic.prime(volume_ml=2)
                else:
                    self.samplomatic.prime(0.2)

                self.somatic.grab_samplomatic()
                if diluent_volume <= 0.3:
                    diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                else:
                    diluent_vial_index = deck_consumables.get_clean_vial()[0]
                print(f'diluent_vial is {diluent_vial_index}')
                self.somatic.draw_sample_from_shaker(volume_ml=sample_volume, shaker_index=shaker_index[i], type='hplc')
                self.somatic.dispense_hplc_grid(volume_ml=diluent_volume, vial_index=diluent_vial_index)
                print(f'diluent solvent for duplicate {i}_{d} dispensed')
                self.somatic.return_samplomatic()

                self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=diluent_vial_index,
                                                                 hplc_runtime=self.hplc_runtime)
                time.sleep(60)
            self.vial_handling.vial_from_shaker(shaker_index=shaker_index[i])
            self.vial_handling.vial_to_tray(vial_index=vial_index)
            print(f'sample {i} moved from shaker to tray')

        print(f'all vials in i = {i} moved to tray successfully')
        self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)
        self.hplc_handling.return_last_vial()
        print('last vial returned')
        self.hplc_handling.process_data_and_send_csv()
        time.sleep(10)

    def save_solubilities_and_convert_to_loading(self, hplc_file, temp, major_peak_label, slope, intercept, dilution_factor):
        #Only takes numerical data from csv
        hplc_df = pd.read_csv(hplc_file, index_col=False)
        hplc_df = hplc_df.iloc[:, 2:len(hplc_df.columns)]
        print(hplc_df)

        # Create the initial dataframe
        output_df = pd.read_csv(self.config_csv, index_col=False)
        print(output_df)
        output_df = output_df[['solvent_A','solvent_B','solvent_ratio']]
        print(output_df)

        # Add the temperature
        output_df['temperature'] = temp

        # Add the aquisition data
        output_df['date'] = datetime.now().strftime('%m-%d-%Y')

        # Merge into single dataframe
        df = pd.concat([output_df,hplc_df],axis="columns")
        print(df)

        # Make a new column with the solid loading
        df['solid_loading_mg_per_ml'] = None
        df['solid_loading_mg_per_ml'] = (df[major_peak_label]-intercept)/slope * dilution_factor

        # Categorize as low, medium or high solubilty solvents
        df['solvent_rank'] = None
        for index, value in enumerate(df['solid_loading_mg_per_ml']):
            if value == 0:
                df.loc[index,'solvent_rank'] = 'below_detection_limit'
                solvent_A = df.at[index, 'solvent_A']
                solvent_B = df.at[index, 'solvent_B']
                solvent_ratio = df.at[index, 'solvent_ratio']
                print(f'below detection limit for {solvent_A}_{solvent_B}_{solvent_ratio}')
            elif value < self.lower_range:
                df.loc[index,'solvent_rank'] = 'low'
            elif (value >= self.lower_range) and (value <= self.upper_range):
                df.loc[index,'solvent_rank'] = 'mid'
            elif value > self.upper_range:
                df.loc[index,'solvent_rank'] = 'high'
            else:
                print(f'{value} is not a number')

        # Save data in csv
        output_csv = self.output_folder + '/' + self.solid_name + '.csv'
        if os.path.exists(output_csv):
            # CSV file already exists
            existing_df = pd.read_csv(output_csv)  # Read the existing CSV as a DataFrame
            updated_df = existing_df.append(df, ignore_index=True)  # Append new data to the existing DataFrame
            updated_df.to_csv(output_csv, index=False)  # Save the updated DataFrame to the CSV file
        else:
            # CSV file does not exist, save as a new CSV
            df.to_csv(output_csv, index=False)

    def set_solubilty_config(self, slurry_volume, sample_volume, dilution_factor, solid_loading_percent,
                             predosed_cups: bool = False, predosed_list: list = [], solvent_bank: bool = False):

        if solvent_bank:
            df = pd.read_csv(self.solvent_bank_csv, index_col=False)
        else:
            df = pd.read_csv(self.cosmo_csv, index_col=False)

        if not predosed_cups:
            df['cup_index'] = 'None'

        #Allows user to set a predefined list instead of using the default sequential list
        elif bool(predosed_list):
            num_rows = df.shape[0]
            truncated_cup_list = predosed_list[:num_rows]
            df['cup_index'] = truncated_cup_list
        else:
            cup_list = ['A1','A2','A3','A4','B1','B2','B3','B4','C1','C2','C3','C4']
            num_rows = df.shape[0]
            truncated_cup_list = cup_list[:num_rows]
            df['cup_index'] = truncated_cup_list

        df['slurry_volume'] = slurry_volume
        df['sample_volume'] = sample_volume
        df['dilution_factor'] = dilution_factor
        df['mass_mg'] = slurry_volume * 1000 * solid_loading_percent / 100

        df.to_csv(self.config_csv)

    def find_pure_solvents_by_type(self, solvent_rank):
        solubility_csv = self.output_folder + '/' + self.solid_name + '.csv'

        df = pd.read_csv(solubility_csv)
        filtered_df = df[df['solvent_rank'] == solvent_rank]

        solvent_list = filtered_df['solvent_A'].to_list()
        print(solvent_list)

        return solvent_list

    # Checks if you get the same peak area depending on the initial slurry volume
    # Requires filling a stock solution with a premade, fully dissolved stock solution
    def test_dilution_at_diff_slurry_volume(self, premade_stock_index, slurry_volume_list, sample_volume, dilution_factor, use_hplc):

        if use_hplc:
            self.hplc_comm.send_tcp_message(f'solubility_{len(slurry_volume_list)}')

        for i in range(len(slurry_volume_list)):

            cup_index = deck_consumables.get_clean_cup()
            filter_index = deck_consumables.get_clean_filter()

            if i == 0:
                self.samplomatic.prime(2)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.3, stock_index=premade_stock_index)
                if use_hplc:
                    self.hplc_handling.run_blank()
            else:
                self.samplomatic.prime(0.2)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.3, stock_index=premade_stock_index)

            self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_list[i], stock_index=premade_stock_index)

            self.somatic.dispense_solvent_in_cup(volume_ml=slurry_volume_list[i], sample_index=cup_index)
            self.somatic.return_samplomatic()

            self.assemble_cup_and_filter(cup_index, filter_index)

            self.vial_handling.cup_to_shaker(shaker_index='B1', vial_type='glass')
            self.vial_handling.push_filter_in_shaker(shaker_index='B1')
            self.vial_handling.push_shaker()
            self.vial_handling.cup_from_shaker('B1')
            self.vial_handling.cup_to_tray(cup_index)

            self.samplomatic.prime(0.6)

            self.somatic.grab_samplomatic()
            if (dilution_factor*sample_volume) <= 0.3:
                diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
            else:
                diluent_vial_index = deck_consumables.get_clean_vial()[0]
            print(f'diluent_vial is {diluent_vial_index}')
            self.somatic.draw_sample_from_cup(volume_ml=sample_volume,sample_index=cup_index,velocity_ml=12)
            self.somatic.dispense_hplc_grid(volume_ml=sample_volume*dilution_factor, vial_index=diluent_vial_index)
            self.somatic.return_samplomatic()

            if use_hplc:
                self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=diluent_vial_index,
                                                                hplc_runtime=self.hplc_runtime)
        if use_hplc:
            self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)
            self.hplc_handling.return_last_vial()
            self.hplc_handling.process_data_and_send_csv()
            time.sleep(10)


















