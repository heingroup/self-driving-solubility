import os

from ur.self_driving.configuration.self_driving_config import cryst
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm
import datetime

if __name__ == '__main__':

    solid = 'pyrethroic acid'

    cryst.vial_handling.home()

    exit()
    # for i in range(3):
    #     cryst.vial_handling.vial_from_tray('E2')
    #     cryst.vial_handling.vial_to_tray('E2')
    # exit()
    #cryst.set_sample_config('pyrethroic acid',10,10,3,'heptane','None',1,'product_240')
     #ADD MASS CHECKS
    #cryst.run_cooling_crystallization(clear_shaker=True)
    #ts.stop_tempering()
    # cryst.vial_handling.vial_from_handoff()
    # cryst.vial_handling.open_gripper()

    hplc_file = data_handling.find_most_recent_hplc_data()
    hplc_dir = os.path.dirname(hplc_file)
    hplc_filename = str(os.path.basename(hplc_file))
    summary_file = os.path.join(hplc_dir, 'cooling_crystal_summary', f'{hplc_filename}')

    cryst.average_hplc_duplicates(hplc_file=hplc_file, output_file=summary_file, remove_outliers=True)

    # cryst.evaporation_peak_area_correction(csv_file = summary_file, json_file=(os.getcwd() + '/data/log.json'), overwrite=False)

    output_directory = os.getcwd() + '/data' + f'/{solid}'
    output_file = cryst.process_data(input_csv=summary_file, output_directory=output_directory, hplc_file=hplc_file,
                                     product_name='product_240', impurity_name='isomer_210')
    results_dict = cryst.process_experimental_results(csv_file_path=output_file,
                                                      yield_enabled=True, rejection_enabled=True, purity_enabled=True)
    print(results_dict)
    exit()

    hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/2023-11-07/2023-11-07-pyrethroic acid-2_temp_15_injection.csv'

    # hplc_file = data_handling.find_most_recent_hplc_data()
    print(hplc_file)
    hplc_dir = os.path.dirname(hplc_file)
    hplc_filename = str(os.path.basename(hplc_file))
    print(hplc_dir)
    # hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/2023-10-22/2023-10-22-pyrethroic acid-2_temp_6_injections.csv'
    # summary_file = f'{hplc_dir}/summary/cooling_crystal_{hplc_filename}'
    current_date = datetime.datetime.today().strftime('%Y-%m-%d')
    summary_file = os.path.join(hplc_dir,'cooling_crystal_summary',f'{hplc_filename}')
    # summary_file = f'results_{hplc_filename}.csv'
    print(summary_file)
    # summary_file = '2023-10-19_summary.csv'
    cryst.average_hplc_duplicates(hplc_file=hplc_file,output_file=summary_file,remove_outliers=True)
    # cryst.evaporation_peak_area_correction(csv_file = summary_file, json_file=(os.getcwd() + '/data/log.json'), overwrite=True)
    output_directory = os.getcwd() + '/data'
    output_file = cryst.process_data(input_csv=summary_file, hplc_file=hplc_file, output_directory=output_directory, product_name='product_240', impurity_name='isomer_210')
    # Parse the HPLC data based on number of samples / duplicates
    # Should also factor in dilution factor
    # Link it with experiment parameters (high temp, low temp, cooling rate, solvent, solid loading)
    # Calculate % yield of product of interest and % purity (peak area pdt / sum of peak area)
    ## pandas to json or pandas to csv
    results_dict = cryst.process_experimental_results(csv_file_path=output_file,
                                                      yield_enabled=True, rejection_enabled=True, purity_enabled=True)
    print(results_dict)
    print('done')
