from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.quantos_handling.quantos_handling import QuantosHandling
from ur.components.shaker_handling.shaker_handling import ShakerHandling
from ur.components.capper.cap_handling import CapHandling

from ur.components.samplomatic.samplomatic import Samplomatic
from ika import Thermoshaker
from ur.components.data_handling.data_handling import DataHandling
import time
from ur.configuration import deck_consumables
import pandas as pd
import json
from datetime import datetime
import os
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
from csv import DictWriter


class Cooling_Crystal:
    def __init__(self, solid_name, hplc_runtime: float, sample_config_csv: str, system_config_csv: str,
                 vial_handling: VialHandling, hplc_handling: HplcHandling, hplc_comm: HPLC_Comm,
                 samplomatic: Samplomatic, somatic: SamplomaticHandling, ts: Thermoshaker,
                 data_handling: DataHandling, quantos_handling: QuantosHandling, shaker_handling: ShakerHandling,
                 cap_handling: CapHandling):
        self.solid_name = solid_name
        self.hplc_runtime = hplc_runtime
        self.sample_config_csv = sample_config_csv
        self.system_config_csv = system_config_csv
        self.vial_handling = vial_handling
        self.hplc_comm = hplc_comm
        self.hplc_handling = hplc_handling
        self.samplomatic = samplomatic
        self.somatic = somatic
        self.ts = ts
        self.data_handling = data_handling
        self.quantos_handling = quantos_handling
        self.shaker_handling = shaker_handling
        self.cap_handling = cap_handling
        self.shake_speed = 600
        self.list_of_files = []

    def weigh_sample(self, shaker_index):
        self.vial_handling.vial_from_shaker(shaker_index=shaker_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.cup_to_shaker(shaker_index=shaker_index, grab_cap=True)
        return vial_solvent_mass
        print('sample weighed successfully and vial returned to shaker')

    def weigh_diluent_vial(self, diluent_vial_index):
        self.vial_handling.vial_from_tray(vial_index=diluent_vial_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.vial_to_tray(vial_index=diluent_vial_index)
        return vial_solvent_mass
        print('diluent vial measured successfully and returned to tray')

    def run_cooling_crystallization(self, clear_shaker: bool = True):
        # reads system and sample configuration files to determine experimental parameters, set thermoshaker params
        parameter_config = pd.read_csv(self.system_config_csv, index_col=False)
        df_parameter = parameter_config.iloc[0]
        config = pd.read_csv(self.sample_config_csv, index_col=False)

        self.ts.set_speed = 600
        between_sample_settling_time = 2

        self.data_handling.set_workflow('cooling_cryst')

        # creation of empty lists "[]" and dictionaries "{}" for data storage later on
        temps = []
        time_temps = []
        pre_sample_masses = {}
        post_sample_masses = {}
        diluent_solvent_masses = {}
        hplc_vials = {}
        run_numbers = {}
        shaker_index = ['A1', 'B1', 'C1', 'A2', 'B2', 'C2', 'D2', 'A3', 'B3', 'C3', 'D3']

        # determination of number of temps from system config
        # populates list based on keys constructed from the loop variable
        # end variables look like 'temp_1', 'temp_2', 'time_temp_1', etc
        number_of_temps = df_parameter["number_of_temps"]
        for n in range(number_of_temps):
            current_temp = 'temp_' + str(n + 1)
            current_time_temp = 'time_temp_' + str(n + 1)
            temps.append(int(df_parameter[current_temp]))
            time_temps.append(int(df_parameter[current_time_temp]))


        # Cooling rate (total time spent cooling)
        cooling_rate_deg_per_min = float(df_parameter["cooling_rate_deg_per_min"])
        # Time between end of shaking and drawing sample
        rest_time = int(df_parameter["rest_time_min"])
        # Number of duplicate HPLC samples (DO NOT have weigh_diluent_solvent set to True)
        duplicates = int(df_parameter[
                             "duplicates"])
        # Weigh after shaking to check for solvent loss
        weigh_solvent = bool(df_parameter['weigh_solvent'])
        # Weighs HPLC vials after dilution
        weigh_diluent_solvent = bool(df_parameter["weigh_diluent_solvent"])
        check_dissolution = True

        # number of samples and injections are determined
        number_of_samples = config.shape[0]

        # HPLC communication to set up run queue
        number_of_injections = number_of_samples * duplicates
        self.hplc_comm.send_tcp_message(f'{number_of_temps}_temp_{number_of_injections}_injections')

        print(f'gathered values for system: cooling rate: {cooling_rate_deg_per_min}, rest time: {rest_time}, '
              f'duplicates: {duplicates}, weigh solvent: {weigh_solvent}, weigh diluent solvent: {weigh_diluent_solvent}'
              f'number of samples: {number_of_samples}, number of injections: {number_of_injections}')
        print(f'sent HPLC command string')

        # loop created to do cooling crystallization for each of the number of samples determined prior
        for i in range(number_of_samples):
            df = config.iloc[i]
            vial_index = df["vial_index"]
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            solid_mass = df['mass_mg']
            slurry_volume = df["slurry_volume"]
            waste_volume = df["waste_volume"]

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            # HPLC vial prep
            if vial_index == 'None':
                vial_index = deck_consumables.get_clean_vial()[0]
                df['vial_index'] = vial_index

                # system gathers empty vial mass and returns it to 'midpoint'
                self.vial_handling.vial_from_tray(vial_index=vial_index, grab_cap=True)
                self.vial_handling.home()
                self.quantos_handling.vial_to_holder(vial_type='hplc')
                vial_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
                self.quantos_handling.vial_from_holder(vial_type='hplc')
                df['vial_mass'] = vial_mass
                print(f'mass of empty vial {i} recorded and vial returned to midpoint')

                # system de-caps vial, doses the vial, and re-caps the vial
                self.cap_handling.uncap()
                self.cap_handling.vial_from_capper()
                self.cap_handling.vial_to_holder()
                dosed_mass = self.quantos_handling.dose_with_quantos(solid_weight=float(solid_mass),
                                                                     vial_type='hplc')
                print(f'vial {i} dosed')
                df['mass_mg'] = dosed_mass

                self.cap_handling.vial_from_holder()
                self.cap_handling.vial_to_capper()
                self.cap_handling.cap()
                self.vial_handling.home()
                self.vial_handling.vial_to_tray(grab_cap=True, vial_index=vial_index)
                self.vial_handling.home()
                print(f'vial {i} returned to home')

            # calculation of slurry volume, solvent_ratio from sample config
            slurry_volume_A = slurry_volume * solvent_ratio
            slurry_volume_B = slurry_volume * (1 - solvent_ratio)

            # Somatic primes lines with THF, then sucks up the waste solvent
            print(f'getting waste solvent, {i}=0')
            if i == 0:
                self.samplomatic.prime(2)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=waste_volume, stock_index=stock_index_A)
            else:
                self.samplomatic.prime(1)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=waste_volume, stock_index=stock_index_A)

            self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)
            print(f'solvent_A dispensed into vial {i}')

            # case for when there is a second solvent, 'solvent B'
            if stock_index_B != None:
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)
            self.somatic.dispense_hplc_grid(volume_ml=slurry_volume, vial_index=vial_index, backend=False)
            self.somatic.return_samplomatic()
            print(f'solvent_B dispensed into vial {i}')

            self.vial_handling.vial_from_tray(vial_index=vial_index)
            self.vial_handling.cup_to_shaker(shaker_index=shaker_index[i])
            print(f'vial {i} successfully moved from tray to shaker')
            prev_solvent_A = solvent_A

            # appends data to new row in config dataframe
            config.iloc[i] = df
            print('data appended to config dataframe')

        # Saves data from quantos weights into the config csv for getting again later
        config.to_csv(self.sample_config_csv, index=False)
        config = pd.read_csv(self.sample_config_csv)

        self.samplomatic.prime(1)

        # weighs quantos holder
        if weigh_solvent:
            self.vial_handling.home()
            self.quantos_handling.switch_to_horizontal()
            mass_quantos_holder = (self.quantos_handling.weigh_with_quantos()) * 1000
            self.quantos_handling.switch_to_vertical()
            self.vial_handling.home()
            print('quantos holder weighed successfully')

        # loop with variable 't' = number_of_temps, sets up empty dictionaries for vial/sample masses
        for t in range(number_of_temps):
            temp = temps[t]
            time_temp = time_temps[t]

            pre_sample_masses = {}
            post_sample_masses = {}
            diluent_solvent_masses = {}
            diluent_vial_index_dict = {}

            # equilibrates at temp_1 for time_temp_1
            if t == 0:
                self.ts.start_shaking()
                self.ts.set_temperature = temp
                time.sleep(1)
                self.ts.start_tempering()
                time.sleep(time_temp * 60)
                self.ts.stop_shaking()
                print(f'equilibration for {t} complete')

                # If check_dissolution is 'True', system takes a picture of each vial to see if everything is dissolved
                if check_dissolution:
                    for s in range(number_of_samples):
                        df = config.iloc[s]
                        vial_index = df["vial_index"]
                        self.vial_handling.take_picture_of_vial(vial_index=vial_index, shaker_index=shaker_index[s],
                                                                temperature=temp)
                        print(f'image captured, {s}')

            # cools and equilibrates at new temp while doing hplc run from queue
            else:
                self.ts.start_shaking()
                temper_time = int((temps[t - 1] - temp) / cooling_rate_deg_per_min)
                self.shaker_handling.temper_over_time_with_hplc(start_temp=temps[t - 1], end_temp=temp,
                                                                time_min=temper_time,
                                                                hplc_runtime=self.hplc_runtime)
                self.shaker_handling.shake_with_hplc(shake_time=time_temp, hplc_runtime=self.hplc_runtime)
                self.ts.stop_shaking()
                print(f'equilibration for {t} complete')

            # turns the hplc pump on
            self.hplc_handling.run_blank()
            print('hplc pump turned on')

            # sets vial index, slurry volume, etc. to the integers found in config df
            for i in range(number_of_samples):
                df = config.iloc[i]
                vial_index = df["vial_index"]
                slurry_volume = float(df["slurry_volume"])
                sample_volume = float(df["sample_volume"])
                dilution_factor = float(df["dilution_factor"])
                mass_mg = float(df['mass_mg'])
                vial_mass = float(df["vial_mass"])
                solvent_A = df["solvent_A"]
                solvent_B = df["solvent_B"]
                solvent_ratio = float(df["solvent_ratio"])

                diluent_volume = sample_volume * dilution_factor

                # weighs solvent if set to True, weighs before all dilutions occur
                if weigh_solvent:
                    vial_solvent_mass = self.weigh_sample(shaker_index=shaker_index[i])
                    pre_sample_solvent_mass = vial_solvent_mass - mass_mg - vial_mass - mass_quantos_holder
                    print(f'{vial_index} pre sample solvent mass at {temp} is {pre_sample_solvent_mass}')

                # runs next hplc sample if the system is ready
                self.hplc_handling.run_next_vial_in_queue_if_ready(hplc_runtime=self.hplc_runtime)
                print('hplc sample ran if system ready')

                # resets shaker position so that needle can insert
                self.vial_handling.push_shaker()
                print('shaker reset to base position')
                time.sleep(rest_time * 60)

                # instructions for gathering sample; system priming, finds clean vial
                # w or w/o insert, depending on what the final diluent volume is.
                # draws up sample and dilutes into clean vial
                diluent_vial_index_list = []
                diluent_solvent_mass_list = []
                for d in range(duplicates):
                    if i == 0:
                        self.samplomatic.prime(volume_ml=2)
                    else:
                        self.samplomatic.prime(0.2)

                    self.somatic.grab_samplomatic()
                    if diluent_volume <= 0.3:
                        diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                    else:
                        diluent_vial_index = deck_consumables.get_clean_vial()[0]
                    diluent_vial_index_list.append(diluent_vial_index)
                    self.somatic.draw_sample_from_shaker(volume_ml=sample_volume, shaker_index=shaker_index[i],
                                                         type='hplc')
                    self.somatic.dispense_hplc_grid(volume_ml=diluent_volume, vial_index=diluent_vial_index)
                    self.somatic.return_samplomatic()
                    print(f'duplicate {d} prepared successfully')

                    time.sleep(between_sample_settling_time * 60)

                    # weighs diluent solvent if parameter is set to 'True'
                    if weigh_diluent_solvent:
                        diluent_vial_mass = deck_consumables.get_vial_mass(diluent_vial_index)
                        vial_solvent_mass = self.weigh_diluent_vial(diluent_vial_index=diluent_vial_index)
                        diluent_solvent_mass = vial_solvent_mass - diluent_vial_mass - mass_quantos_holder
                        diluent_solvent_mass_list.append(diluent_solvent_mass)
                        print('diluent solvent weighed successfully')

                    # again checks if system can start the next hplc run
                    self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=diluent_vial_index,
                                                                     hplc_runtime=self.hplc_runtime)
                    diluent_vials = ','.join(diluent_vial_index_list)
                    diluent_solvent_mass = ','.join(map(str, diluent_solvent_mass_list))

                # weighs sample vials after all dilutions are taken
                if weigh_solvent:
                    vial_solvent_mass = self.weigh_sample(shaker_index=shaker_index[i])
                    post_sample_solvent_mass = vial_solvent_mass - mass_mg - vial_mass - mass_quantos_holder
                    print(f'{vial_index} post sample solvent mass at {temp} is {post_sample_solvent_mass}')

                # saves data in a new list
                self.data_handling.new_sample()
                save_dict = locals()
                save_list = ['vial_index', 'diluent_vials', 'mass_mg', 'slurry_volume', 'solvent_A', 'solvent_B',
                             'solvent_ratio', 'solid_name', 'rest_time', 'sample_volume', 'dilution_factor', 'temp',
                             'time_temp', 'cooling_rate_deg_per_min']
                print('list saved')

                if weigh_solvent:
                    save_list = save_list + ['pre_sample_solvent_mass', 'post_sample_solvent_mass']
                if weigh_diluent_solvent:
                    save_list = save_list + ['diluent_solvent_mass']
                for s in save_list:
                    self.data_handling.save_value(data_name=s, data_value=str(save_dict.get(s)))

        # runs remainder of hplc vials
        self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)
        print('sent command to run remainder of hplc vials')

        self.vial_handling.above_hplc_tray_front()
        self.hplc_handling.return_last_vial()
        print('last vial returned')
        time.sleep(self.hplc_runtime * 60)
        self.hplc_handling.process_data_and_send_csv()
        print('data processed and csv sent')
        time.sleep(10)

        # if param is set to true, system removes vials from thermoshaker and moves back to the tray
        if clear_shaker:
            for x in range(number_of_samples):
                df = config.iloc[x]
                vial_index = df["vial_index"]
                self.vial_handling.vial_from_shaker(shaker_index=shaker_index[x])
                self.vial_handling.vial_to_tray(vial_index=vial_index)
                print('shaker cleared and all vials returned to tray')

        print('cooling crystallization complete')

    # function to clear shaker; removes sample from shaker and moves them to a tray location
    def clear_shaker(self):
        shaker_index = ['A1', 'B1', 'C1', 'D1', 'A2', 'B2', 'C2', 'D2', 'A3', 'B3', 'C3', 'D3']

        config_file_path = self.sample_config_csv
        config = pd.read_csv(config_file_path, index_col=False)

        number_of_samples = config.shape[0]

        for x in range(number_of_samples):
            df = config.iloc[x]
            vial_index = df["vial_index"]
            self.vial_handling.vial_from_shaker(shaker_index=shaker_index[x])
            self.vial_handling.vial_to_tray(vial_index=vial_index)
            print(f'vial {x} returned to tray from shaker')

    # function to average the HPLC duplicates
    def average_hplc_duplicates(self, hplc_file, output_file, remove_outliers: bool = False):
        df = pd.read_csv(hplc_file, index_col=False)

        parameter_config = pd.read_csv(self.system_config_csv, index_col=False)
        df_parameter = parameter_config.iloc[0]
        duplicates = int(df_parameter["duplicates"])  # Number of duplicate HPLC samples

        averaged_values = []

        # Process the data in chunks of size duplicates
        for i in range(0, len(df), duplicates):
            chunk = df.iloc[i:i + duplicates]

            # Exclude the first column (string values)
            chunk = chunk.iloc[:, 1:]

            if remove_outliers:
                outlier_columns = None  # Initialize the variable
                # Calculate the column-wise relative standard deviation as a percentage
                numeric_data = chunk.select_dtypes(include=[np.number])
                if not numeric_data.empty:
                    relative_stdev = numeric_data.std() / numeric_data.mean() * 100

                    # Identify columns with relative standard deviation above 10%
                    outlier_columns = relative_stdev[relative_stdev > 5].index

                # Iterate through each outlier column
                if outlier_columns is not None:
                    for col_name in outlier_columns:
                        z_scores = (chunk[col_name] - chunk[col_name].mean()) / chunk[col_name].std()
                        row_to_remove = z_scores.abs().idxmax()

                        # Check if row_to_remove is not NaN
                        if not pd.isna(row_to_remove):
                            chunk = chunk.drop(index=row_to_remove)

                # Calculate the average for each column within the chunk
            avg_values = chunk.mean(numeric_only=True)  # Use numeric_only=True to avoid warnings

            # Append the averaged values to the list
            averaged_values.append(avg_values)

        # Create a new DataFrame with the averaged values
        result_df = pd.concat(averaged_values, axis=1).T

        # Save the new DataFrame to a CSV file
        output_dir = os.path.dirname(output_file)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        result_df.to_csv(output_file, index=False)

    # function to correct peak area for evaporation shifts
    def evaporation_peak_area_correction(self, csv_file, json_file, overwrite: bool = True):
        # Load the JSON file
        with open(json_file, 'r') as json_file:
            json_data = json.load(json_file)

        # Load the CSV file
        df = pd.read_csv(csv_file)

        # Number of rows to consider from the JSON file
        X = len(df)

        # Extract the last X entries from the JSON data
        last_X_entries = json_data[-X:]

        # Extract 'pre_sample_solvent_mass' and 'post_sample_solvent_mass' values
        pre_sample_mass_values = [float(entry['pre_sample_solvent_mass']) for entry in last_X_entries]
        post_sample_mass_values = [float(entry['post_sample_solvent_mass']) for entry in last_X_entries]

        # Add 'pre_sample_solvent_mass' and 'post_sample_solvent_mass' columns to the DataFrame
        df['pre_sample_solvent_mass'] = pre_sample_mass_values
        df['post_sample_solvent_mass'] = post_sample_mass_values

        # Calculate the correction factor for 'high_temp' (initially set to 1)
        df['correction_factor'] = 1

        # Split the DataFrame into two halves and reset the index
        half_point = len(df) // 2
        high_temp = df.iloc[:half_point].reset_index(drop=True)
        low_temp = df.iloc[half_point:].reset_index(drop=True)

        # Calculate the correction factor for 'low_temp' based on your description
        low_temp['correction_factor'] = (high_temp['post_sample_solvent_mass'] / low_temp['pre_sample_solvent_mass'])

        # Combine the DataFrames
        df = pd.concat([high_temp, low_temp])

        # Apply the correction factors to all columns (except 'pre_sample_solvent_mass', 'post_sample_solvent_mass', and correction factors)
        cols_to_correct = [col for col in df.columns if
                           col not in ['pre_sample_solvent_mass', 'post_sample_solvent_mass', 'correction_factor']]
        df[cols_to_correct] = df[cols_to_correct].mul(df['correction_factor'], axis=0)

        # Extract the original CSV file name without the extension
        filename_without_extension = csv_file.split('.')[0]

        if not overwrite:
            # Save the corrected CSV with the same filename but 'corrected' appended
            output_csv = f'{filename_without_extension}_corrected.csv'
            df.to_csv(output_csv, index=False)

        else:
            output_csv = csv_file
            df.to_csv(csv_file, index=False)

        print(f'Correction applied and saved to {output_csv}')

    # Added by Rebekah 2023-10-13 vvvvv

    # for i in number_of_lines_isn_samp_config:
    #     df_sample = sample_config.iloc[i]
    #     calculaulahgehew
    #     temp_data = pd.concat([system_config, sample_config, calculated_data], axis=1)

    def set_system_config(self, low_temp, temp_diff, cooling_rate):
        df = pd.read_csv(self.system_config_csv)
        config = df.iloc[0]

        config['temp_2'] = int(low_temp)
        config['temp_1'] = int(low_temp + temp_diff)
        config['cooling_rate_deg_per_min'] = cooling_rate

        df.iloc[0] = config
        df.to_csv(self.system_config_csv, index=False)

    def set_sample_config(self, solid_name, low_temp, temp_diff, duplicates, solvent_A, solvent_B, solvent_ratio, product, user_fills_vials):
        system_df = pd.read_csv(self.system_config_csv)
        system_config = system_df.iloc[0]

        df = pd.read_csv(self.sample_config_csv)
        high_temp = low_temp + temp_diff

        # find A, B and C depending on solid/solvent
        curve_filepath = f'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/cooling_crystal/curve_equations/{solid_name}/{solvent_A.lower()}_{solvent_B.lower()}_{solvent_ratio}.csv'
        file = pd.read_csv(curve_filepath)
        parameter = file.iloc[0]
        A = parameter['A']
        B = parameter['B']
        C = parameter['C']
        peak_area_high_temp = C * np.exp(np.log(A) - B / (8.314 * (high_temp+273.15)))

        conversion_filepath = f'C:/Users/User/PycharmProjects/self_driving_solubility/ur/self_driving/components/cooling_crystal/conversion_factors/{solid_name}.csv'
        file = pd.read_csv(conversion_filepath)
        parameter = file.loc[file['label'] == product]
        slope = parameter['slope']
        intercept = parameter['intercept']

        solid_loading = (peak_area_high_temp - intercept) / slope

        solid_loading = solid_loading * 1.1

        config = df.iloc[0]
        slurry_volume = config['slurry_volume']
        mass_mg = slurry_volume * solid_loading

        if not user_fills_vials:
            df = df.drop(df.index[1:])
            config = df.iloc[0]
            config['vial_index'] = 'None'
            config['solid_name'] = solid_name
            config['mass_mg'] = mass_mg
            config['solvent_A'] = solvent_A
            config['solvent_B'] = solvent_B
            config['solvent_ratio'] = solvent_ratio
            df.iloc[0] = config

            df = pd.concat([df]*duplicates)
            df.to_csv(self.sample_config_csv, index=False)

        else:
            mass_mg = mass_mg.item()
            print("Put " + str(mass_mg) + " mg of solid, then manually change vial index and mass in sample_config.csv")





        # duplicate the row {duplicate} times




        # # clear everything
        #
        # df.iloc[0]['solvent_A'] = solvent_A
        #
        # # other parameters
        # # eventually needs to set solid loading based on solvent ratio curves
        #
        # pd.concat([df] * duplicates)
        #
        # df.to_csv()

    def evaluate(self, params, solvent_A, solvent_B):
        number_of_temps, temp_1, time_temp_1, temp_2, time_temp_2, cooling_rate_deg_per_min, rest_time_min, duplicates, weigh_solvent, weigh_diluent_solvent = params.get(
            "number_of_temps"), params.get("temp_1"), params.get("time_temp_1"), params.get("temp_2"), params.get(
            "time_temp_2"), params.get("cooling_rate_deg_per_min"), params.get("rest_time_min"), params.get(
            "duplicates"), params.get("weigh_solvent"), params.get("weigh_diluent_solvent")

        self.appendcsv(params)

        suggested_loading = 10  # this needs to be extracted from Rebekah's solubility modelling fxn !!
        volume = 1
        mass = suggested_loading * volume * 1.15

        df = pd.read_csv("sample_config.csv")
        df.loc[0, "mass_mg"] = mass
        df.loc[0, "vial_index"] = None
        df.loc[0, "solvent_A"] = solvent_A
        df.loc[0, "solvent_B"] = solvent_B
        df.to_csv("sample_config.csv", index=False)

    # Function to sort the input data into duplicates
    def data_sorting(self, input_csv, duplicates):
        try:
            df = pd.read_csv(input_csv)
        except FileNotFoundError:
            print(f"File not found: {input_csv}")
            return []

        # rows_per_duplicate = len(df) // duplicates
        # df_list = []

        # for i in range(duplicates):
        #     start_idx = i * rows_per_duplicate
        #     end_idx = start_idx + rows_per_duplicate
        #     df_duplicate = df[start_idx:end_idx]
        #     df_list.append(df_duplicate)

        num_rows = len(df)

        if num_rows % 2 != 0:
            raise ValueError("The number of rows must be even for pairing.")

        df_list = []
        for i in range(num_rows // 2):
            df_pair = df.iloc[[i, i + num_rows // 2]]
            df_pair = df_pair.reset_index(drop=True)
            df_list.append(df_pair)

        return df_list

    # Function to calculate various metrics
    def sample_calculations(self, input_data, product, maj_impurity, other_impurities: list = []):

        try:
            product_high = input_data.loc[0, product]
            product_low = input_data.loc[1, product]
            impurity_high = input_data.loc[0, maj_impurity]
            impurity_low = input_data.loc[1, maj_impurity]

            impurity_sum = impurity_high - impurity_low

            if other_impurities:
                for impurity in other_impurities:
                    impurity_sum += (input_data.loc[0, impurity] - input_data.loc[1, impurity])

            if product_high == 0:
                raise ZeroDivisionError("Product high value is zero.")

            percent_yield = ((product_high - product_low) / product_high) * 100
            percent_rejection = (impurity_low / impurity_high) * 100
            percent_purity = ((product_high - product_low) / (product_high - product_low + impurity_sum)) * 100

            return percent_yield, percent_rejection, percent_purity
        except KeyError as e:
            print(f"KeyError: {e}. Please ensure the column names are correct.")
            return None, None, None

    # Main function to process the data
    # def process_data(self, input_csv, output_directory, product_name, impurity_name, other_impurities: list = []):
    #     try:
    #         system_config = pd.read_csv(self.system_config_csv)
    #     except FileNotFoundError:
    #         print(f"File not found: {self.system_config_csv}")
    #         return
    #
    #     current_date = datetime.now().strftime('%Y-%m-%d')
    #
    #     try:
    #         sample_config = pd.read_csv(self.sample_config_csv)
    #     except FileNotFoundError:
    #         print(f"File not found: {self.sample_config_csv}")
    #         return
    #
    #     duplicates = len(sample_config)
    #     data_frames = self.data_sorting(input_csv, duplicates)
    #
    #     for i, df in enumerate(data_frames):
    #         product_type = product_name
    #         impurity_type = impurity_name
    #
    #         percent_yield, percent_rejection, percent_purity = self.sample_calculations(df, product_type, impurity_type,
    #                                                                                     other_impurities)
    #
    #         if percent_yield is not None:
    #             sample_info = sample_config.loc[
    #                 i, ['solid_name', 'solvent_A', 'solvent_B', 'sample_volume', 'solvent_ratio', 'slurry_volume',
    #                     'mass_mg', 'dilution_factor']]
    #             system_info = system_config.loc[
    #                 0, ['temp_1', 'time_temp_1', 'temp_2', 'time_temp_2', 'cooling_rate_deg_per_min', 'rest_time_min']]
    #
    #             output_csv_name = f"{current_date}_{sample_info['solid_name']}_{sample_info['solvent_A']}_{sample_info['solvent_B']}.csv"
    #             file_number = 1
    #             while os.path.exists(os.path.join(output_directory, output_csv_name)):
    #                 output_csv_name = f"{current_date}_{sample_info['solid_name']}_{sample_info['solvent_A']}_{sample_info['solvent_B']}_{file_number}.csv "
    #                 file_number += 1
    #
    #             output_data = {
    #                 'temp_1': [system_info['temp_1']],
    #                 'time_temp_1': [system_info['time_temp_1']],
    #                 'temp_2': [system_info['temp_2']],
    #                 'time_temp_2': [system_info['time_temp_2']],
    #                 'cooling_rate_deg_per_min': [system_info['cooling_rate_deg_per_min']],
    #                 'rest_time_min': [system_info['rest_time_min']],
    #                 'solid_name': [sample_info['solid_name']],
    #                 'solvent_A': [sample_info['solvent_A']],
    #                 'solvent_ratio': [sample_info['solvent_ratio']],
    #                 'solvent_B': [sample_info['solvent_B']],
    #                 'sample_volume': [sample_info['sample_volume']],
    #                 'slurry_volume': [sample_info['slurry_volume']],
    #                 'mass_mg': [sample_info['mass_mg']],
    #                 'dilution_factor': [sample_info['dilution_factor']],
    #                 'percent_yield': [percent_yield],
    #                 'percent_rejection': [percent_rejection],
    #                 'percent_purity': [percent_purity],
    #             }
    #
    #             output_df = pd.DataFrame(output_data)
    #             output_df.to_csv(os.path.join(output_directory, output_csv_name), index=False)

    # Example usage
    # input_csv = 'output.csv'
    # sample_config_csv = 'sample_config.csv'
    # system_config_csv = 'system_config.csv'
    # output_directory = 'cooling_crystal/data'
    # product_name = 'product_240'
    # impurity_name = 'isomer_210'
    # other_impurities = ['other_impurity_1', 'other_impurity_2']  # List of other impurities

    # process_data(system_config_csv, sample_config_csv, input_csv, output_directory, product_name, impurity_name,
    #              other_impurities)

    ## output file for HPLC data averaging is not set. Therefore we need to set that and then reference it for input_csv in
    ## the process data function.

    # def process_experimental_results(self, yield_enabled=True, rejection_enabled=True, purity_enabled=True):
    #     # Define the path to the directory where CSV files are stored
    #     data_directory = os.getcwd() + '/data'
    #
    #     # List all files in the directory and sort them by modification time (most recent first)
    #     csv_files = sorted([f for f in os.listdir(data_directory) if f.endswith('.csv')],
    #                        key=lambda f: os.path.getmtime(os.path.join(data_directory, f)),
    #                        reverse=True)
    #
    #     # Load the sample_config.csv to determine the number of duplicates to look for
    #     num_duplicates_to_look_for = len(self.sample_config_csv)
    #
    #     # Create an intermediate DataFrame to store averaged values
    #     avg_values_df = pd.DataFrame(columns=["percent_yield", "percent_rejection", "percent_purity"])
    #
    #     # Iterate through the recent CSV files and process them if not already processed
    #     for i, csv_file in enumerate(csv_files[:num_duplicates_to_look_for]):
    #         # Load the CSV file and extract relevant data for averaging
    #         file_path = os.path.join(data_directory, csv_file)
    #         df = pd.read_csv(file_path)
    #         avg_values_df = avg_values_df.append(df[["percent_yield", "percent_rejection", "percent_purity"]].mean(),
    #                                              ignore_index=True)
    #
    #         # Move the recently processed CSV file to the 'processed_data_{solid_name}' folder
    #         solid_name = csv_file.split('_')[1]  # Extract solid name from file name
    #         processed_data_directory = os.path.join(data_directory, f"processed_data_{solid_name}")
    #         os.makedirs(processed_data_directory, exist_ok=True)
    #         os.rename(file_path, os.path.join(processed_data_directory, csv_file))
    #
    #     # Calculate average values based on the intermediate DataFrame
    #     avg_yield = avg_values_df["percent_yield"].mean()
    #     avg_rejection = avg_values_df["percent_rejection"].mean()
    #     avg_purity = avg_values_df["percent_purity"].mean()
    #
    #     # Create a dictionary to store the results
    #     results_dict = {}
    #
    #     # Append results to the dictionary based on user preferences
    #     if yield_enabled:
    #         results_dict["yield"] = avg_yield
    #     if rejection_enabled:
    #         results_dict["rejection"] = avg_rejection
    #     if purity_enabled:
    #         results_dict["purity"] = avg_purity
    #
    #     return results_dict

    def process_data(self, input_csv, hplc_file, output_directory, product_name, impurity_name,
                     other_impurities: list = []):
        try:
            system_config = pd.read_csv(self.system_config_csv)
        except FileNotFoundError:
            print(f"File not found: {self.system_config_csv}")
            return

        hplc_data = pd.read_csv(hplc_file)

        current_date = datetime.now().strftime('%Y-%m-%d')

        # Just keeps the code happy, will fill this
        summary_df = pd.DataFrame({})

        try:
            sample_config = pd.read_csv(self.sample_config_csv)
        except FileNotFoundError:
            print(f"File not found: {self.sample_config_csv}")
            return

        duplicates = len(sample_config)
        data_frames = self.data_sorting(input_csv, duplicates)

        for i, df in enumerate(data_frames):
            product_type = product_name
            impurity_type = impurity_name

            percent_yield, percent_rejection, percent_purity = self.sample_calculations(df, product_type, impurity_type,
                                                                                        other_impurities)

            if percent_yield is not None:
                sample_info = sample_config.loc[
                    i, ['solid_name', 'solvent_A', 'solvent_B', 'sample_volume', 'solvent_ratio', 'slurry_volume',
                        'mass_mg', 'dilution_factor']]
                system_info = system_config.loc[
                    0, ['temp_1', 'time_temp_1', 'temp_2', 'time_temp_2', 'cooling_rate_deg_per_min', 'rest_time_min']]

                output_data = {
                    'sample_date': hplc_data.loc[0, ['sample']],
                    'temp_1': [system_info['temp_1']],
                    'time_temp_1': [system_info['time_temp_1']],
                    'temp_2': [system_info['temp_2']],
                    'time_temp_2': [system_info['time_temp_2']],
                    'cooling_rate_deg_per_min': [system_info['cooling_rate_deg_per_min']],
                    'rest_time_min': [system_info['rest_time_min']],
                    'solid_name': [sample_info['solid_name']],
                    'solvent_A': [sample_info['solvent_A']],
                    'solvent_B': [sample_info['solvent_B']],
                    'sample_volume': [sample_info['sample_volume']],
                    'solvent_ratio': [sample_info['solvent_ratio']],
                    'slurry_volume': [sample_info['slurry_volume']],
                    'mass_mg': [sample_info['mass_mg']],
                    'dilution_factor': [sample_info['dilution_factor']],
                    'percent_yield': [percent_yield],
                    'percent_rejection': [percent_rejection],
                    'percent_purity': [percent_purity],
                }
                if i == 0:
                    summary_df = pd.DataFrame(output_data)
                    output_csv_name = f"{current_date}_{sample_info['solid_name']}_{sample_info['solvent_A']}_{sample_info['solvent_B']}_{sample_info['solvent_ratio']}.csv"
                else:
                    temp_df = pd.DataFrame(output_data)
                    summary_df = pd.concat([summary_df, temp_df], axis=0)
                    print(summary_df)

        output_fp = os.path.join(output_directory, output_csv_name)
        if os.path.exists(output_fp):
            previous_df = pd.read_csv(output_fp)
            summary_df = pd.concat([previous_df, summary_df], axis=0)
        if not os.path.exists(output_directory):
            os.mkdir(output_directory)
        summary_df.to_csv(os.path.join(output_directory, output_csv_name), index=False)

        return output_fp

    def process_experimental_results(self, csv_file_path, yield_enabled=True, rejection_enabled=True,
                                     purity_enabled=True):

        sample_config = pd.read_csv(self.sample_config_csv)

        num_of_duplicates = sample_config.shape[0]
        print(num_of_duplicates)

        # Load the specified CSV file
        df = pd.read_csv(csv_file_path)

        # Select the most recent X rows from the dataframe
        df = df.tail(num_of_duplicates)

        # Calculate average values
        avg_yield = df["percent_yield"].mean()
        avg_rejection = df["percent_rejection"].mean()
        avg_purity = df["percent_purity"].mean()

        # Create a dictionary to store the results
        results_dict = {}

        # Append results to the dictionary based on user preferences
        if yield_enabled:
            results_dict["yield"] = avg_yield
        if rejection_enabled:
            results_dict["rejection"] = avg_rejection
        if purity_enabled:
            results_dict["purity"] = avg_purity

        return results_dict

# Change to save as one csv with multiple lines instead of multiple CSVs (both of Rebekahs functions)
# Same deal, multiple lines is better than one file
