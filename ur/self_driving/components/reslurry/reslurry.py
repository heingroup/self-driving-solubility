from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.quantos_handling.quantos_handling import QuantosHandling
from ur.components.shaker_handling.shaker_handling import ShakerHandling
from ur.components.ur3_centrifuge.centrifuge import Centrifuge
from ur.components.stir_vision.stirrer_handling import StirHandling

from ur.components.samplomatic.samplomatic import Samplomatic
from ika import Thermoshaker
from ur.components.data_handling.data_handling import DataHandling
import time
from ur.configuration import deck_consumables
import pandas as pd
import json
from datetime import date
import os
import matplotlib.pyplot as plt
from scipy import stats

class Reslurry:
    def __init__(self, solid_name, hplc_runtime: float, reslurry_config_csv: str, experiment_log_csv: str,
                vial_handling: VialHandling, centrifuge: Centrifuge, hplc_handling: HplcHandling,
                hplc_comm: HPLC_Comm, samplomatic: Samplomatic, somatic: SamplomaticHandling, ts: Thermoshaker,
                data_handling: DataHandling, quantos_handling: QuantosHandling, shaker_handling: ShakerHandling,
                stir_handling = StirHandling):
        self.solid_name = solid_name
        self.hplc_runtime = hplc_runtime
        self.reslurry_config_csv = reslurry_config_csv
        self.experiment_log_csv = experiment_log_csv
        self.vial_handling = vial_handling
        self.centrifuge = centrifuge
        self.hplc_comm = hplc_comm
        self.hplc_handling = hplc_handling
        self.samplomatic = samplomatic
        self.somatic = somatic
        self.ts = ts
        self.data_handling = data_handling
        self.quantos_handling = quantos_handling
        self.shaker_handling = shaker_handling
        self.stir_handling = stir_handling

    def run_reslurry(self):

        reslurry_config = pd.read_csv(self.reslurry_config_csv, index_col=False)
        number_of_experiments = len(reslurry_config.index)
        self.samplomatic.prime(volume_ml=2)
        # self.vial_handling.home()
        self.centrifuge.home()

        for i in range(number_of_experiments):
            df = reslurry_config.iloc[i]
            hplc_vial_list = []
            solid_mass = float(df["solid_mass_mg"])
            tube_index = df["tube_index"]
            duration = int(df["duration_min"])
            spin_duration = int(df['spin_duration_sec'])
            stir_rate = int(df['stir_rate_rpm'])
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            number_of_washes = int(df["number_of_washes"])
            solvent_ratio = float(df["solvent_ratio"])
            slurry_volume = float(df["slurry_volume_ml"])
            sample_volume = float(df["sample_volume_ml"])
            dilution_factor = float(df["dilution_factor"])
            use_shaker = bool(df["use_shaker"])
            duplicates = int(df["duplicates"])

            self.hplc_comm.send_tcp_message(f'wash_{number_of_washes}_samples_{duplicates}_duplicates')
            self.centrifuge.home()
            self.vial_handling.home()
            self.vial_handling.push_shaker()
            self.vial_handling.home()
            self.centrifuge.home()
            self.centrifuge.remove_cover()
            print(self.centrifuge.measure_centrifuge_angle())

            if tube_index == 'None':
                tube_index = deck_consumables.get_clean_tube(stirbar=True)
                print(tube_index)
                self.quantos_handling.grab_centrifuge_adapter()
                self.quantos_handling.vial_to_holder(vial_type='cent_adapter')

                tare_mass = self.quantos_handling.weigh_with_quantos()
                self.quantos_handling.switch_to_vertical()

                self.centrifuge.tube_to_active_area(tube_index=tube_index)
                self.centrifuge.open_tube()

                self.centrifuge.filter_from_tube('front')
                self.quantos_handling.vial_to_holder(vial_type='centrifuge')

                tube_mass = self.quantos_handling.weigh_with_quantos() - tare_mass
                df["tube_mass_mg"] = tube_mass * 1000

                dosed_mass = self.quantos_handling.dose_with_quantos(solid_weight=solid_mass,
                                                                vial_type='centrifuge')
                solid_mass = dosed_mass
                df["solid_mass_mg"] = solid_mass

                reslurry_config.iloc[i] = df

                reslurry_config.to_csv(self.reslurry_config_csv, index=False)
                reslurry_config = pd.read_csv(self.reslurry_config_csv, index_col=False)

                self.quantos_handling.switch_to_vertical()
                self.centrifuge.re_open_tube(index='front')
                self.quantos_handling.vial_from_holder(vial_type='centrifuge')
                self.centrifuge.filter_to_tube('front')

                self.quantos_handling.vial_from_holder(vial_type='cent_adapter')
                self.quantos_handling.return_centrifuge_adapter()

            else:
                self.centrifuge.tube_to_active_area(tube_index=tube_index)
                self.centrifuge.open_tube()

            for n in range(number_of_washes):
                stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
                stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]
                slurry_volume_A = slurry_volume * solvent_ratio
                slurry_volume_B = slurry_volume * (1 - solvent_ratio)

                self.vial_handling.home()
                self.samplomatic.prime(volume_ml=0.3)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)

                if stock_index_B != None:
                    self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)

                self.somatic.dispense_solvent_in_tube(volume_ml=slurry_volume, sample_index='A3')
                self.somatic.return_samplomatic()

                # centrifuge.push_tube_down(open=True)
                self.centrifuge.close_tube(index='front')

                if use_shaker:
                    self.centrifuge.tube_from_tray('A3')
                    self.centrifuge.tube_to_shaker()
                    self.ts.set_speed = stir_rate
                    self.ts.start_shaking()
                    self.shaker_handling.shake_with_hplc(shake_time=duration, hplc_runtime=self.hplc_runtime)
                    self.ts.stop_shaking()
                    self.centrifuge.tube_from_shaker()
                    self.centrifuge.tube_to_tray('A3')
                else:
                    self.centrifuge.tube_to_stirrer()
                    self.stir_handling.stir_for_duration(stir_duration=duration * 60, speed=stir_rate)
                    self.centrifuge.tube_from_stirrer()

                self.centrifuge.run_centrifuge(tube_index='front', centrifuge_duration=spin_duration)

                clean_tube_index = deck_consumables.get_clean_tube()
                self.centrifuge.tube_to_active_area(tube_index=clean_tube_index, front=False)
                self.centrifuge.open_tube(index='back')
                self.centrifuge.open_tube(index='front')
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.switch_filter(leave_front_empty=True)

                self.samplomatic.prime(volume_ml=1.5)
                self.vial_handling.home()
                current_vials = []

                if n == 0:
                    self.hplc_handling.run_blank()
                for d in range(duplicates):
                    self.samplomatic.prime(0.3)
                    self.somatic.grab_samplomatic()
                    if dilution_factor == 1:
                        self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3', airgap=True)
                    else:
                        self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3')
                    self.vial_handling.home()
                    if (sample_volume * dilution_factor) <= 0.3:
                        vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                    else:
                        vial_index = deck_consumables.get_clean_vial()[0]
                    current_vials.append(vial_index)
                    if dilution_factor == 1:
                        self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor,
                                                        vial_index=vial_index, backend=False)
                    else:
                        self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor,
                                                        vial_index=vial_index)
                    self.somatic.return_samplomatic()

                self.centrifuge.re_open_tube(index='front')
                self.centrifuge.tube_from_tray(tube_index='B3', filter=True)
                self.centrifuge.filter_to_tube(index='front')
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.close_tube(index='front')
                self.centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
                self.centrifuge.tube_to_active_area(tube_index='C3', front=True)
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.re_open_tube()
                self.centrifuge.home()

                for vial_index in current_vials:
                    self.vial_handling.home()
                    if use_shaker:
                        self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=vial_index,hplc_runtime=self.hplc_runtime)
                    else:
                        self.hplc_handling.run_current_vial(vial_index=vial_index, hplc_runtime=self.hplc_runtime)

                tube_index = clean_tube_index

            self.centrifuge.home()
            self.centrifuge.close_tube(index='front')
            self.centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
            self.centrifuge.place_cover()
            self.vial_handling.home()
            self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)
            self.hplc_handling.return_last_vial()

            self.hplc_handling.process_data_and_send_csv()

    # For testing movements with tube handling, place one vial in A1 and one in B1
    def tube_test(self):
        self.centrifuge.tube_to_active_area(tube_index='A1', front=False)
        self.centrifuge.tube_to_active_area(tube_index='B1', front=True)
        self.centrifuge.open_tube(index='back')
        self.centrifuge.open_tube(index='front')
        self.centrifuge.push_tube_down(open=True)
        self.centrifuge.switch_filter(leave_front_empty=True)
        self.centrifuge.tube_from_tray(tube_index='B3', filter=True)
        self.centrifuge.filter_to_tube(index='front')
        self.centrifuge.push_tube_down(open=True)
        self.centrifuge.close_tube(index='front')
        self.centrifuge.tube_from_active_area(tube_index='B1', front=True)
        self.centrifuge.tube_to_active_area(tube_index='C3', front=True)
        self.centrifuge.push_tube_down(open=True)


    def run_reslurry_no_hplc(self):

        reslurry_config = pd.read_csv(self.reslurry_config_csv, index_col=False)
        number_of_experiments = len(reslurry_config.index)
        self.samplomatic.prime(volume_ml=2)
        # self.vial_handling.home()
        self.centrifuge.home()

        for i in range(number_of_experiments):
            df = reslurry_config.iloc[i]
            hplc_vial_list = []
            solid_mass = float(df["solid_mass_mg"])
            tube_index = df["tube_index"]
            duration = int(df["duration_min"])
            spin_duration = int(df['spin_duration_sec'])
            stir_rate = int(df['stir_rate_rpm'])
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            number_of_washes = int(df["number_of_washes"])
            solvent_ratio = float(df["solvent_ratio"])
            slurry_volume = float(df["slurry_volume_ml"])
            sample_volume = float(df["sample_volume_ml"])
            dilution_factor = float(df["dilution_factor"])
            use_shaker = bool(df["use_shaker"])
            duplicates = int(df["duplicates"])

            self.hplc_comm.send_tcp_message(f'wash_{number_of_washes}_samples_{duplicates}_duplicates')
            self.centrifuge.home()
            self.vial_handling.home()
            self.vial_handling.push_shaker()
            self.vial_handling.home()
            self.centrifuge.home()
            self.centrifuge.remove_cover()
            print(self.centrifuge.measure_centrifuge_angle())

            if tube_index == 'None':
                tube_index = deck_consumables.get_clean_tube(stirbar=True)
                print(tube_index)
                self.quantos_handling.grab_centrifuge_adapter()
                self.quantos_handling.vial_to_holder(vial_type='cent_adapter')

                tare_mass = self.quantos_handling.weigh_with_quantos()
                self.quantos_handling.switch_to_vertical()

                self.centrifuge.tube_to_active_area(tube_index=tube_index)
                self.centrifuge.open_tube()

                self.centrifuge.filter_from_tube('front')
                self.quantos_handling.vial_to_holder(vial_type='centrifuge')

                tube_mass = self.quantos_handling.weigh_with_quantos() - tare_mass
                df["tube_mass_mg"] = tube_mass * 1000

                dosed_mass = self.quantos_handling.dose_with_quantos(solid_weight=solid_mass,
                                                                vial_type='centrifuge')
                solid_mass = dosed_mass
                df["solid_mass_mg"] = solid_mass

                reslurry_config.iloc[i] = df

                reslurry_config.to_csv(self.reslurry_config_csv, index=False)
                reslurry_config = pd.read_csv(self.reslurry_config_csv, index_col=False)

                self.quantos_handling.switch_to_vertical()
                self.centrifuge.re_open_tube(index='front')
                self.quantos_handling.vial_from_holder(vial_type='centrifuge')
                self.centrifuge.filter_to_tube('front')

                self.quantos_handling.vial_from_holder(vial_type='cent_adapter')
                self.quantos_handling.return_centrifuge_adapter()

            else:
                self.centrifuge.tube_to_active_area(tube_index=tube_index)
                self.centrifuge.open_tube()

            for n in range(number_of_washes):
                stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
                stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]
                slurry_volume_A = slurry_volume * solvent_ratio
                slurry_volume_B = slurry_volume * (1 - solvent_ratio)

                self.vial_handling.home()
                self.samplomatic.prime(volume_ml=0.3)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=0.8, stock_index=stock_index_A)
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_A, stock_index=stock_index_A)

                if stock_index_B != None:
                    self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)

                self.somatic.dispense_solvent_in_tube(volume_ml=slurry_volume, sample_index='A3')
                self.somatic.return_samplomatic()

                # centrifuge.push_tube_down(open=True)
                self.centrifuge.close_tube(index='front')

                if use_shaker:
                    self.centrifuge.tube_from_tray('A3')
                    self.centrifuge.tube_to_shaker()
                    self.ts.set_speed = stir_rate
                    self.ts.start_shaking()
                    self.shaker_handling.shake_with_hplc(shake_time=duration, hplc_runtime=self.hplc_runtime)
                    self.ts.stop_shaking()
                    self.centrifuge.tube_from_shaker()
                    self.centrifuge.tube_to_tray('A3')
                else:
                    self.centrifuge.tube_to_stirrer()
                    self.stir_handling.stir_for_duration(stir_duration=duration * 60, speed=stir_rate)
                    self.centrifuge.tube_from_stirrer()

                self.centrifuge.run_centrifuge(tube_index='front', centrifuge_duration=spin_duration)

                clean_tube_index = deck_consumables.get_clean_tube()
                self.centrifuge.tube_to_active_area(tube_index=clean_tube_index, front=False)
                self.centrifuge.open_tube(index='back')
                self.centrifuge.open_tube(index='front')
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.switch_filter(leave_front_empty=True)

                self.samplomatic.prime(volume_ml=1.5)
                self.vial_handling.home()
                current_vials = []

                for d in range(duplicates):
                    self.samplomatic.prime(0.3)
                    self.somatic.grab_samplomatic()
                    if dilution_factor == 1:
                        self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3', airgap=True)
                    else:
                        self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3')
                    self.vial_handling.home()
                    if (sample_volume * dilution_factor) <= 0.3:
                        vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                    else:
                        vial_index = deck_consumables.get_clean_vial()[0]
                    if dilution_factor == 1:
                        self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor,
                                                        vial_index=vial_index, backend=False)
                    else:
                        self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor,
                                                        vial_index=vial_index)
                    self.somatic.return_samplomatic()

                self.centrifuge.re_open_tube(index='front')
                self.centrifuge.tube_from_tray(tube_index='B3', filter=True)
                self.centrifuge.filter_to_tube(index='front')
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.close_tube(index='front')
                self.centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
                self.centrifuge.tube_to_active_area(tube_index='C3', front=True)
                self.centrifuge.push_tube_down(open=True)
                self.centrifuge.re_open_tube()
                self.centrifuge.home()

                # for vial_index in current_vials:
                #     self.vial_handling.home()
                #     if use_shaker:
                #         self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=vial_index,hplc_runtime=self.hplc_runtime)
                #     else:
                #         self.hplc_handling.run_current_vial(vial_index=vial_index, hplc_runtime=self.hplc_runtime)

                tube_index = clean_tube_index

            self.centrifuge.close_tube(index='front')
            self.centrifuge.tube_from_active_area(tube_index=tube_index, front=True)
            self.centrifuge.place_cover()
            # self.hplc_handling.return_last_vial()

            # self.hplc_handling.process_data_and_send_csv()


    def dilute_from_tube(self):
        reslurry_config = pd.read_csv(self.reslurry_config_csv, index_col=False)
        i=0
        df = reslurry_config.iloc[i]
        sample_volume = float(df["sample_volume_ml"])
        dilution_factor = float(df["dilution_factor"])
        duplicates = int(df["duplicates"])
        # self.samplomatic.prime(2)

        for d in range(duplicates):
            self.samplomatic.prime(0.3)
            self.somatic.grab_samplomatic()
            if dilution_factor == 1:
                self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3', airgap=True)
            else:
                self.somatic.draw_sample_from_tube(volume_ml=sample_volume, sample_index='A3')
            self.vial_handling.home()
            if (sample_volume * dilution_factor) <= 0.3:
                vial_index = deck_consumables.get_clean_vial(insert=True)[0]
            else:
                vial_index = deck_consumables.get_clean_vial()[0]
            if dilution_factor == 1:
                self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor, vial_index=vial_index, backend=False)
            else:
                self.somatic.dispense_hplc_grid(volume_ml=sample_volume * dilution_factor, vial_index=vial_index)
            self.somatic.return_samplomatic()

    def append_to_experiment_log(self):
        df = pd.read_csv(self.reslurry_config_csv)

        formatted_date = date.today().strftime("%m-%d-%Y")

        df.insert(0,'date',formatted_date)

        df_log = pd.read_csv(self.experiment_log_csv)
        df_log = df_log.append(df, ignore_index=True)

        df_log.to_csv(self.experiment_log_csv, index=False)

