from ur.self_driving.configuration.self_driving_config import standard_curve
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm

if __name__ == '__main__':
    # Set minimum accetable r2 value and acceptable range for intercept (+/- X% of the slope)
    r2_threshold = 0.99
    percent_of_slope = 10

    # Set masses for each dilution factor
    mass_3=5.26
    mass_4=5.29
    mass_5=4.90

    # Set temperature and set time to dissolve
    temp=25
    temp_time_min=5

    # Set number of dilutions and the hplc runtime
    number_of_dilutions=3

    # This is necesary trust me
    continue_dil_3 = False
    continue_dil_4 = False
    continue_dil_5 = False


    #Runs 3 standard curves at different dilutions, storing the slope, intercept and r2 in a json for each peak
    #Standard Curve, dilution = 3
    standard_curve.run_standard_curve(predosed_vial_index='A1',shaker_index='A1',mass=mass_3,
                                      prime_volume_ml=3,temp=temp,temp_time_min=temp_time_min,initial_volume_ml=1,sample_volume_ml=0.4,
                                      dilution_factor=3,number_of_dilutions=number_of_dilutions)
    # Standard Curve, dilution = 4
    standard_curve.run_standard_curve(predosed_vial_index='A2',shaker_index='A1',mass=mass_4,
                                      prime_volume_ml=1,temp=temp,temp_time_min=temp_time_min,initial_volume_ml=1,sample_volume_ml=0.3,
                                      dilution_factor=4,number_of_dilutions=number_of_dilutions)
    # Standard Curve, dilution = 5
    standard_curve.run_standard_curve(predosed_vial_index='A3',shaker_index='A1',mass=mass_5,
                                      prime_volume_ml=1,temp=temp,temp_time_min=temp_time_min,initial_volume_ml=1,sample_volume_ml=0.25,
                                      dilution_factor=5,number_of_dilutions=number_of_dilutions)

    while True:
        # Checks the r2 of each peak, set to True if above threshold, False if below
        dil_3_r2_check = standard_curve.check_r2_threshold(dilution_factor=3, threshold=r2_threshold)
        dil_4_r2_check = standard_curve.check_r2_threshold(dilution_factor=4, threshold=r2_threshold)
        dil_5_r2_check = standard_curve.check_r2_threshold(dilution_factor=5, threshold=r2_threshold)

        # Checks the intercept of each peak. Makes a range of X percent of the slope above and below 0. If inside range
        # returns True, if outside, returns False
        dil_3_intercept_check = standard_curve.check_intercept_through_zero(dilution_factor=3, percent_of_slope=percent_of_slope)
        dil_4_intercept_check = standard_curve.check_intercept_through_zero(dilution_factor=4, percent_of_slope=percent_of_slope)
        dil_5_intercept_check = standard_curve.check_intercept_through_zero(dilution_factor=5, percent_of_slope=percent_of_slope)


        #If all checks are True, then calculates the average of the slope and intercept and saves it into standard_cruve_averaged.json
        if ((dil_3_r2_check and dil_4_r2_check) or continue_dil_3) and ((dil_5_r2_check and dil_3_intercept_check) or continue_dil_4) and ((dil_4_intercept_check \
        and dil_5_intercept_check) or continue_dil_5):
            print('continue')
            standard_curve.calculate_averages(dilution_factors=[3,4,5])
            break

        #If the r2 or intercept checks are False, prompts user to check data and rerun, continue, or edit the peak data
        #and recalculate the slope/intercept
        if not continue_dil_3:
            if not dil_3_r2_check or not dil_3_intercept_check:
                answer = input('check dilution 3 data. Enter [rerun] to rerun standard curve. Enter [continue] to continue with current values. \
                               Enter [hplc_file.csv] (the name of the csv summary) to recalculate')
                if answer == 'rerun':
                    standard_curve.run_standard_curve(predosed_vial_index='A1', shaker_index='A1',
                                                      mass=mass_3,
                                                      prime_volume_ml=1, temp=temp, temp_time_min=temp_time_min,
                                                      initial_volume_ml=1.1, sample_volume_ml=0.4,
                                                      dilution_factor=3, number_of_dilutions=number_of_dilutions)

                elif answer == 'continue':
                    continue_dil_3 = True
                    continue
                else:
                    hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/' + answer
                    standard_curve.make_standard_curve(hplc_file=hplc_file, number_of_samples=number_of_dilutions+1,
                                                           initial_mass=mass_3,initial_volume=1.1,dilution_factor=3,min_r2=0.9)
        if not continue_dil_4:
            if not dil_4_r2_check or not dil_4_intercept_check or not continue_dil_4:
                answer = input('check dilution 4 data. Enter [rerun] to rerun standard curve. Enter [continue] to continue. \
                                           Enter [hplc_file.csv] (the name of the csv summary) to recalculate')
                if answer == 'rerun':
                    standard_curve.run_standard_curve(predosed_vial_index='A2', shaker_index='A1',
                                                      mass=mass_3,
                                                      prime_volume_ml=1, temp=temp, temp_time_min=temp_time_min,
                                                      initial_volume_ml=1, sample_volume_ml=0.3,
                                                      dilution_factor=4, number_of_dilutions=number_of_dilutions)

                elif answer == 'continue':
                    continue_dil_4 = True
                    continue
                else:
                    hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/' + answer
                    standard_curve.make_standard_curve(hplc_file=hplc_file, number_of_samples=number_of_dilutions + 1,
                                                       initial_mass=mass_4, initial_volume=1, dilution_factor=4, min_r2=0.9)
        if not continue_dil_5:
            if not dil_5_r2_check or not dil_5_intercept_check or not continue_dil_5:
                answer = input('check dilution 5 data. Enter [rerun] to rerun standard curve. Enter [continue] to continue. \
                                                       Enter [hplc_file.csv] (the name of the csv summary) to recalculate')
                if answer == 'rerun':
                    standard_curve.run_standard_curve(predosed_vial_index='A3', shaker_index='A1',
                                                      mass=mass_5,
                                                      prime_volume_ml=1, temp=temp, temp_time_min=temp_time_min,
                                                      initial_volume_ml=1, sample_volume_ml=0.25,
                                                      dilution_factor=5, number_of_dilutions=number_of_dilutions)
                elif answer == 'continue':
                    continue_dil_5 = True
                    continue
                else:
                    hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/' + answer
                    standard_curve.make_standard_curve(hplc_file=hplc_file, number_of_samples=number_of_dilutions + 1,
                                                       initial_mass=mass_5, initial_volume=1, dilution_factor=5,
                                                       min_r2=0.9)






