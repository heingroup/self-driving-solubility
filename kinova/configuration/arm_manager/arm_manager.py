import time
from typing import List
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from hein_robots.kinova.kinova_sequence import KinovaSequence


class ArmManager:
    def __init__(self, arm: KinovaGen3Arm, action_sequences: List[KinovaSequence],
                 sequence_names: List[str]):

        self.arm = arm
        self.action_sequences = {}

        self.SAFE_HEIGHT_LIFT = 200  # mm
        self.SAFE_HEIGHT_PUT = 5
        self.REGRIPPTING_LIFT = 160
        self.GRIPPER_OPEN_DEFAULT = 0.62
        self.GRIPPER_CLOSE_DEFAULT = 0.88
        self.GRIPPER_CLOSE_NEEDLE = 0.95
        self.GRIPPER_CLOSE_SM = 0.7
        self.GRIPPER_OPEN_SM = 0.5
        self.GRIPPER_CLOSE_HPLC_VIAL = 0.99
        self.GRIPPER_OPEN_HPLC_VIAL = 0.80
        self.GRIPPER_CLOSE_CENT_BLOCK = 0.49
        self.GRIPPER_OPEN_CENT_BLOCK = 0.40
        self.capped = True
        self.from_capping = False
        self.hplc_left_occupied = False
        self.hplc_right_occupied = False
        self.ur_back_occupied = True
        self.ur_front_occupied = False
        if not len(sequence_names) == len(action_sequences):
            raise ('Sequence amount and sequence name amount do not match.')
        for i in range(len(sequence_names)):
            self.action_sequences[sequence_names[i]] = action_sequences[i]

    def grid_location(self, row: int = 1, column: int = 1, tray: str = 'B', is_sm: bool = False):
        starting_position = self.action_sequences['vial_tray']['vial_tray_' + tray]
        if is_sm:
            starting_position = self.action_sequences['sm_locations']["vial_tray_" + tray]
        final_position = None
        if tray == "A":
            if row > 2 or column > 3:
                raise IndexError('The input row or column is invalid.')
            final_position = starting_position.translate(x=-40 * (row - 1), y=-39.5 * (column - 1))
        if tray == 'B':
            if row > 2 or column > 4:
                raise IndexError('The input row or column is invalid.')
            else:
                final_position = starting_position.translate(x=-28 * (row - 1), y=-30 * (column - 1))
        if tray == 'I':
            if row > 5 or column > 2:
                raise IndexError('The input row or column is invalid.')
            else:
                final_position = starting_position.translate(x=-50.25 * (row - 1), y=-81 * (column - 1))
        if tray == 'C':
            if row > 2 or column > 2:
                raise IndexError('The input row or column is invalid.')
            else:
                final_position = starting_position.translate(x=-25 * (row - 1), y=-33 * (column - 1))
        if tray == 'N_v':
            if row > 4 or column > 6:
                raise IndexError('The input row or column is invalid.')
            else:
                final_position = starting_position.translate(x=-20 * (row - 1), y=-19.9 * (column - 1))

        # Todo: add autosampler tray grid
        # if tray == 'AS':
        #     if row > 6 or column > 8:
        #         raise IndexError('The input row or column is invalid.')
        #     else:
        #         final_position = starting_position.translate(x=-20 * (row - 1), y=-19.9 * (column - 1))
        return final_position

    def safe_pose_test(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        self.arm.move_to_locations(safe_pose_v)


    def vial_from_tray(self, row: int = 1, column: int = 1, tray: str = 'A'):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        vial_position = self.grid_location(row, column, tray)
        lift_position = vial_position.translate(z=self.SAFE_HEIGHT_LIFT)

        # actions
        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(lift_position)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(vial_position)
        self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)
        time.sleep(2)
        self.arm.move_to_locations(lift_position)

    def vial_to_regripping_v(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v_regripping']
        regripping_v = self.grid_location(1, 1, 'A')
        regripping_lift_v = regripping_v.translate(z=50)
        if (not self.capped) or self.from_capping:
            regripping_v = regripping_v.translate(z=-30)
            self.from_capping = False

        # actions
        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(regripping_lift_v)
        self.arm.move_to_locations(regripping_v)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(regripping_lift_v)
        self.arm.move_to_locations(safe_pose_v)

    def vial_from_regripping_h(self):
        regripping_h = self.action_sequences['regripping_action']['regripping_grab_h']
        regripping_lift_h = regripping_h.translate(z=150)
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']

        # actions
        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(regripping_lift_h)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(regripping_h)
        time.sleep(0.5)
        self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)
        time.sleep(2)
        self.arm.move_to_locations(regripping_lift_h)

    def vial_to_integrity(self, row: int, column: int, capped=True):
        position = self.grid_location(row, column, 'I')
        lift = position.translate(z=self.SAFE_HEIGHT_LIFT)
        self.arm.move_to_locations(lift)
        self.arm.move_to_locations(position)
        time.sleep(1)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(lift)

    def vial_from_integrity(self, row: int, column: int, capped=True):
        position = self.grid_location(row, column, 'I')
        lift = position.translate(z=self.SAFE_HEIGHT_LIFT / 2)

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(lift)
        self.arm.move_to_locations(position)
        if capped:
            self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)

        else:
            self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        time.sleep(2)
        self.arm.move_to_locations(lift, velocity=10)

    def vial_to_regripping_h(self):
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        regripping_h = self.action_sequences['regripping_action']['regripping_grab_h']
        regripping_lift_h = regripping_h.translate(z=150)

        # actions
        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(regripping_lift_h)
        self.arm.move_to_locations(regripping_h)
        time.sleep(0.5)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(regripping_lift_h)
        self.arm.move_to_locations(safe_pose_h_side)

    def vial_from_regripping_v(self):
        safe_pose_v_side = self.action_sequences['regripping_action']['safe_pose_v_regripping']
        regripping_v = self.grid_location(1, 1, 'A')
        regripping_lift_v = regripping_v.translate(z=150)
        # actions
        self.arm.move_to_locations(safe_pose_v_side)
        self.arm.move_to_locations(regripping_lift_v)

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        if not self.capped:
            regripping_v = regripping_v.translate(z=-30)
        self.arm.move_to_locations(regripping_v)
        time.sleep(0.5)
        self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)

        time.sleep(2)
        self.arm.move_to_locations(regripping_lift_v)
        self.arm.move_to_locations(safe_pose_v_side)

    def vial_to_tray(self, row: int = 1, column: int = 1, tray: str = 'A'):
        vial_position = self.grid_location(row, column, tray)
        lift_position = vial_position.translate(z=self.SAFE_HEIGHT_LIFT)
        if self.from_capping:
            vial_position = vial_position.translate(z=-30)
            self.from_capping = False
        self.arm.move_to_locations(lift_position)
        self.arm.move_to_locations(vial_position)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(lift_position)

    def uncap(self):
        capped = self.action_sequences['cap']['capped']
        park = self.action_sequences['cap']['park']
        uncapped = self.action_sequences['cap']['uncapped']
        capped_lift = capped.translate(z=100)
        park_lift = park.translate(z=100)

        self.arm.move_to_locations(capped)

        self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)
        for i in range(8):
            self.arm.move(z=0.5, rz=90, relative=True, velocity=500)
        self.arm.move_to_locations(capped_lift, park_lift, park)

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(park_lift, capped_lift)
        self.capped = False

    def vial_to_capping_station(self):
        """
        Using the capping station
        """
        capped = self.action_sequences['cap']['capped']
        uncapped = self.action_sequences['cap']['uncapped']
        capped_lift = capped.translate(z=150)

        self.arm.move_to_locations(capped_lift)
        if self.capped:
            self.arm.move_to_locations(capped, velocity=100)
        else:
            self.arm.move_to_locations(uncapped, velocity=100)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(capped_lift)

    def cap(self):
        """
        Using the capping station with vial placed inside
        """
        capped = self.action_sequences['cap']['capped']
        park = self.action_sequences['cap']['park']
        capped_lift = capped.translate(z=150)
        park_lift = park.translate(z=100)

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(capped_lift, park_lift, park)
        self.arm.open_gripper(self.GRIPPER_CLOSE_DEFAULT)
        self.arm.move_to_locations(park_lift, capped_lift, capped)
        for i in range(8):
            self.arm.move(z=-0.25, rz=-90, relative=True, velocity=500)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(capped_lift)
        self.capped = True

    def vial_from_capping_station(self):
        capped = self.action_sequences['cap']['capped']
        uncapped = self.action_sequences['cap']['uncapped']
        capped_lift = capped.translate(z=100)

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(uncapped)
        self.arm.open_gripper(self.GRIPPER_CLOSE_SM)
        self.arm.move_to_locations(capped_lift)
        self.from_capping = True

    def get_vial_from_capping(self, end_pose = 'V'):
        '''
        Take the vial out of the capping station. Including regripping.
        '''
        self.vial_from_capping_station()
        self.vial_to_regripping_v()
        if self.capped and end_pose == 'V':
            self.vial_from_regripping_v()
        elif end_pose == 'H':
            self.vial_from_regripping_h()



    def vial_from_n9(self):
        # Note: Always starting on the Kinova side.
        # No stops at N9 self
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        intermediate_position_Kinova = self.action_sequences['gateway_moves']['retreat_3']
        intermediate_position_between = self.action_sequences['gateway_moves']['retreat_2']
        intermediate_position_N9 = self.action_sequences['gateway_moves']['retreat_1']
        N9_grab = self.action_sequences['gateway_moves']['N9_grab_vial']
        N9_lift = N9_grab.translate(z=50)

        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_N9)

        self.arm.move_to_locations(N9_lift)
        time.sleep(2)

        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        time.sleep(2)

        self.arm.move_to_locations(N9_grab)
        time.sleep(2)

        self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        time.sleep(2)
        self.arm.move_to_locations(N9_lift)
        time.sleep(2)

        self.arm.move_to_locations(intermediate_position_N9)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(safe_pose_h_side)

    def vial_to_n9(self):
        ### Note: Always starting on the Kinova side.
        ### No stops at N9 self
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        intermediate_position_Kinova = self.action_sequences['gateway_moves']['retreat_3']
        intermediate_position_between = self.action_sequences['gateway_moves']['retreat_2']
        intermediate_position_N9 = self.action_sequences['gateway_moves']['retreat_1']
        N9_grab = self.action_sequences['gateway_moves']['N9_grab_vial']

        N9_lift = N9_grab.translate(z=50)

        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_N9)

        self.arm.move_to_locations(N9_lift)
        self.arm.move_to_locations(N9_grab)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(N9_lift)
        self.arm.move_to_locations(intermediate_position_N9)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(safe_pose_h_side)

    def vial_to_kinova(self):
        ### Note: Always starting on the Kinova side.
        ### No stops at N9 self
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        intermediate_position_Kinova = self.action_sequences['gateway_moves']['retreat_3']
        time.sleep(2)

        Kinova_grab = self.action_sequences['gateway_moves']['Kinova_grab_vial']

        Kinova_lift = Kinova_grab.translate(z=50)
        time.sleep(2)

        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(Kinova_lift)

        self.arm.move_to_locations(Kinova_grab)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(Kinova_lift)
        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(safe_pose_h_side)

    def vial_from_kinova(self):
        ### Note: Always starting on the Kinova side.
        ### No stops at N9 self
        # safe_pose_h_side = self.action_sequences['regripping_action['safe_pose_h_side']
        intermediate_position_Kinova = self.action_sequences['gateway_moves']['retreat_3']

        Kinova_grab = self.action_sequences['gateway_moves']['Kinova_grab_vial']
        Kinova_lift = Kinova_grab.translate(z=50)

        self.arm.move_to_locations(intermediate_position_Kinova)
        self.arm.move_to_locations(Kinova_lift)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(Kinova_grab)
        self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        time.sleep(2)
        self.arm.move_to_locations(Kinova_lift)
        self.arm.move_to_locations(intermediate_position_Kinova)

    def sm_from_base(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        sm_grab = self.action_sequences['sm_locations']['grab_sm']
        sm_lift = sm_grab.translate(z=200, x=1)

        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(sm_lift)
        self.arm.open_gripper(self.GRIPPER_OPEN_SM)
        self.arm.move_to_locations(sm_grab)
        self.arm.open_gripper(self.GRIPPER_CLOSE_SM)
        time.sleep(2)
        self.arm.move_to_locations(sm_lift)
        self.arm.move_to_locations(safe_pose_v)

    def sm_to_base(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        sm_grab = self.action_sequences['sm_locations']['grab_sm']
        sm_lift = sm_grab.translate(z=200, x=1)
        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(sm_lift)
        self.arm.move_to_locations(sm_grab)
        self.arm.open_gripper(self.GRIPPER_OPEN_SM)
        time.sleep(1)
        self.arm.move_to_locations(sm_lift)
        self.arm.move_to_locations(safe_pose_v)

    def insert_vent(self):
        pickup = self.action_sequences['ventilator']['pickup']
        pierce = self.action_sequences['ventilator']['pierce'].translate(z=-3)
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        safe_pose_h = self.action_sequences['regripping_action']['safe_pose_h_inward']
        pickup_lift = pickup.translate(z=50)
        enter = pickup.translate(x=200)
        pierce_lift = pierce.translate(z=50)

        self.arm.open_gripper()
        self.arm.move_to_locations(safe_pose_v, safe_pose_h, enter, pickup)
        self.arm.close_gripper(self.GRIPPER_CLOSE_NEEDLE)
        time.sleep(1)
        self.arm.move_to_locations(pickup_lift, pierce_lift, pierce, velocity=50)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(enter, safe_pose_h, safe_pose_v)

    def retract_vent(self):
        pickup = self.action_sequences['ventilator']['pickup'].translate(z=-3)
        pierce = self.action_sequences['ventilator']['pierce']
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        safe_pose_h = self.action_sequences['regripping_action']['safe_pose_h_inward']
        pickup_lift = pickup.translate(z=50)
        enter = pickup.translate(x=200)
        pierce_lift = pierce.translate(z=50)

        self.arm.open_gripper()
        self.arm.move_to_locations(safe_pose_v, safe_pose_h, enter, pierce)
        self.arm.close_gripper(self.GRIPPER_CLOSE_NEEDLE)
        time.sleep(1)
        self.arm.move_to_locations(pierce_lift, pickup_lift, pickup, velocity=50)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(enter, safe_pose_h, safe_pose_v)

    def sm_to_integrity(self, row: int, column: int, lift: bool = False):
        position = self.grid_location(row, column, 'I', True)
        position_out = position.translate(x=100)
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        if lift:
            position = position.translate(z=50)
        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(position_out)
        self.arm.move_to_locations(position)

    def sm_to_capping_station(self, tilt_left=True):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        capping_station = self.action_sequences['sm_locations']['capping_right']

        if tilt_left:
            capping_station = self.action_sequences['sm_locations']['capping_left']

        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(capping_station)

    def sm_from_capping_station(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        self.arm.move_to_locations(safe_pose_v)

    def sm_from_integrity(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(safe_pose_v)

    def sm_to_reagent(self, row: int = 1, column: int = 1):
        reagent_position = self.grid_location(row=row, column=column, is_sm=True)
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(reagent_position)

    def sm_to_cdi(self):
        cdi_position = self.action_sequences['vial_tray']['cdi_position']
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(cdi_position)

    def sm_from_cdi(self):
        cdi_position = self.action_sequences['vial_tray']['cdi_position']
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(cdi_position)
        self.arm.move_to_locations(safe_pose_v)

    def sm_from_reagent(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']
        self.arm.move_to_locations(safe_pose_v)

    def sm_to_hplc_tray(self):
        hplc_tray = self.action_sequences['sm_locations']['vial_tray_N']
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(safe_pose_v)
        self.arm.move_to_locations(hplc_tray.translate(z=100))
        self.arm.move_to_locations(hplc_tray)

    def sm_from_hplc_tray(self):
        hplc_tray = self.action_sequences['sm_locations']['vial_tray_N']
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move_to_locations(hplc_tray)

        self.arm.move_to_locations(hplc_tray.translate(z=100))
        self.arm.move_to_locations(safe_pose_v)

    def sm_to_tray_A(self, row: int, column: int):
        position = self.grid_location(row, column, tray='A', is_sm=True)
        lift = position.translate(z=50)
        self.arm.move_to_locations(lift)
        self.arm.move_to_locations(position)

    def pour_into_funnel(self):
        """
        start by holding a vial with solid-liquid mixture inside in horizontal position
        """
        before = self.action_sequences['filtration']['before']
        pour = self.action_sequences['filtration']['pour']
        drain = self.action_sequences['filtration']['drain']
        lift = before.translate(z=150)

        self.arm.move_to_locations(lift, before, pour, drain)
        for i in range(4):
            self.arm.move(z=2, relative=True)
            self.arm.move(z=-2, relative=True)
        time.sleep(10)
        for i in range(4):
            self.arm.move(z=2, relative=True)
            self.arm.move(z=-2, relative=True)
        self.arm.move_to_locations(drain, pour, before, lift)

    def centrifuge_tray_from_base(self):
        centrifuge = self.action_sequences['centrifuge']['centrifuge']
        centrifuge_lift = centrifuge.translate(z=100)
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']

        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(safe_pose_h_side, centrifuge_lift, centrifuge)
        self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        self.arm.move_to_locations(centrifuge_lift, safe_pose_h_side)

    def centrifuge_tray_to_base(self):
        centrifuge = self.action_sequences['centrifuge']['centrifuge']
        centrifuge_lift = centrifuge.translate(z=100)
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']

        self.arm.move_to_locations(centrifuge_lift, centrifuge)
        self.arm.open_gripper(self.GRIPPER_OPEN_DEFAULT)
        self.arm.move_to_locations(centrifuge_lift, safe_pose_h_side)

    def centrifuge_tray_to_n9(self):
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        intermediate_position_kinova = self.action_sequences['gateway_moves']['retreat_3']
        intermediate_position_between = self.action_sequences['gateway_moves']['retreat_2']
        intermediate_position_n9 = self.action_sequences['gateway_moves']['retreat_1']
        n9_centrifuge = self.action_sequences['centrifuge']['n9_centrifuge'].translate(z=6)
        n9_centrifuge_lift = n9_centrifuge.translate(z=50)

        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(intermediate_position_kinova)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_n9)

        self.arm.move_to_locations(n9_centrifuge_lift)
        self.arm.move_to_locations(n9_centrifuge)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(n9_centrifuge_lift)
        self.arm.move_to_locations(intermediate_position_n9)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_kinova)
        self.arm.move_to_locations(safe_pose_h_side)

    def centrifuge_tray_from_n9(self):
        safe_pose_h_side = self.action_sequences['regripping_action']['safe_pose_h_side']
        intermediate_position_kinova = self.action_sequences['gateway_moves']['retreat_3']
        intermediate_position_between = self.action_sequences['gateway_moves']['retreat_2']
        intermediate_position_n9 = self.action_sequences['gateway_moves']['retreat_1']
        n9_centrifuge = self.action_sequences['centrifuge']['n9_centrifuge']
        n9_centrifuge_lift = n9_centrifuge.translate(z=50)

        self.arm.move_to_locations(safe_pose_h_side)
        self.arm.move_to_locations(intermediate_position_kinova)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_n9)

        self.arm.move_to_locations(n9_centrifuge_lift)

        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(n9_centrifuge)
        self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        self.arm.move_to_locations(n9_centrifuge_lift)
        self.arm.move_to_locations(intermediate_position_n9)
        self.arm.move_to_locations(intermediate_position_between)
        self.arm.move_to_locations(intermediate_position_kinova)
        self.arm.move_to_locations(safe_pose_h_side)

    def sm_to_centrifuge_tray(self, row: int, column: int):
        position = self.grid_location(row, column, 'C', is_sm=True)
        position_lift = position.translate(z=100)

        self.arm.move_to_locations(position_lift, )
        self.arm.move_to_locations(position)

    def sm_return_safe_pose_v(self):
        safe_pose_v = self.action_sequences['regripping_action']['safe_pose_v']

        self.arm.move(z=100, relative=True)
        self.arm.move_to_locations(safe_pose_v)

    def vial_to_ur_front(self):
        urdeck_approach = self.action_sequences['hplc_picknplace']['PickNPlace_URDeck_Approach']
        urdeck_frontvial = self.action_sequences['hplc_picknplace']['PickNPlace_URDeck_Front']
        urdeck_frontvial_lift = urdeck_frontvial.translate(z=50)

        self.arm.move_to_locations(urdeck_approach)
        time.sleep(3)
        self.arm.move_to_locations(urdeck_frontvial_lift)
        self.arm.move_to_locations(urdeck_frontvial)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(urdeck_frontvial_lift)
        self.arm.move_to_locations(urdeck_approach)

    def hplc_exchange_home(self):
        urdeck_approach = self.action_sequences['hplc_picknplace'].joints['PickNPlace_URDeck_Approach']
        self.arm.move_joints(urdeck_approach)

    def vial_hplc_exchange(self):
        # urdeck_approach = self.action_sequences['hplc_picknplace']['PickNPlace_URDeck_Approach']
        urdeck_approach = self.action_sequences['hplc_picknplace'].joints['PickNPlace_URDeck_Approach']
        urdeck_backvial = self.action_sequences['hplc_picknplace']['PickNPlace_URDeck_Rear']
        urdeck_frontvial =  self.action_sequences['hplc_picknplace']['PickNPlace_URDeck_Front']
        hplc_midsafe1 =  self.action_sequences['hplc_picknplace'].joints['PickNPlace_MidSafe1']
        hplc_midsafe2 = self.action_sequences['hplc_picknplace'].joints['PickNPlace_MidSafe2']
        urdeck_backvial_lift = urdeck_backvial.translate(z=50)
        urdeck_frontvial_lift =urdeck_frontvial.translate(z=50)
        hplc_approach = self.action_sequences['hplc_picknplace'].locations['PickNPlace_HPLC_Approach']
        # hplc_approach = self.action_sequences['hplc_picknplace'].joints['PickNPlace_HPLC_Approach']
        hplc_left = self.action_sequences['hplc_picknplace']['PickNPlace_HPLC_Left_new']
        hplc_left_lift = hplc_left.translate(z=35)
        hplc_right = self.action_sequences['hplc_picknplace']['PickNPlace_HPLC_Right_new']
        hplc_right_lift = hplc_right.translate(z=35)

        self.arm.move_joints(urdeck_approach)
        self.arm.move_to_locations(urdeck_frontvial_lift)
        self.arm.move_to_locations(urdeck_frontvial)
        self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
        self.arm.move_to_locations(urdeck_frontvial_lift)
        self.arm.move_joints(urdeck_approach)
        self.arm.move_joints(hplc_midsafe1)
        self.arm.move_joints(hplc_midsafe2)
        self.arm.move_to_locations(hplc_approach)
        if self.hplc_left_occupied == False:
            self.arm.move_to_locations(hplc_left_lift)
            self.arm.move_to_locations(hplc_left)
            self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
            self.arm.move_to_locations(hplc_left_lift)
            self.hplc_left_occupied = True
            if self.hplc_right_occupied == True:
                self.arm.move_to_locations(hplc_right_lift)
                self.arm.move_to_locations(hplc_right)
                self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
                self.arm.move_to_locations(hplc_right_lift)
                self.hplc_right_occupied = False
        elif self.hplc_right_occupied == False:
            self.arm.move_to_locations(hplc_right_lift)
            self.arm.move_to_locations(hplc_right)
            self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
            self.arm.move_to_locations(hplc_right_lift)
            self.hplc_right_occupied = True
            if self.hplc_left_occupied == True:
                self.arm.move_to_locations(hplc_left_lift)
                self.arm.move_to_locations(hplc_left)
                self.arm.open_gripper(self.GRIPPER_CLOSE_HPLC_VIAL)
                self.arm.move_to_locations(hplc_left_lift)
                self.hplc_left_occupied = False
        self.arm.move_to_locations(hplc_approach)
        self.arm.move_joints(hplc_midsafe2)
        self.arm.move_joints(hplc_midsafe1)
        self.arm.move_joints(urdeck_approach)
        self.arm.move_to_locations(urdeck_backvial_lift)
        self.arm.move_to_locations(urdeck_backvial)
        self.arm.open_gripper(self.GRIPPER_OPEN_HPLC_VIAL)
        self.arm.move_to_locations(urdeck_backvial_lift)
        self.arm.move_joints(urdeck_approach)

    def hplc_reset_to_default(self):
        self.hplc_left_occupied = False
        self.hplc_right_occupied = False
        self.ur_back_occupied = True
        self.ur_front_occupied = False

# if __name__ == "__main__":