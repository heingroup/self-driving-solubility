from hein_robots.kinova.kinova_sequence import KinovaSequence

regripping_action = KinovaSequence(r"C:\Users\User\PycharmProjects\self_driving_solubility\kinova\configuration\sequences\regripping_action.json")
hplc_picknplace = KinovaSequence(r"C:\Users\User\PycharmProjects\self_driving_solubility\kinova\configuration\sequences\PickNPlace_HPLC.json")
# vial_tray = KinovaSequence(r"C:\Users\User\PycharmProjects\self_driving_solubility\kinova\configuration\sequences\autosampler_grid.json")

sequences = [regripping_action,hplc_picknplace]
# sequences = [regripping_action,hplc_picknplace, vial_tray]

sequence_names = ['regripping_action','hplc_picknplace']
# sequence_names = ['regripping_action','hplc_picknplace', 'vial_tray']